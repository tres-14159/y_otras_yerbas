// meson init -l cpp -b build .

#include <iostream>

#include "a.h"
#include "b.h"

int main(int argc, char **argv) {
    if (argc != 1) {
        std::cout << argv[0] <<  "takes no arguments.\n";
        return 1;
    }
    std::cout << "Testing class and files.\n";
    
    A a = A(3);
    B b = B(8);
    
    std::cout << a.a << "\n";
    std::cout << b.a << "\t" << b.b.a << "\n";
    
    return 0;
}
