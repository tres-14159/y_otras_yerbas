#ifndef A_H
#define A_H

class B;
class A {
    public:
        int a;
        A(void);
        A(int a);
        B createB(int a);
};

#endif
