[[_TOC_]]

# MisPedradas

- Me gusta las instalaciones tal cual (por eso no desecho vim frente a neovim) y me gusta trabajar tal cual viene de fábrica porque menos es mas: menos mierdas menos cpu y ram, el miedo de usar un servidor desconocido con un vim del año de la polca. Y por eso voy a intentar llegar al mínimo de plugins (por ejemplo el nerdtree que tanto habla la gente por lo poco que voy viendo no es necesario). 
- Lo he organizado un poco así lo que yo necesito y en el orden que yo necesito.
- Puede crecer o mejorar (espero que no empeorar) el documento.

# Modos (no puedes beber y respirar a la vez)

Del **Normal** se va a todos, pero no se puede ir de uno a otro sin pasar por Normal.

Los mas importantes son:
- **Normal Mode:** el de por defecto.
  - `esc` : volver a el.
- **Insert Mode:** el de escribir.
  - `i`: empiezas a escribir detras del cursor.
  - `a`: empiezas a escribir delante del cursor.
- **Remplace Mode:** el de remplazar.
  - `r`: remplaza un solo caracter encima del cursor. Vuelve a **Normal Mode**.
  - `R`: empiezas en modo remplazo encima del cursor. Se queda en **Remplace Mode**.
- **Visual Mode:** el de seleccionar texto.
  - `v`: seleccionar caracter a caracter.
  - `shift` + `v`: seleccionar linea a linea.
  - `ctrl` + `v`: seleccionar en cuadrado.
- **Command Mode:** el de la cajita de texto (un prompt) de abajo (especial, no le afecta el movimiento en vertical y otros).
  - `:` : tecla de los dos puntos. Desde **Insert Mode** y **Remplace Mode** no se puede llegar, escribe : .

En caso de perdida, `esc` varias veces hasta estar en el **Normal Mode**.

# Partes de la pantalla
De arriba a abajo.

- pestañas.
- zona de escribir.
- ~ : las lineas solo con ~ es la nada (historia interminable) ahí no hay fichero.
- barra de estado: muestra cosas de estado (y vim es solo una y va cambiando y muestra mensajes y también la cajita de comando). Se puede mejorar por plugins y asi.
  - nombre fichero
  - estado: (+) editado.
  - posición: fila,columna .
  - Si el fichero entra en la pantalla pone Todo.
    - Si no n% dentro fichero que la parte que vemos.
    - Comienzo si estan las primeras lineas
    - Final si estan las finales
- neovim: tiene otra mas
  - estado tambien cajita de comando
  - muestra mensajes

# Movimiento

| Mov | Modo           | Tecla |
|:---:|----------------|:-----:|
| ←   | Normal, Visual | `h`   |
| ←   | (TODOS)        | `izq` |
| →   | Normal, Visual | `l`   |
| →   | (TODOS)        | `der` |
| ↑   | Normal, Visual | `k`   |
| ↑   | (TODOS)        | `arr` |
| ↓   | Normal, Visual | `j`   |
| ↓   | (TODOS)        | `abj` |

| Mov                                                 | Modo             | Tecla          |
|-----------------------------------------------------|------------------|:--------------:|
| atrás inicio palabra (se para signos y espacios)    | Normal, Visual   | `b`            |
| atrás inicio palabra (se para signos y espacios)    | Insert, Remplace | `ctrl` + `izq` |
| atrás inicio palabra (se para espacios)             | Normal, Visual   | `B`            |
| atrás inicio palabra (se para espacios)             | Normal, Visual   | `ctrl` + `izq` |
| adelante inicio palabra (se para signos y espacios) | Normal, Visual   | `w`            |
| adelante inicio palabra (se para signos y espacios) | Insert, Remplace | `ctrl` + `der` |
| adelante final palabra (se para signos y espacios)  | Normal, Visual   | `e`            |
| adelante final palabra (se para espacios)           | Normal, Visual   | `E`            |

| Mov                                     | Modo           | Tecla           |
|-----------------------------------------|----------------|:---------------:|
| Inicio linea                            | Normal, Visual | `0`             |
| Inicio linea                            | (TODOS)        | `Home`          |
| Inicio linea 1º letra (obvia espacios)  | Normal, Visual | `^`             |
| Final linea                             | Normal, Visual | `$`             |
| Final linea                             | (TODOS)        | `End`           |
| Inicio de documento                     | Normal, Visual | `gg`            |
| Inicio de documento                     | (TODOS)        | `ctrl` + `Home` |
| Final del documento                     | Normal, Visual | `G`             |
| Final del documento                     | (TODOS)        | `ctrl` + `End`  |
| Scroll Arriba                           | Normal, Visual | `ctrl` + `b`    |
| Scroll Arriba                           | (TODOS)        | `PagUp`         |
| Scroll Arriba                           | (TODOS)        | `shift` + `arr` |
| Scroll Abajo                            | Normal, Visual | `ctrl` + `f`    |
| Scroll Abajo                            | (TODOS)        | `PagDown`       |
| Scroll Abajo                            | (TODOS)        | `shift` + `abj` |
| 1/2 Scroll Arriba                       | Normal, Visual | `ctrl` + `u`    |
| 1/2 Scroll Abajo                        | Normal, Visual | `ctrl` + `d`    |
| 1 linea arriba sin mover el cursor      | Normal, Visual | `ctrl` + `y`    |
| 1 linea abajo sin mover el cursor       | Normal, Visual | `ctrl` + `e`    |
| Inicio de la primera linea en pantalla  | Normal, Visual | `H`             |
| Inicio de la linea en mitad de pantalla | Normal, Visual | `M`             |
| Inicio de la final linea de pantalla    | Normal, Visual | `L`             |
| **Deshacer un mov (de los saltos)**     | Normal         | `ctrl` + `o`    |

# Acciones básicas

## Abrir, Guardar y Salir

Tecla **Tab** en el prompt o en la linea de comandos muestra un menu contextual para autocompletar (en neovim en plan vertical y en vim horizontal).

| Acción                              | Modo   | Tecla/Comando |
|-------------------------------------|--------|:-------------:|
| Salir                               | Prompt | q             |
| Salir sin guardar                   | Prompt | q!            |
| Salir sin guardar                   | Normal | `ZQ`          |
| Salir multiples buffers             | Prompt | qa / qall     |
| Salir multiples buffers sin guardar | Prompt | qa! / qall!   |


| Acción                              | Modo   | Tecla/Comando |
|-------------------------------------|--------|:-------------:|
| Guardar                             | Prompt | w             |
| Guardar y salir                     | Prompt | wq / x        |
| Guardar y salir                     | Normal | `ZZ`          |
| Guardar todos los buffers           | Prompt | wa / wall     |
| Guardar todos los buffers y salir   | Prompt | wqa           |

**Guardar como** es `:w fichero` o `:wq fichero` (guardar y salir) pero es un poco especial porque si es un buffer nuevo pues cambia en la barra de estado a ese fichero nuevo, pero si estas editando un fichero sigue en esa edición y no cambia al nuevo y se queja por salir sin guardar los cambios en el fichero antiguo.

Abrir normalmente es directamente durante la ejecución neovim/vim, si abres en un nvim/vim vacio pues ya esta, si no lo envía a un buffer nuevo y va a el. (Funciona tab)

| Acción        | Modo   | Tecla/Comando |
|---------------|--------|:-------------:|
| Abrir fichero | Prompt | e rutafichero |


## Copiar, Pegar y Poder Modificar (homenaje a la Hackmeeting)

No se puede seleccionar ni copiar en el modo Insert.

| Acción                                        | Modo           | Tecla/Comando |
|-----------------------------------------------|----------------|:-------------:|
| Copiar caracter debajo cursor                 | Normal         | `y`           |
| Copiar texto seleccionado                     | Visual         | `y`           |
| Copiar la linea completa                      | Normal         | `yy`          |
| Copiar la linea del cursor al final linea     | Normal         | `y$`          |
| Copiar la linea del cursor al principio linea | Normal         | `y0`          |
| Cortar texto seleccionado o caracter debajo   | Visual         | `d`           |
| Cortar la linea completa (borra la linea)     | Normal         | `dd`          |
| Pegar                                         | Normal         | `p`           |
| Borrar un caracter                            | Normal         | `x`           |
| Borrar texto seleccionado o caracter debajo   | Visual         | `x`           |
| Copiar todo el buffer                         | Prompt         | `%y`          |
| Pone en mayusculas el texto seleccionado      | Visual         | `U`           |
| Pone en minusculas el texto seleccionado      | Visual         | `u`           |
| Hace sangria (pero con tabs) a la derecha     | Visual         | `>`           |
| Hace sangria (pero con tabs) a la izquierda   | Visual         | `<`           |
| Volver a seleccionar lo último seleccionado   | Normal, Visual | `gv`          |

**Remplazar**: copias y pasas a Modo Remplazar, despues `ctrl+R` aparece un " y si pulsas 0 (que es el portapapeles por defecto) y remplaza.

**Remplazar**: copias y pasas a modo visual, seleccionas el texto y después pulsas `p`.

Se puede pegar en Modo Insert usando el `ctrl`+ `r` y el aparecer " y pulsas `0` o `"`.

Al ser un terminal funciona el copiar y pegar del escritorio en terminal, que es: `ctrl` + `shiftp  + `c` copiar, `ctrl` + `shift + `v` pegar, cortar no funciona.

### Copiar y pegar con el portapapeles de GNU/Linux

Vim necesita estar compilado con el clipboard, pero neovim no, yo tengo añadido en un acceso directo para menos teclas (con `<leader>` key que le he puesto la `,` como mucha gente).

| Acción | Modo           | Tecla/Comando |
|--------|----------------|---------------|
| Copiar | Visual         | "+y           |
| Pegar  | Normal, Visual | "+p           |
| Copiar | Visual         | `<leader>c`   |
| Pegar  | Normal, Visual | `<leader>p`   |

## Hacer y deshacer

| Acción   | Modo   | Tecla/Comando |
|----------|--------|:-------------:|
| Deshacer | Normal | u             |
| Rehacer  | Normal | ctrl + r      |

## Buscar y remplazar

Buscar si se pulsa `/` o `?` (para buscar hacia atras) va a la cajita de comando para escribir la busqueda (También si ya estas en la cajita de prompt `:` ) .

Para iniciar la busqueda pulsar `enter` .

| Acción                              | Modo   | Tecla/Comando |
|-------------------------------------|--------|:-------------:|
| Buscar Adelante                     | Normal | n             |
| Buscar Atras                        | Normal | shift + n     |
| Quitar resaltado la última busqueda | Prompt | :noh          |

Y se puede ir hacia atras y adelante ( con `arriba` y `abajo`) en el historial del busqueda cuando estas ya con `/` en la barra.

La busqueda es regex directamente pero es sensible a mayusculas o minusculas vía `:set noic` o insensible a mayusculas o minusculas `:set ic`.

En modo normal si vuelves a pulsar `n` o `N` se vuelve a relanzar la última busqueda y resaltar en el vim/neovim....al relanzar puedes cambiar el funcionamiento de la última que sea insensible o no a mayusculas.

No tengo en cuenta en varios ficheros o busquedas en directorio de proyecto, lo veo mas adelante.

**SIN PLUGINS DE FABRICA NO BUSCA DENTRO UN BLOQUE DE TEXTO SELECCIONADO POR VISUAL**

| Acción                                                | Modo   | Tecla/Comando |
|-------------------------------------------------------|--------|:-------------:|
| Busca la palabra que hay encima cursor hacia adelante | Normal | *             |
| Busca la palabra que hay encima cursor hacia atras    | Normal | #             |

Buscar el texto seleccionado hay que seguir los siguientes pasos:
- Entrar Modo Visual
- Selecciona
- Pulsa `y` para copiar
- Pulsa `/` para empezar con la cajita de busqueda
- Pulsa `ctrl + r` y pegara un `"` que es el punto donde pegara el texto.
- Pulsa `"` para que pegue el texto, si pulsas `esc` u otras teclas desaparece
- Busca con `enter` o mejora la busqueda

### Remplazar

Remplazar tiene una sintaxis tal que
`<rango>s/<regex>/<cadena_remplazar>/<opciones>`

Rango:
- . : linea actual
- % : todo el fichero
- Remplazar en la selección: entras en Modo Visual y seleccionas lo que quieras y pulsas `:` y ya sale el `'<,'>` que es el rango de buscar dentro de la selección, despues puedes escribir tu sentencia para remplazar `'<,'>/PRUEBA/CACA/gc`.

Opciones:
- g : en toda la linea
- c : pide confirmación
- i : insensible mayusculas y minusculas

# Ayuda offline

Desde la caja de comando:
- `:help` : muestra el inicio de ayuda. Partiendo la ventana en dos, dejando lo que este abajo.
- `:help <comando>` : ayuda sobre un comando, funciona el `tab` para autocompletar.
- se puede buscar con `/` en la ayuda.
- `:q` para salir de la ayuda.

- `:helpgrep <texto>` : buscar dentro de la ayuda por un texto.
  - Saca la primera coincidencia.
  - `:copen` : muestra una ventana con los ficheros de ayuda que ha encontrado. Se puede navegar arriba y abajo y `enter` para mostrarla.
  - `:cclose` : cierra la ventana de ficheros anterior.
- `:help quickref` : lanza una ayuda rápida.
- `:Tutor`: un tutorial interactivo. 

## Navegar ayuda

La ayuda tiene como *"links"* que estan resaltados en azul.

- ir al enlace: el cursor sobre el enlace `ctrl + ]` teclado español `ctrl + altgr + +`
- volver: `ctrl + o`


# Varios ficheros

- Buffer: un fichero abierto o sin nombre.
- Ventana: un trozo de la pantalla (rollo tiles escritorio i3 o tmux).
  - puede tener el mismo fichero a la vez abierto en varias ventanas y se va refrescando a la vez
- Pestaña: agrupación de ventanas.

**OJO**: Hay que meter en la configuración la siguiente linea `set hidden` para poder pasar entre buffers aunque tengan cambios. En nvim se guarda `~/.config/nvim/init.vim` .

**OJO BUSCAR Y REMPLAZAR EN VARIOS FICHEROS**: Sin plugins no hay una manera fácil de buscar en varios ficheros a la vez. Hay código como `:bufdo vimgrepadd <texto> % | copen` o `:cex [] | bufdo vimgrepadd /<texto>/g % | cw` pero es un jaleo.

## Buffers

- Abrir varios ficheros desde linea de comando `nvim <fichero1> <fichero2> <fichero3>` los abre en buffers y te muestra en pantalla completa el primero `<fichero1>`.
- Abrir otro fichero es `:e <fichero>` y lo muestra.
- `:ls` `:buffers` : muestra una lista de los buffers.
 - La lista de buffers es `<num> <estado> <fichero> <linea esta cursor>
   - Estados:
     - % : el que se esta mostrando.
     - a : el que esta activo.
     - h : el que oculto, no se en que influye.
     - `#` : algo para saltar pero no funciona porque es `ctrl + ^` y en el teclado español no hay esa tecla directamente.
     - `+` : si el buffer tiene alguna modificación.
     - = : si el buffer esta en modo lectura.

- `:e <fichero>`: abre para editar un fichero en un buffer en la ventana actual.
- `:enew`: abre un buffer vacio nuevo en la ventana actual.
- `:b <num_buffer>` o `:b <fichero>` (`tab` autocompletar): para ir directamente a ese buffer.
- `:bn` o `:bnext` : va al siguiente buffer.
- `:bl` o `:blast` : va al anterior buffer.
- `:bd` o `:bdelete`: cierra el buffer actual.
- `:bd <num>` o `:bd <fichero>` : cierra el buffer al que identifica el número o que ha abierto el fichero.
- `:e` : recarga el fichero con los cambios externos al vim/nvim.
- `:e!` : recarga el fichero con los cambios externos al vim/nvim y descarta los que se hayan hecho dentro.
- `:bufdo :<comandos>` : hace los comandos pasados en todos los buffers.

(POR HACER BUSCAR Y REMPLAZAR EN LOS BUFFERS)

## Ventanas

- Cuando divides una ventana no abre buffer nuevo. 
- Mover un buffer a una ventana: `:vsplit | buffer <num o fichero>` (**OJO** que el `|` no es tubería de shell es para concatenar comandos).
- No hay zoom a ventanas (de fábrica, con plugins si), hay maximización es dentro de su rama de ventanas (ver adelante).

La división es parecida a como gestiona los tiles el escritorio i3, por dentro es un arbol, de una pantalla mientras se divida de la misma manera (todas horizontales...) son hojas del mismo nivel, si se cambia a otra forma se produce en la hoja (ventana activa) se produce debajo una rama con las nuevas hojas de la partición.

```
+------+
|      |
|   R  |       R
|      |
+------+
+--+---+
|  |   |       
| 1|  2|       /\
|  |   |      1  2
+------+
+--+---+
|  | 2 |       /\
| 1|---|      1 /\
|  | 3 |       2  3
+------+
```

### Dividir ventanas

- `:split`: divide en horizontal.
- `:vsplit` : divide en vertical.
- `:split <fichero>` o `:vsplit <fichero>` : divide y abre el fichero que se le pase. 
- `:new`: divide en horizontal y el nuevo es un buffer vacio.
- `:vnew`: divide en vertical y el nuevo es un buffer vacio.
- `:only` : cierra todas las ventanas menos la activa (la que tiene el cursor).
- `:quit` o `:q` : cierra la ventana actual, si es la última cierra nvim ...si no esta modificado el buffer.
- `:qall` o `:qall!` : cierra todas las ventanas.


### Moverse ventanas y accesos directos

| Acción                                   | Modo   | Tecla/Comando                  |
|------------------------------------------|--------|:------------------------------:|
| Divide en horizontal                     | Normal | ctrl + w s                     |
| Divide en vertical                       | Normal | ctrl + w v                     |
| Cierra todas menos la activa             | Normal | ctrl + w o                     |
| Cierra la ventana activa                 | Normal | ctrl + w q                     |
| Maximiza la ventana activa (en su rama)  | Normal | ctrl + w _                     |
| Iguala las ventanas de una rama          | Normal | ctrl + w =                     |
| Moverse a la siguiente ventana           | Normal | ctrl + w w , ctrl + w ctrl + w |
| Moverse a la anterior ventana            | Normal | ctrl + w shift + w             |
| Moverse a la ventana arriba              | Normal | ctrl + w arr , ctrl + w k      |
| Moverse a la ventana abajo               | Normal | ctrl + w abj , ctrl + w j      |
| Moverse a la ventana izquierda           | Normal | ctrl + w izq , ctrl + w h      |
| Moverse a la ventana derecha             | Normal | ctrl + w der , ctrl + w l      |
| Intercambiar ventana con la siguiente    | Normal | ctrl + w r                     |
| Agrandar horizontal ventana activa       | Normal | `ctrl + w <`                   |
| Encoger horizontal ventana activa        | Normal | `ctrl + w >`                   |
| Agrandar verticalmente ventana activa    | Normal | ctrl + +                       |
| Encoger verticalmente ventana activa     | Normal | ctrl + -                       |
| Intercambiar/rotar ventanas (en su rama) | Normal | ctrl + w r                     |

## Pestañas

- Puedes abrir en pestañas directamente cada fichero a una pestaña con `nvim -p <fichero1> <fichero2> <fichero3>
- `:tabnew` : abre una nueva pestaña vacia.
- `:tabnew <fichero>` : (soporta tabulador) abre un fichero en una pestaña nueva, si ya esta en la lista de buffers, no.
- `:tabs` : lista las pestañas que hay con sus ventanas.
- `:tabnext` o `:tabn` : siguiente pestaña.
- `:tabprevious` o `:tabp` : anterior pestaña.

| Acción                                | Modo   | Tecla/Comando |
|---------------------------------------|--------|:-------------:|
| Siguiente pestaña                     | Normal | gt            |
| Anterior pestaña                      | Normal | gT            |
| Pestaña `<num>`                       | Normal | `<num>gt`     |
| Mover una ventana a una pestaña nueva | Normal | ctrl + w + T  |

**OJO** No se puede mover una ventana a una pestaña existente (sin plugins).

## Sesiones

Sin plugins se puede guardar como esta el nvim de buffers abiertos, pestañas y ventanas con:

 - `:mksession` : guarda en el directorio de trabajo un `Session.vim`.
 - `:mksession!` : sobreescribe el fichero `Session.vim`.
 - `:mksession <fichero>` : guarda el fichero de sesión en un fichero.
 - `nvim -S ` : carga el fichero de sesion que en el sitio que ejecutes nvim/vim.
 - `nvim -S <fichero>` : carga la sesión guardada en un fichero.

Ojo que puedes tener varios ficheros de sesión, eso es bueno y malo.

Con un plugin llamado `vim-workspace` se guarda la sesión automáticamente al salir y carga al entrar (sin -S), y solo es una sola.

Con `vim-workspace` tienes los comandos:
- `:CloseHiddenBuffers` : cierra todos los buffers que no tienen ventana enganchada.

# Otros

- `:view` : pone en modo lectura el buffer actual (en todas las ventanas), se queja si hay algún cambio.
- `:edit` : vuelve a modo escritura el buffer actual.
- `:term` : abre un terminal (puedes abrir un neovim/vim dentro de el matrioska), ojo que lo abre en la ventana actual remplazando lo que hay, partiendo la ventana se puede apañar rollo tmux. Y para usarlo hay que pulsar `i` y para salir del terminal y cerrar la ventana pues `exit` o `ctrl + d` pero sin cerrar el terminal y volver al rollo vim, es `ctrl + \ ctrl + n` en un teclado español `ctrl + altgr + º ctrl + n`. 

- `:set number` : muestra las lineas de números en la derecha.
- `:set nonumber` : oculta los números de linea de la derecha.
- `:set relativenumber` : muestra los números de linea relativos a la posición del cursor, si se combina con `:set number` sale la linea actual en vez de 0.
- `:set norelativenumber` : oculta los números de linea relativos.
- `:options`: abre una ventana con la lista de opciones con una pequeña descripción, puedes ir cambiandolas para ver como cambia, pero no guarda.
- `:set <option>?` : imprime el valor de la configuración.
- `nvim +<comando>` : abre el nvim/vim ejecutando ese comando que ejecutarías en la cajita de comandos en plan `:<comando>`.
- `:set readonly` : pone en modo lectura el buffer actual. Aparece en la barra de estado `[RO]` y en la lista de buffers `=` .
- `:set noreadonly` : vuelve a poner en modo lectura y escritura el buffer actual.
- `:<num>` : salta a la linea que se le pasa como número.
- `:set filetype?` : devuelve el tipo de fichero.
- `:set fileencoding?` : devuelve la codificación del fichero.
- `:set fileformat?` : devuelve el tipo de retorno de carro (unix y los otros).
- `:checkhealth` : (solo nvim) abre una pestaña con un listado de chequeos que hace, muy interesante para saber donde esta el fichero de configuración.
- `nvim --clean` : inicia el nvim sin plugins y configuraciones.
- `.` : repetir la última acción.
- `:%y` : copiar todo el buffer actual.
- `:echo expand('%:p')` : sacar el fullpath del buffer actual.
- `nvim -V9<fichero.log>` : para lanzar el vim/nvim y que vaya logueando las cosas (útil para plugins y cosas que fallan mal). El 9 es el nivel máximo de log.
- `:nmap` : abre una pantalla listando (sin mucha descripción) las teclas de acceso directo.
- `:r ! echo %` : pega el nombre del fichero con el path en la posición del cursor.
- `ctrl` + `g` : muestra en la barra de estado el nombre del fichero y otros datos.

## Links de interes

- https://vimawesome.com/ : listado de  plugins.
- https://github.com/amix/vimrc/blob/master/vimrcs/basic.vim : una configuración de vim famosa.
- https://github.com/rockerBOO/awesome-neovim : un awesome sobre neovim.

## Ejecución

A parte de lo de `:term`, vim/neovim tiene la cosa potente (una de las muchas killer features) es trabajar como en bash pipeando comandos de shell tanto de entrada como de salida.

Ojo que el vim/nvim ejecuta desde el directorio de trabajo, para saberlo es `:pwd` y para cambiarlo es como shell `:cd <directorio>` soporta tabulador y `~` para home.

- `:! <comando o pipe de comandos>` : saca la salida por abajo en la caja ampliandola si es necesario.
- `:r ! <comando o pipe de comandos>` : coge la salida y la pega en donde esta el cursor.
- `:w ! <comando o pipe de comandos>` : mete el buffer entero como entrada (stdio) del comando.
  - si seleccionas en Modo Visual y después pulsas `:` y aparece `:'<,'>` y solo hay que añadirle `:'<,'> :w ! <comando o pipe de comandos>` .
- ejecutar texto seleccionado tanto en `r` como `w` es parecido a buscar texto seleccionado:
  - se entra en Modo Visual, se selecciona el texto y se copia con `y`.
  - se entra en cajita comando `:` escribes lo que quieras de ejecución y se pulsa `ctrl+r` aparece el caracter `"`
  - se pulsa `"` y aparece lo copiado.
  - ejecutas

Puedes hacer varias ejecuciones una detrás de otra, al terminar vuelves a pulsar `:` y se mantiene la cajita de abajo y puedes empezar con la siguiente.

## Ortografía

- Setear y poner la ortografía española `:setlocal spell spelllang=es` (pero no queda guardado en la configuración), si no esta el paquete de ortografía lo descarga y lo guarda en un directorio local (avisa cual es).
- `:set spell` (o el anterior) : resalta en rojo las faltas ortográficas.
- `:set nospell` : quita el resaltado en rojo.


| Acción                                                       | Modo   | Tecla/Comando                                         |
|--------------------------------------------------------------|--------|:-----------------------------------------------------:|
| Ir a la siguiente falta ortográfica                          | Normal | altgr + + (teclado español) s , ]s (teclado ingles)   |
| Ir a la anterior falta ortográfica                           | Normal | altgr + \` (teclado español) s , \[s (teclado ingles) |
| Abrir pantalla sugerencias (después elegir número)           | Normal | z=                                                    |
| Aceptar y guardar el palabro como bien                       | Normal | zg                                                    |
| Aceptar y guardar el palabro como mal (incluso no subrayado) | Normal | zw                                                    |

*OJO:* nvim guarda las palabras nuevas en `/home/<user>/.config/nvim/spell/es.utf-8.add` es un fichero de texto normal, por si añades una palabra que hace sangrar los ojos al cuñado facha Perez Reverte. Y vim en `/home/<user>/.vim/spell/es.utf-8.add`

Y el fichero que descargar de ortografía que descarga esta dentro del directorio `~/.local/share/nvim/site/spell`, se puede borrar el contenido si tienes algún problema.

## Leader (que no leather) key

Es una tecla para iniciar atajos de teclado o accesos directos que usan plugins y los accesos directos que metas en la config.

Normalmente la gente mete la tecla , (coma).

Y solo funciona en el modo normal.

## Configuraciones

Aquí la gente se vuelve loca pero es mejor ir a lo mínimo.

Para nvim esta:
`~/.config/nvim/init.vim`

- `:so $MYVIMRC` : recargar la configuración en vivo de vim/nvim (Ojo que aplica solo a ventana activa).

Mi configuración de nvim:

```
"CONFIG NVIM MIGUEL DE DIOS

set title "MUESTRA EL TITULO EN LA VENTANA DE TERMINAL
set number "MUESTRA EL NÚMERO DE LINEA
set cursorline  "RESALTA LA LINEA ACTUAL
set colorcolumn=80 "MUESTRA LA LINEA DE 80 CARACTERES

set hidden  "PERMITE CAMBIAR DE BUFFER SIN PEDIR GUARDARLO
set spell
set spelllang=es

set ignorecase  "BUSCA CASE INSENSITIVE
set smartcase  "BUSCA CASE SENSITIVE SI HAY MAYUSCULAS EN LA BUSQUEDA

set termguicolors "ACTIVA TRUE COLOR Y MEJORA COMO QUE PONE SUBRAYADOS EN LAS FALTAS ORTOGRÁFICAS
" AL SETEAR EL ANTERIOR SE PIERDE EN LAS PESTAÑAS NO ACTIVA EL COLOR Y NO SE LEE

colorscheme industry "SOLUCION PARA VER LAS PESTAÑAS

"DESHABILITA EL SWP FILE QUE SIEMPRE DA LA COÑA
set nobackup
set nowb
set noswapfile

"SÍMBOLO DE INICIO CUANDO SE DESBORDA LA LINEA
set showbreak=↪\ 
"SÍMBOLOS PARA MOSTRAR TABULADORES, ESPACIOS DESPUÉS DEL FINAL DE LINEA,
"CUANDO NO DESBORDA LA LINEA Y ESPACIO
set listchars=tab:→\ ,trail:.,extends:⟩,precedes:⟨
"ACTIVAR QUE SE VEAN
set list

"set autochdir "PONE EL DIRECTORIO DE TRABAJO (PARA EJECUTAR COSAS) EN EL MISMO QUE EL BUFFER EN EDICIÓN

"PLUGINS
call plug#begin()
Plug 'Yggdroot/indentLine'
Plug 'thaerkh/vim-workspace'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-sleuth'
Plug 'alvan/vim-closetag'
Plug 'mattn/emmet-vim'
Plug 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
        \ 'do': 'bash install.sh',
        \ }
Plug 'ncm2/ncm2'
Plug 'roxma/nvim-yarp'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
call plug#end()

"INDENTLINE
let g:indentLine_showFirstIndentLevel = 1
let g:indentLine_leadingSpaceEnabled = 1
let g:indentLine_leadingSpaceChar = '·'

"VIM WORKSPACE
let g:workspace_autosave = 0 "EVITA QUE EL PLUGIN HAGA EL GILIPOLLAS COMO LOS PYCHARM DE GUARDAR SOLO
let g:workspace_autocreate = 1 "SI NO EXISTE CREA EL FICHERO DE SESIÓN
let g:workspace_session_name = stdpath('data') . '/Session.vim' "RUTA AL FICHERO SE SESIÓN

"LSP
"https://langserver.org/
"Python: https://github.com/python-lsp/python-lsp-server
let g:LanguageClient_serverCommands = {
    \ 'python': ['/usr/local/bin/pylsp']
    \ }

function LC_maps()
  if has_key(g:LanguageClient_serverCommands, &filetype)
    nmap <buffer> <silent> K <Plug>(lcn-hover)
    nmap <buffer> <silent> gd <Plug>(lcn-definition)
    nmap <buffer> <silent> <F2> <Plug>(lcn-rename)
  endif
endfunction
autocmd FileType * call LC_maps()

" enable ncm2 for all buffers
autocmd BufEnter * call ncm2#enable_for_buffer()

" IMPORTANT: :help Ncm2PopupOpen for more information
set completeopt=noinsert,menuone,noselect

"TECLAS DE ACCESO DIRECTO

let mapleader = "," "TECLA DE ACCESO DIRECTO

"GUARDADO RÁPIDO
nmap <leader>w :w!<cr>

"SALIR YA GUARDANDO
nmap <leader>q :wqall<cr>

"SALIR YA SIN GUARDAR
nmap <leader>Q :qall!<cr>

"FZF
nmap <leader>f :FZF<cr>
nmap <leader>b :Buffers<cr>
nmap <leader>r :Rg<cr>
```

# Plugins

- No hay un gestor de plugins en plan pip, cargo o npm, tienes que ir metiendolos en el config y ejecutar la instalación.
- Los plugins son scripts que se guardan en home del usuario en un subdirectorio de vim/nvim.
- De los existentes, apuesto por https://github.com/junegunn/vim-plug

## Instalación vim-plug

`curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim`

Y ya esta pero no funciona ningun comando de plug si no hay metido ningun plugin en el init.vim (neovim).

## Config vim-plug

Hay que meter un bloque de código para vimplug en el config tal que asi:

```
call plug#begin()
Plug 'Yggdroot/indentLine'
Plug 'blablaba'
call plug#end()
```

## Uso vim-plug

- `:PlugInstall` : instala los plugins que hay en el config (ojo que abre una nueva ventana).
- `:PlugClean` : borra los plugins que ya no estan en el config.
- `:PlugUpdate` : actualiza los plugins del config.
- `:PlugUpdate` : actualiza el vim-plug.

Uso en caliente de un plugin nuevo:
- abrir el init.vim (si es nvim).
- añadir el plugin.
- y estando en el mismo panel `:source %` para recargar esta configuración.
- `:PlugInstall`

## Plugins de interes

- `thaerkh/vim-workspace` : guarda y carga automáticamente la sesión.
- `Yggdroot/indentLine` : muestra la linea de identación vertical.

# IDE

- vim/nvim automáticamente ya resaltan (le meten colorines) el código fuente. Incluso las aperturas y cierres de parentesis y etiquetas html/xml.

(¿LINTEO?)

## Comentar código

De fábrica no hay nada pero con el plugin `tpope/vim-commentary` se puede tener:
- selecciona las lineas `shift + v`
- `gc` en modo visual y comenta esas lienas
- `gc` otra vez y descomenta


## Tabular bloques
- selecciona las lineas con `shift + v` para pasar a modo visual line
- pulsa `>` para mover a la derecha
- pulsa `<` para mover a la izquierda
+- pulsa `.` para seguir repitiendo el movimiento

No acaba de funcionar bien, hay veces que mete 2 tabuladores en vez de espacios.

El plugin que uso para que funcione bien es `tpope/vim-sleuth`.

## Autocerrado de etiquetas html

Uso el plugin `alvan/vim-closetag`, lo unico que necesita que el fichero sea .html o así, pero se puede activar en el buffer actual con `:CloseTagEnableBuffer`.


## Autocompletado

### Emmet

Hay un plugin `mattn/emmet-vim` que permite usar emmet y después en la cadena de emmet (por ejemplo div.main ) si se pulsa `ctrl + y ,` se transforma en `<div class="main"></div>` .

### Autocompletado

Sin plugins tienes el `ctrl + n` en modo insertar que coge el trozo de palabra y busca entre los buffers para darte una lista a lo bruto de posibles palabros para autocompletar.

### LSP

Por desgracia micro$oft ha hecho una tecnología para unir a todos rollo anillo. Es LSP, en nvim (en la siguiente versión viene integrado) se divide en 2 plugins y N *"server"* para montarlo:
 - https://github.com/autozimu/LanguageClient-neovim : para enganchar con los servers que se pongan y realizar las acciones de "goto definition", "hover", "rename" y otras.
 - https://github.com/ncm2/ncm2 : el que hace que salga  un popup de sugerencias de autocompletado.

- `shift + k` : muestra info de lo que hay encima del cursor.
- `g d`: va a la definición de la función o lo que haya encima del cursor.
- `F2` : renombrar (en plan refactorización masivo) una función o lo que tengas encima del cursor.

Algunas veces algún lsp de algún lenguaje no funciona, lo mejor es habilitar el log (que es suyo y no usa el de nvim/vim):
 - En el `~/.config/nvim/init.vim` meter las lineas:
   ```
   " 'DEBUG' | 'INFO' | 'WARN' | 'ERROR'
   let g:LanguageClient_loggingLevel='WARN'
   let g:LanguageClient_loggingFile = '/tmp/lg.log'
   ```

# Búsqueda difusa

Hay que instalar `fzf` https://github.com/junegunn/fzf.vim y también hay que instalar `ripgrep`.

| Acción                                            | Modo   | Tecla/Comando                   |
|---------------------------------------------------|--------|:-------------------------------:|
| Fuzzy buscar nombre ficheros (sin path es `:pwd`) | Normal | `:FZF <path>` o `:Files <path>` |
| Fuzzy buscar nombre ficheros                      | Normal | `<leader> f`                    |
| Fuzzy buscar buffers                              | Normal | `:Buffers`                      |
| Fuzzy buscar buffers                              | Normal | `<leader> b`                    |
| Fuzzy buscar contenido ficheros (en el `:pwd`)    | Normal | `:Rg`                           |
| Fuzzy buscar contenido ficheros (en el `:pwd`)    | Normal | `<leader> r`                    |

Sale un popup muy elegante con el buscador fzf o ripgrep para ir tecleando y buscando, tiene los siguientes atajos de teclado:
- `<enter>`: abre el fichero en la misma ventana.
- `ctrl + t`: abre el fichero en una pestaña nueva.
- `ctrl + x`: abre el fichero en una ventana horizontal nueva.
- `ctrl + v`: abre el fichero en una ventan vertical nueva.
- `esc` : cerrar el popup.
