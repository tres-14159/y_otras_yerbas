# Notes and tips

[[_TOC_]]

## Git

### Automatic add *ficheros basura* to .gitignore

```
$ git status -s | cut -d" " -f2 >> .gitignore
```

### Show in leafpad the changes of file in each commit

It shows a several versions of file in a light desktop editor.

```
$ for i in $(git log --follow --format=format:%H file.txt); do echo "SHOW FILE VERSION: ${i}"; git show $i:file.txt | leafpad; done
```

### Remove branches not in remote

```
$ git remote prune origin
$ git branch -vvv
# branch with [blabla :gone] is deleted in remote

$ git fetch -p && for branch in $(git branch -vv | grep ': gone]' | awk '{print $1}'); do git branch -D $branch; done
```

### Compare remote tags opossite local tags

```
$ diff <(git ls-remote -t 2>/dev/null | cut -f2 | cut -d'/' -f3) <(git tag) -y
```

## Python

### Flake8

Show as html the warnings and erros of flake8. You need https://pypi.org/project/flake8-html/ .

```
flake8 --config=project/.flake8 project/ --format=html --htmldir=/tmp/; xdg-open /tmp/index.html 
```

### A CSV (into a list/array) with head to list of dict

```
c = [['aa', 'bb', 'cc', 'dd'],
 [0, 1, 2, 3],
 [1, 2, 3, 4],
 [2, 3, 4, 5],
 [3, 4, 5, 6]]
 
[{field : value for field in c[0] for value in row} for row in c[1:]]

[{'aa': 3, 'bb': 3, 'cc': 3, 'dd': 3},
 {'aa': 4, 'bb': 4, 'cc': 4, 'dd': 4},
 {'aa': 5, 'bb': 5, 'cc': 5, 'dd': 5},
 {'aa': 6, 'bb': 6, 'cc': 6, 'dd': 6}]

```

### Pandas dataframe merge two dataframes by id and you can filter by if there is in one or both or only left.

```
import pandas

a = pandas.DataFrame([{"a": 1, "b": True}, {"a": 2, "b": False}, {"a": 3, "b": True}])
b = pandas.DataFrame([{"a": 1, "b": True}, {"a": 2, "b": False}])

c = pandas.merge(a, b, on='a', how='left', indicator=True)

c
Out[8]: 
   a    b_x    b_y     _merge
0  1   True   True       both
1  2  False  False       both
2  3   True    NaN  left_only

c[c['_merge'] == 'both']
```

## Bash

### head and tail

Show file from n line of begining to m line of end.

Example: 3º line of begin and except last two lines of end.

```
 cat /tmp/111 | tail -n +3 | head -n -2
```

---

Show file the first n lines

Example: 4 first lines.

```
 cat /tmp/111 | head -n 4
```

---

Show file the without last n lines

Example: whithout the last 4 lines.

```
 cat /tmp/111 | head -n -4
```

---

Show file whithout first n lines.

Example: whithout the first 4 lines.

```
 cat /tmp/111 | tail -n +4
```

---

Show file last n lines.

Example: whithout last 4 lines.

```
 cat /tmp/111 | tail -n 4
```

### date


Get timestamp now

```
 date +%s
```

----

Get timestamp from a specify date

```
 date +%s --date='2020-08-31 10:11:16'
```

### seq

Loop from 13 to 7 (reverse loop).

```
$ for i in $(seq -f "%02g" 13 -1 7); do echo "aaa.$i.md"; done
aaa.13.md
aaa.12.md
aaa.11.md
aaa.10.md
aaa.09.md
aaa.08.md
aaa.07.md
```

### A complex example to move a files with padding 0.

```
for i in $(seq -f "%02g" 13 -1 9); 
do
    p=$(printf "%02i" $(echo "$i - 1" | bc)); 
    git mv "capitulo.$p.md" "capitulo.$i.md";
done
```

### Cut for a word (instead a char)

aaa SEPARATOR bbb

Print bbb
```
cat list.txt | awk -F "SEPARATOR" '{print $2}'
```

### Cut column for several spaces (when cut -d" " fails)

```
ls -l

-rwxrwxrwx 1 root root 3523263912 Feb  7 07:07 case-195858.zip
-rw-r--r-- 1 root root 2780701164 Feb  8 01:56 case-195879.zip
-rwxrwxrwx 1 root root          0 Jan 27 03:30 case-195999.zip
-rwxrwxrwx 1 root root 3448417863 Jan 26 15:42 case-196065.zip
-rw-r--r-- 1 root root 3516149895 Feb  8 02:24 case-196722.zip
-rwxrwxrwx 1 root root 2810360947 Feb  7 10:54 case-196867.zip
-rwxrwxrwx 1 root root 3703488999 Jan 24 04:58 case-197283.zip
```

```
ls -l | awk '{ print $5";"$9 }'

3940472659;case-193343.zip
0;case-193393.zip
2701683950;case-193475.zip
4050631505;case-193483.zip
2793671338;case-193830.zip
2794101074;case-193946.zip
2840664485;case-194200.zip
3880796866;case-194536.zip
3549803702;case-194815.zip
3628467010;case-195102.zip
3457575728;case-195166.zip
3251675236;case-195321.zip
3687964072;case-195339.zip
3380475991;case-195773.zip
3523263912;case-195858.zip
2780701164;case-195879.zip
0;case-195999.zip
3448417863;case-196065.zip
3516149895;case-196722.zip
2810360947;case-196867.zip
```

### Create a file with zeros o random data with a size

A file 4mb of zeroes
```
dd if=/dev/zero of=/tmp/fichero bs=1M count=4
```

A file 4mb of random
```
dd if=/dev/random of=/tmp/fichero bs=1M count=4
```

### Search files that they changed the last 2 days

Search files that they changes the last 2 days and print the timestamp, the time for humans and the name.

The list is sorted by timestamp and remove the timestamp field.

```
find . -ctime -2 -printf '%T@ %Tc %P\n' 2>/dev/null | sort -n | awk '{$1=""; print $0}'
``` 

## JS/Typescript

### Threejs debug scene tree

```
(
function printGraph( obj ) {
  let v = new THREE.Vector3();
  obj.getWorldPosition(v);
  console.group(obj.type + " (" + obj.name + ", (" + v.toArray() +"), %O)", obj);
  obj.children.forEach( printGraph );
  console.groupEnd();
}(scene));
```

Another debug scene tree:

```
(function printGraph( obj ) {
    
    console.group(obj.name + ' <%o> ', obj);
 
    obj.children.forEach( printGraph );
    
    console.groupEnd();

}(scene));
```

## Others

### Imagemagick

#### Generate random image

```
convert -size 100x100 xc: +noise Random noise.png
```

```
convert -size 120x120  plasma:fractal fractal.png
```

### PDF

#### Optimize PDF

```
ps2pdf input.pdf output.pdf
```

More but less quality:
```
ps2pdf -dPDFSETTINGS=/ebook input.pdf output.pdf
```

#### Remove borders PDF

Remove 300px bottom border
Remove 280px left border and 280px right border
Create pdf file with each page each image and fit to dinA4
Optimize size of pdf

```
convert -density 300 archivo.pdf pagina_%03d.png
mogrify -gravity south -chop 0x1100 pagina_*.png
mogrify -gravity west -chop 280x0 -gravity east -chop 280x0 pagina_*.png
pdfjam --outfile outfile.pdf --paper a4paper --fitpaper true pagina_*.png
ps2pdf outfile.pdf outfile.optimizada.pdf
```

# JQ

## List the keys of object recursive

```
$ jq 'paths | map(tostring) | join(".")' zzzz.json

$ echo '{"a": 111, "b": {"c": 222, "d": 333, "e": [2,3,4]}}' | jq 'paths | map(tostring) | join(".")'
"a"
"b"
"b.c"
"b.d"
"b.e"
"b.e.0"
"b.e.1"
"b.e.2"
```

