extends Control

var console = JavaScriptBridge.get_interface("console")
var JSON_js = JavaScriptBridge.get_interface("JSON")
var messageListenerCallback = JavaScriptBridge.create_callback(messageListener)

var portJS : JavaScriptObject

# Called when the node enters the scene tree for the first time.
func _ready():
	$Label.text = OS.get_name()
	if OS.has_feature('web'):
		console.log("ES WEB");
		JavaScriptBridge.get_interface("window").addEventListener('message', messageListenerCallback)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass
	
func messageListener(args) -> void:
	var js_event = args[0]
	if (js_event.data.origin == 'parent'):
		match js_event.data.data:
			"init" :
				var message = JSON_js.parse(JSON.stringify({'origin': 'iframe', 'data': 'inited'}))
				print(message)
				console.log(message)
				portJS = js_event.ports[0]
				portJS.postMessage(message)
				
				portJS.onmessage = messageListenerCallback
			#"image":
				 ## Crear una imagen
				#var image = Image.new()
				#var error = image.load_jpg_from_buffer(js_event.data.image)
				#if error == OK:
					#var texture = ImageTexture.new()
					#texture.create_from_image(image)
					## Asignar la textura al Sprite
					#$icon.texture = texture
				#else:
					#print("Error al cargar la imagen: ", error)
			_ :
				var data = JSON.parse_string(JSON_js.stringify(js_event.data.data))
				$message.text = data
		print("Message")
		print(js_event.data.data)
		console.log(js_event.data)
