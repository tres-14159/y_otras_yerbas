# Deeplearning: FROM BOTTOM TO THE TOP

Auto-tutorial, ejemplos de deeplearning.

Casi todos los tutoriales que encuentro montan unos chochos de escandalo
que si enganchar con GPU para aceleración gráfica, que si abrir sockets
en docker al GNU/Linux para la aceleración gráfica de cuda ,
que si montar jupyter, que si el puto conda (porque soy un gilipollas y
no se usar GNU/Linux y tengo que usar un asqueroso windows), que si
vamos ha hacer siempre el ejemplo de los perros y los gatos (y hay que
echar la ostia de código para procesar imágen). Una mierda.

Pues prefiero pasito a pasito, prefiero una red que no este optimizada
con la GPU e ir entendiendo.

## Este documento

Voy a intentar que sea rollo cuaderno de bitacora (aunque en teoría con
los comandos git no haría falta poner fechas), al final del documento
estan los cambios y no se hasta donde llegaré.

A donde me gustaría llegar, pues no lo se, me gustaría primero
entenderlo, me gustaría hacer inventos como lo la red neuronal que juega
al Mario https://www.youtube.com/watch?v=qv6UVOQ0F44 y ser capaz
de reconocer cosas en una imagen.

## Dudas

* ¿Como funciona el face remplace?
* Si una red neuronal "se compila" ya queda fija en el tiempo. ¿Cómo
se hace para que aprenda cosas nuevas? ¿Se vuelve a compilar?
  * O una red neuronal de recomendación de música...¿Como se va
  alimentando con los nuevos mp3s que escuchas?
* Ahora a parte del tema de tensorflow, keras y tal. ¿La vida a mejorado
  porque no te tienes que rayar (mirar formulas mátemagicas) con el
  entrenamiento? ¿Aplicas una plantilla y el solo lo entrena?

## Teoría

Mucho hemos estudiado en las universidades, modulos de formación
profesional sobre redes neuronales, teorías de hace 30 años, que si
el perceptron multicapa y tal.

* *Modelo*: es la red neuronal sin entrenar.

## Ejemplos

### Red neuronal que sume binario
¿Por que sumar binario y no sumar a secas? Porque asi la funcion de
activación es mas fácil.

01 + 01 = 010

## Bitacora
Para generar la fecha es: `$ date +%Y%m%d`

### 20190808

* Comenzando con este camino del conocimiento.
* Creando una imagen docker básica desde debian con python.
* El primer ejemplo que quería hacer es:
  * Una red neuronal que sume en binario

### 20190811
* He añadido muchas cosas al dockerfile (keras, tensorflow, jupyter
notebook, ipython...cosas básicas de unn GNU/Linux).
* Un script simple para levantar un jupyter notebook server con docker.
* He encontrado un tutorial muy simple pero que genera muchas dudas en:
https://www.aprendemachinelearning.com/una-sencilla-red-neuronal-en-python-con-keras-y-tensorflow/
  * Dudas que genera:
    * Entrena sobre toda la muestra, asi que no es un buen ejemplo, porque una red neuronal
      tiene que predecir resultados fuera de la muestra pero si le das todo ya lo único que
      hace (que no es poco) es aprender a casar resultados.
    * ¿Por qué esa forma de la red neuronal?
    * ¿Por qué "relu" de función de activación y porque "sigmoid" de activación?
    * ¿Por que esas opciones de entrenamiento?
    * ¿Como se engancha automágicamente al backend e tensorflow?
    * ¿Qué significa el output del entrenamiento?

### 20190812
* Resuelta una duda:
  * ¿Como se engancha automágicamente al backend e tensorflow?
    * He quitado del pip install el tensorflow y el keras en el ejemplo
    del xor y salen muchos fallos del keras porque usa por defecto el
    backend de Tensorflow.
    * Keras por defecto usa el backend tensorflow, lo descubrí quitando
    de la imagen de docker el tensorflow, el exemplo del xor petaba
    que faltaba el backend tensorflow y en algún punto ponia que era
    el backend por defecto. 
