use std::io;

fn main() {
	let mut first : f32;
	let mut second : f32;
	
	loop {
		println!("Calculator:");
		
		println!("First number:");
		let mut line = String::new();
		io::stdin()
			.read_line(&mut line)
			.expect("Failed to read line");
		line = line.trim().to_string();
		let result = line.parse();
		if let Err(result) = result {
			println!("It is not a number {}", result);
			continue;
		}
		first = result.unwrap();
		
		println!("Operation:");
		let mut line = String::new();
		io::stdin()
			.read_line(&mut line)
			.expect("Failed to read line");
		let operation = line.trim();
		
		println!("Second number:");
		let mut line = String::new();
		io::stdin()
			.read_line(&mut line)
			.expect("Failed to read line");
		line = line.trim().to_string();
		let result = line.parse();
		if let Err(result) = result {
			println!("It is not a number {}", result);
			continue;
		}
		second = result.unwrap();
		
		let solution = match operation {
			"+" => Some(first + second),
			"*" => Some(first * second),
			"/" => Some(first / second),
			"%" => Some(first % second),
			"^" => Some(f32::powf(first,second)),
			_ => None
		};
		if solution.is_none() {
			println!("It is not a valid operation {}", operation);
			continue;
		}
		let solution = solution.unwrap();
		
		println!("{} {} {} = {}", first, operation, second, solution);
	}
}
