function mostrarMensaje(mensaje: string) {
  const outputElement = document.getElementById("output");
  if (outputElement) {
    outputElement.textContent = mensaje;
  }
}

window.addEventListener("load", () => {
  mostrarMensaje("¡¡BILL GATES, JOBS TE ESTA ESPERANDO!!");
});
