# Typescript minimal boiler plate

```console
$ npm init -y
$ npm install typescript
$ npm install http-server
$ npx tsc --init
```

Add to `tsconfig.json` the config:
```json
    "outDir": "./dist",
    "rootDir": "./src"
```
