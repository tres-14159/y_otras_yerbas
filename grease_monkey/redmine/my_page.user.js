/*
unnamed
Copyright (C) 2018 Miguel de Dios Matias

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

// ==UserScript==
// @name     Mi página redmine
// @version  1
// @grant GM.xmlHttpRequest
// @grant GM.getValue
// @grant GM.setValue
// @grant GM.listValues
// @grant unsafeWindow
// @include http://10.0.20.10/redmine/my/page*
// @require https://code.jquery.com/jquery-3.3.1.js
// ==/UserScript==

// TODO
// CHECK VERSION OF REDMINE
// ADD A EDITOR TO SAVE CHANGES
// ADD FOR ANY INSTALLATION OF REDMINE
// ADD A FORM TO ADD HOURS

let count_widgets_top = $("#list-top .mypage-box").length;
let count_widgets_left = $("#list-left .mypage-box").length;
let count_widgets_right = $("#list-right .mypage-box").length;

let id_user = $("#top-menu a.user.active").attr("href").split("/").pop();

// --- INIT - HTML "widgets" -----------------------------------------------------------------

$block = $("\
<div class='mypage-box'>\
	<h3>TITLE</h3>\
	<div>CONTENT</div>\
</div>");

$edit_img = $("<img>").attr("src", "../images/edit.png");

// --- END - HTML "widgets" -----------------------------------------------------------------

// --- INIT - BLOCK USEFUL LINKS ----------------------------------------------------------------
useful_links = GM.getValue("useful_links", null);

useful_links.then(function(list_useful_links) {
  
  if (list_useful_links === null) {
  	// My own useful links
    list_useful_links = [];
    list_useful_links.push({"href": "http://10.0.20.10/intranet/", "text": "Intranet"});
    list_useful_links.push({"href": "http://10.0.20.10/redmine/projects/interno/wiki/Plantilla_An%C3%A1lisis", "text": "Plantilla de análisis"});
    list_useful_links.push({"href": "http://10.0.20.10/redmine/projects/interno/wiki", "text": "Wiki Unlimiteck"});
    
    GM.setValue("useful_links", list_useful_links);
  }
  
  
  $useful_links = $block.clone();
  $("h3", $useful_links).html("Enlaces interesantes").append($("<a>").attr("href", "#").click(mio).append($edit_img));
  $("div", $useful_links).empty().append(
    $("<ul>").attr("id", "useful_links")
  );
  
  
  $("div", $useful_links).append(
    $("<table>").attr("id", "useful_links_editor").attr("width", "100%").append(
      $("<tr>")
      	.append($("<td>").html("<b>URL</b>"))
      	.append($("<td>").html("<b>TEXT</b>"))
      	.append($("<td>").html(""))
		)
    .hide()
   );
  
  
  $.each(list_useful_links, function(i, link) {
    $("#useful_links", $useful_links).append($("<li>").append($("<a>").attr("href", link.href).html(link.text)));
    
    $("#useful_links_editor", $useful_links).append(
      $("<tr>")
      	.append($("<td>")
         	.append(
          	$("<input>").attr("type", "text").val(link.href)
        	)
        )
      	.append($("<td>").html(link.text))
      	.append($("<td>").html("aaaa"))
    );
  });

  $("#list-right").prepend($useful_links);
});

function mio()
{
  $("#useful_links").toggle();
  $("#useful_links_editor").toggle();
}

// --- END - BLOCK USEFUL LINKS ----------------------------------------------------------------


// --- INIT - BLOCK ACTIVITY OF USER -----------------------------------------------------------

GM.xmlHttpRequest({
  method: "GET",
  url: "http://10.0.20.10/redmine/activity?user_id=" + id_user,
  onload: function(response) {
    $page = $(response.responseText);
    $content = $("#content", $page);
    $("#sidebarHandler", $content).empty();
    
    $activity = $block.clone();
    
    $("h3", $activity).html("Actividad");
		$("div", $activity).append(
      $content
		);

		$("#list-right").append($content);
  }
});

// --- END - BLOCK ACTIVITY OF USER -----------------------------------------------------------
