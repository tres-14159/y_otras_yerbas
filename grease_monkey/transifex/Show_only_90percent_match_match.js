/*
Show_only_90percent_match

Copyright (C) 2018 Miguel de Dios Matias

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

// ==UserScript==
// @name     Show_only_90percent_match_match
// @version  1
// @grant GM.xmlHttpRequest
// @grant GM.getValue
// @grant GM.setValue
// @grant GM.listValues
// @grant unsafeWindow
// @include https://www.transifex.com/*/*/translate/*/*
// @require https://code.jquery.com/jquery-3.5.1.js
// ==/UserScript==


function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

(async _ => {
  console.log(33333);
  
  $("div[region='qa-checks']").before(
  `
  <div region="show_only_percent" class="u-display-flex">
    <div>
      <a id="show_only_percent" class="o-button o-button--toggle o-button--autowidth u-marginLeft-1x tipsy-enable-nu-display-flex u-textAlign-center  js-qa-checks ">
        Show only 90% &gt;= match
      </a>
    </div>
  </div>
  `
  );
  
  $("#show_only_percent").on('click', function() {
    let total = $("#string-list > div > div").length;
    console.log(total);
    
    let test = 10;
    $("#string-list > div > div").each(function( index ) {
      $('div', this).trigger( "click" );
      return false;
    });
  });
  
/*
  $("#show_only_percent").on('click', function() {
    let total = $("#string-list > div > div").length;
    console.log(total);

    let test = 10;
    $("#string-list > div > div").each(function( index ) {
      $('div', this).trigger( "click" );
      console.log(test);
      await sleep(3000);
      console.log(111);
      return false;
      test--;
      if (test == 0) {
        return false;
      }
    });
  });
*/
})();


