#!/usr/bin/env python3
# 
# 20questions.py
# Copyright (C) 2020  Miguel de Dios Matias (as developer)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from sqlalchemy import create_engine, and_, func, desc, asc
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

# --- INIT - TABLES ------------------------------------------------------

Base = declarative_base()

class Thing(Base):
    __tablename__ = 'things'
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable = False)
    
    thingquestions = relationship("ThingQuestion", cascade="all, delete", back_populates="thing")

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return f'Thing({self.id}, {self.name})'
    
    def __str__(self):
        return self.name

    @classmethod
    def save(cls, name):
        global session
        
        temp = Thing(name)
        session.add(temp)
        session.commit()
        return temp

class Question(Base):
    __tablename__ = 'questions'
    id = Column(Integer, primary_key=True)
    phrase = Column(String, nullable = False)
    
    thingquestions = relationship("ThingQuestion", cascade="all, delete", back_populates="question")
    nextquestions_questions = relationship("NextQuestion", cascade="all, delete",
        foreign_keys="NextQuestion.question_id", back_populates="question")
    nextquestions_nextquestions = relationship("NextQuestion", cascade="all, delete",
        foreign_keys="NextQuestion.nextquestion_id", back_populates="nextquestion")
    
    def __init__(self, phrase):
        self.phrase = phrase

    def __repr__(self):
        return f'Question({self.id}, {self.phrase})'
    
    def __str__(self):
        return self.phrase

    @classmethod
    def save(cls, phrase):
        global session
        
        temp = Question(phrase)
        session.add(temp)
        session.commit()
        return temp

class ThingQuestion(Base):
    __tablename__ = 'things_questions'
    id = Column(Integer, primary_key=True)
    thing_id = Column(Integer, ForeignKey('things.id'))
    question_id = Column(Integer, ForeignKey('questions.id'))
    res_yes = Column(Integer, nullable = False, default = 0)
    res_not = Column(Integer, nullable = False, default = 0)
    
    thing= relationship("Thing", back_populates="thingquestions")
    question= relationship("Question", back_populates="thingquestions")
    
    def __init__(self, thing_id, question_id, res_yes, res_not):
        self.thing_id = thing_id
        self.question_id = question_id

    def __repr__(self):
        return f'ThingQuestion({self.id}, {self.thing_id}, {self.question_id})'
    
    def __str__(self):
        return self.__repr__()
    
    @classmethod
    def save(cls, thing, question, res_yes = 0, res_not = 0):
        global session
        
        thing_obj = session.query(Thing).filter(Thing.name == thing).first()
        question_obj = session.query(Question).filter(Question.phrase == question).first()
        if not thing_obj:
            thing_obj = Thing.save(thing)
        if not question_obj:
            question_obj = Question.save(question)
        temp = ThingQuestion(thing_obj.id, question_obj.id, res_yes, res_not)
        session.add(temp)
        session.commit()
        return temp

class NextQuestion(Base):
    __tablename__ = 'next_questions'
    id = Column(Integer, primary_key=True)
    question_id = Column(Integer, ForeignKey('questions.id'))
    nextquestion_id = Column(Integer, ForeignKey('questions.id'))
    res_yes = Column(Integer, nullable = False, default = 0)
    res_not = Column(Integer, nullable = False, default = 0)
    
    question = relationship("Question", back_populates="nextquestions_questions", foreign_keys=[question_id])
    nextquestion = relationship("Question", back_populates="nextquestions_nextquestions", foreign_keys=[nextquestion_id])

    def __init__(self, question_id, nextquestion_id, res_yes, res_not):
        self.question_id = question_id
        self.nextquestion_id = nextquestion_id
        self.res_yes = res_yes
        self.res_not = res_not

    def __repr__(self):
        return f'NextQuestion({self.id}, {self.question_id}, {self.next_question_id})'
    
    def __str__(self):
        return self.__repr__()
    
    @classmethod
    def save(cls, question, nextquestion, res_yes = 0, res_not = 0):
        global session
        
        if type(question) is str:
            question_obj = session.query(Question).filter(Question.phrase == question).first()
        elif type(question) is int:
            question_obj = session.query(Question).filter(Question.id == question).first()
        else:
            question_obj = question
        
        if type(nextquestion) is str:
            nextquestion_obj = session.query(Question).filter(Question.phrase == nextquestion).first()
        elif type(nextquestion) is int:
            nextquestion_obj = session.query(Question).filter(Question.id == nextquestion).first()
        else:
            nextquestion_obj = nextquestion
        
        temp = NextQuestion(question_obj.id, nextquestion_obj.id, res_yes, res_not)
        session.add(temp)
        session.commit()
        return temp

# --- END  - TABLES ---------------------------------------------

def get_best_question(count, game):
    global session
    
    if count == 1:
        count_questions = session.query(ThingQuestion).count()
        if count_questions > 0:
            percent_80 = count_questions * 80 / 100
            percent_20 = count_questions * 20 / 100
            percent_60 = count_questions * 60 / 100
            percent_40 = count_questions * 40 / 100
        else:
            return None
        for value_up, value_down in [[80, 20], [60, 40]]:
            percent_up = count_questions * value_up/ 100
            percent_down = count_questions * value_down / 100
            
            thing_question = session.query(ThingQuestion).filter(
                and_(ThingQuestion.res_yes > percent_up,
                    ThingQuestion.res_not < percent_down)).order_by(
                    desc(ThingQuestion.res_yes)).first()
            if thing_question is None:
                thing_question = session.query(ThingQuestion).filter(
                    and_(ThingQuestion.res_not > percent_up,
                        ThingQuestion.res_yes < percent_down)).order_by(
                        desc(ThingQuestion.res_not)).first()
        if thing_question:
            return thing_question.question
        else:
            return session.query(Question).first()
    else:
        used_questions = [step['question'] for step in game if step['question']]
        
        # TODO: AT THE MOMENT THIS SHIT
        return session.query(Question).filter(Question.id.notin_(used_questions)).first()

def get_best_thing(count, game):
    global session
    
    if count == 1:
        return None
    else:
        previous_question = game[-1]['question']
        count_questions = session.query(ThingQuestion).filter(
            ThingQuestion.question_id == previous_question).count()
        if count_questions > 0:
            percent_80 = count_questions * 80 / 100
            percent_20 = count_questions * 20 / 100
        else:
            return None
        
        thing_question = None
        if game[-1]['response'] == 'S':
            thing_question = session.query(ThingQuestion).filter(
                and_(ThingQuestion.question_id == previous_question,
                    ThingQuestion.res_yes > percent_80,
                    ThingQuestion.res_not < percent_20)).order_by(
                    desc(ThingQuestion.res_yes)).first()
        elif game[-1]['response'] == 'N':
            thing_question = session.query(ThingQuestion).filter(
                and_(ThingQuestion.question_id == previous_question,
                    ThingQuestion.res_yes < percent_20,
                    ThingQuestion.res_not > percent_80)).order_by(
                    desc(ThingQuestion.res_not)).first()
        else:
            return None
        return (thing_question.thing if thing_question else None)

def ask_question(question, thing):
    clean_response = False
    while True:
        print("Por favor responde: con (s)i (n)o, (x) no lo se, (p)robablemente si, p(r)obablemente no")
        if question:
            response = input(f'{question.phrase} > ').upper()
            if response not in ['S', 'N', 'X', 'P', 'R']:
                print('Por favor responde S o N o X o P o R')
            else:
                return response
        else:
            response = input(f'¿Estas pensando en {thing.name}? > ').upper()
            if response not in ['S', 'N']:
                print('Por favor responde S o N')
            else:
                return response

def process_question(count, game):
    if game[-1]['thing'] != 0:
        if game[-2]['question'] != 0:
            thing_question = session.query(ThingQuestion).filter(
                and_(ThingQuestion.thing_id == game[-1]['thing'],
                    ThingQuestion.question_id == game[-2]['question'])).first()
            if game[-1]['response'] == 'S':
                thing_question.res_yes += 1
                win = True
            else:
                thing_question.res_not += 1
                win = False
            session.add(thing_question)
            session.commit()
        return win
    elif game[-1]['question'] != 0:
        if len(game) > 1:
            if game[-2]['question'] != 0:
                next_question = session.query(NextQuestion).filter(
                    and_(NextQuestion.question_id == game[-2]['question'],
                        NextQuestion.nextquestion_id == game[-1]['question'])).first()
                if next_question is None:
                    next_question = NextQuestion.save(game[-1]['question'], game[-2]['question'])
                if game[-1]['response'] == 'S':
                    next_question.res_yes += 1
                else:
                    next_question.res_not += 1
                session.add(next_question)
                session.commit()
    return False


def main():
    global session

    engine = create_engine('sqlite:///data.db')
    Base.metadata.create_all(engine) # Create or update sqlitefile
    Session = sessionmaker(bind=engine)
    session = Session()

    playing = True
    while playing:
        print("Vamos a jugar a adivinar en que piensas.")
        
        game = []
        count = 1
        win = False
        while count <= 20:
            print(f'Vamos con el turno {count}')
            question = None
            thing = get_best_thing(count, game)
            if thing is None:
                question = get_best_question(count, game)
                if question is None:
                    break
            
            response = ask_question(question, thing)
            game.append({
                'question': question.id if question else 0,
                'thing': thing.id if thing else 0,
                'response': response})
            win = process_question(count, game)
            if win:
                print("He ganado")
                break
            count += 1
        
        if not win:
            print('Has ganado.')
            new_thing = input('No se que es, me lo puedes decir > ')
            new_question = input(f'Y me puedes decir una pregunta acierte con "{new_thing}" > ')
            
            thing_question = session.query(ThingQuestion).join(Thing).join(Question).filter(
                and_(Thing.name == new_thing, Question.phrase == new_question)).first()
            
            if thing_question is None:
                thing_question = ThingQuestion.save(new_thing, new_question)
            thing_question.res_yes += 1
            session.add(thing_question)
            session.commit()
        response = input('¿Jugar otra vez? (S/N) > ')
        playing = response.upper() == 'S'

if __name__ == '__main__':
     main()
