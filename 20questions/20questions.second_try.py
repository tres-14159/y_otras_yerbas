#!/usr/bin/env python3
# 
# 20questions.py
# Copyright (C) 2020  Miguel de Dios Matias (as developer)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from sqlalchemy import create_engine, and_, func, desc, asc
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship

# --- INIT - TABLES ------------------------------------------------------

Base = declarative_base()

class Thing(Base):
    __tablename__ = 'things'
    id = Column(Integer, primary_key=True)
    name = Column(String, nullable = False)
    
    turns = relationship("Turn", cascade="all, delete", back_populates="thing")

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return f'Thing({self.id}, {self.name})'
    
    def __str__(self):
        return self.name

    @classmethod
    def save(cls, name):
        global session
        
        temp = Thing(name)
        session.add(temp)
        session.commit()
        return temp
    
    @classmethod
    def get_or_save(cls, name):
        global session
        
        temp = session.query(Thing).filter(Thing.name == name).first()
        if not temp:
            temp = Thing.save(name)
        return temp

class Question(Base):
    __tablename__ = 'questions'
    id = Column(Integer, primary_key=True)
    phrase = Column(String, nullable = False)
    
    turns = relationship("Turn", cascade="all, delete", back_populates="question")
    
    def __init__(self, phrase):
        self.phrase = phrase

    def __repr__(self):
        return f'Question({self.id}, {self.phrase})'
    
    def __str__(self):
        return self.phrase

    @classmethod
    def save(cls, phrase):
        global session
        
        temp = Question(phrase)
        session.add(temp)
        session.commit()
        return temp

    @classmethod
    def get_or_save(cls, phrase):
        global session
        
        temp = session.query(Question).filter(Question.phrase == phrase).first()
        if not temp:
            temp = Question.save(phrase)
        return temp

class Game(Base):
    __tablename__ = 'games'
    id = Column(Integer, primary_key=True)
    turns = relationship("Turn", cascade="all, delete", back_populates="game")
    
    def __init__(self):
        pass
    
    def __repr__(self):
        return f'Game({self.id})'
    
    def __str__(self):
        return self.__repr__()

    @classmethod
    def save(cls, game):
        global session
        
        new_game = Game()
        session.add(new_game)
        session.commit()
        
        # TODO: PROCESS "NO SE" AND OTHERS ANSWERS
        turn_number = 1
        for turn in game:
            turn_obj = Turn.save(turn['thing'], turn['question'], turn['response'], turn_number, new_game.id)
            session.add(turn_obj)
            session.commit()
            turn_number += 1
            

class Turn(Base):
    __tablename__ = 'turns'
    id = Column(Integer, primary_key=True)
    thing_id = Column(Integer, ForeignKey('things.id'))
    question_id = Column(Integer, ForeignKey('questions.id'))
    response = Column(String, nullable = False)
    turn = Column(Integer, nullable = False)
    game_id = Column(Integer, ForeignKey('games.id'))
    
    thing = relationship("Thing", back_populates="turns")
    question = relationship("Question", back_populates="turns")
    game = relationship("Game", back_populates="turns")
    
    def __init__(self, thing_id, question_id, response, turn, game_id):
        self.thing_id = thing_id
        self.question_id = question_id
        self.response = response
        self.turn = turn
        self.game_id = game_id

    def __repr__(self):
        return f'Turn({self.id}, {self.thing_id}, {self.question_id}, {self.response})'
    
    def __str__(self):
        return self.__repr__()
    
    @classmethod
    def save(cls, thing_id, question_id, response, turn, game_id):
        global session
        
        temp = Turn(thing_id, question_id, response, turn, game_id)
        session.add(temp)
        session.commit()
        return temp

# --- END  - TABLES ---------------------------------------------

def get_best_other_games(turn, game):
    # TODO: Get the most similar turn of old games
    
    global session
    
    game = session.query(Game).first()
    if game:
        turn = session.query(Turn).filter(
            and_(Turn.turn == turn, Turn.game_id == game.id)).first()
        if turn:
            return turn.question, turn.thing
    return None, None

def ask_question(question, thing):
    clean_response = False
    while True:
        print("Por favor responde: con (s)i (n)o, (x) no lo se, (p)robablemente si, p(r)obablemente no")
        if question:
            response = input(f'{question.phrase} > ').upper()
            if response not in ['S', 'N', 'X', 'P', 'R']:
                print('Por favor responde S o N o X o P o R')
            else:
                return response
        else:
            response = input(f'¿Estas pensando en {thing.name}? > ').upper()
            if response not in ['S', 'N']:
                print('Por favor responde S o N')
            else:
                return response

def main():
    global session

    engine = create_engine('sqlite:///data.db')
    Base.metadata.create_all(engine) # Create or update sqlitefile
    Session = sessionmaker(bind=engine)
    session = Session()

    playing = True
    while playing:
        print("Vamos a jugar a adivinar en que piensas.")
        
        game = []
        turn = 1
        win = False
        while turn <= 20:
            print(f'Vamos con el turno {turn}')
            question, thing = get_best_other_games(turn, game)
            if not question and not thing:
                break
            response = ask_question(question, thing)
            game.append({
                'question': question.id if question else 0,
                'thing': thing.id if thing else 0,
                'response': response})
            if thing and response == 'S':
                win = True
                print("He ganado")
                break
            turn += 1
        
        if not win:
            print('Has ganado.')
            new_thing = input('No se que es, me lo puedes decir > ')
            new_question = input(f'Y me puedes decir una pregunta acierte con "{new_thing}" > ')
            
            fake_game = []
            question = Question.get_or_save(new_question)
            thing = Thing.get_or_save(new_thing)
            
            fake_game.append({
                'question': question.id,
                'thing': None,
                'response': 'S'})
            fake_game.append({
                'question': None,
                'thing': thing.id,
                'response': 'S'})
            game = fake_game
        Game.save(game)
        
        response = input('¿Jugar otra vez? (S/N) > ')
        playing = response.upper() == 'S'

if __name__ == '__main__':
     main()
