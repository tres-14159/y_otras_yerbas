* Akinator, me rallas.
  * La lucha por entender el akinator y hacerte uno con casinos
  * Por: Miguel de Dios Matias

* Esta conferencia
  * Es para divertirnos y curiosidad.
  * Miguel no es un experto en temas de "machine learning" e IA.
  * Tu eres un experto, te puedes aburrir o no.
    * Paz, Amor y Linux.
    * Colaboradores SI , Pedantes NO, Cuñados NO (ya esta Miguel)

* Akinator (un no-juego)
  * Adivina lo que estas pensando.
  * Tiene mas años que la tana.
  * Te hace preguntas.
    * Respuestas Si, No, Nose, Probable Si, Probable No.
  * Resiste troleos.
  * Tiene una base de datos, aprende...¿Cómo?
  * Reformado hace poco (a peor porque antes era front html cutre pero rápido).
  
* Espiritu hacker
  * persona X mas hacker que tu, te dice que eres hacker
  * curiosidad
  * jugar con las cosas para exprimirlas o que haga otra cosa
  
* ¿Qué me come del Akinator?
  * ¿Cómo trolearlo?
    * imposible, mucha gente llenando, no vas a intoxicar la BD. Ejm "¿Es una democracia? Si. Es USA."
      * Me apuesto a que tienen backups, chequeos y seguridad para ataques "DoS" para intoxicar.
  * ¿Cómo funciona?
    * Hacer un clon libre del akinator

* Investigación
  * El problema de 20 preguntas.
  * https://en.wikipedia.org/wiki/Twenty_Questions
  * Muchos links que los hipsters de la programación se excitan y se tocan, stackoverflow, medium...
    * Hipster programación == Programador con portátil blanquito de manzana con pegatinas mansas y nada críticas

* ¿Quiero programarlo?
  * Python: porque para mi es rápido de programar y releer...no es lo mas óptimo pero.
  * SQLAlchemy: para guardar datos
    * Necesito gritarlo
      * la locura del ORM de SQLAlchemy necesitas guarrear las clases padre con cosas del hijo en vez que cada una sea independiente. Hablo del `relationship` para tener el `delete on cascade`.
      * la mierda de que el ORM necesite un objeto session, los objetos no estan engachados a la BD.
        * https://stackoverflow.com/questions/31584974/sqlalchemy-model-django-like-save-method
  * SQLite
    * Recuerda joven guerrero de las tierras de BD
      * No estas en terrenos malditos del noSQL, pero si en terrenos pantanosos de las rayadas de sqlite.
        * No hay constraint para las columnas, puedes meter todo en todo.
          * Se puede habilitar, pero desde SQLAlchemy "creo que no".
          * No hay constraint para los foreignkey, puedes meter 
  * ¿pandas.DataFrame?
  * Python
    * La magia de ipython3 -i <fichero.py> para jugar con el código.
      * Ojo con el `def main()` que después no llegas a las cosas.

* Mi primer intento
  * Empece con una lista de dict.
  * Vi que me quedaba grande para hacer busquedas.
  * Pense en pandas.DataFrame .
  * Me hizo ojitos sqlite.
  * Empece con sqlite3 a pelo en python.
  * Vi que era tirar mucho código.
  * Empece con SQLAlchemy y su "ORM".
    * Era menos código pero tampoco una fiesta.
  * Guarda
    * Cosas
    * Preguntas
    * Cosa - Pregunta
    * Pregunta - Siguiente Pregunta
  * Guarda las veces que si y que no a "Cosa - Pregunta"
  * Medio funciona
    * Pero solo usa Cosa - Pregunta para sacar la lista de preguntas.
  * Lo abandone porque:
    * creo que es jaleo sacar la mejor pregunta de "Pregunta - Siguiente Pregunta".
      * siempre recuerdo el contraejemplo de un libro de crear aventuras textuales en basic de hacer un monton if-else o un parser. 
    * creo que se necesita una estructura superior para agrupar caminos mas largos de pregunta-pregunta-intento cosa-pregunta-pregunta-cosa o así.

* Mi segundo intento
  * Cogí lo bueno del código anterior.
  * Pense en guardar las partidas ganadoras.
  * Y después "buscar" la partida ganadora mas parecida a la que esta en curso para sacar la siguiente pregunta.
  * Si perdia no guardar, si ganaba guardar la partida.
  * Este esta mas a medias.
  * Lo abandone porque:
    * Vi que se necesitaba "una base de datos bestial de partidas" para tener un algoritmo que pareciera inteligente.
    * Que el orden de las preguntas de lo mismo (entrecomillas) y yo estaba dandole importancia al turno de cada pregunta.
    * Que no sabía como buscar "partida parecida", la igual si, pero la parecida.

* He fracasado
  * El árticulo de bayes y tal en Medium
  * WIP

* Mi tercer intento

* ¿Cómo evitar el troleo?

* ¿Y ahora qué?