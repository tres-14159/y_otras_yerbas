#! /bin/bash

echo "" > list_package_bugs.txt

for package in `dpkg --get-selections | sed 's:install$::'`
do
	echo "---------"$package"-----------" >> list_package_bugs.txt
	querybts -B debian -b $package >> list_package_bugs.txt
done

#~ dpkg --get-selections | sed 's:install$::' | xargs -t -n1 querybts -B debian -b