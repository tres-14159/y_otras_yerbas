#!/usr/bin/env python3
# 
# get_list_bugs_packages_installed_per_severity.py
# Copyright (C) 2020  Miguel de Dios Matias (as developer)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import logging
import sqlite3
import os
import debianbts
import pickle
import base64

sql_bugs_table = """
CREATE TABLE bugs (
    package TEXT NOT NULL CHECK (typeof(package) = 'text'),
    bug_num INTEGER NOT NULL CHECK (typeof(bug_num) = 'integer'),
    severity TEXT NOT NULL CHECK (typeof(severity) = 'text'),
    subject TEXT NOT NULL CHECK (typeof(subject) = 'text'),
    date TEXT NOT NULL CHECK (typeof(date) = 'text'),
    tags TEXT NOT NULL CHECK (typeof(tags) = 'text'),
    done INTEGER NOT NULL CHECK (typeof(done) = 'integer')
);
"""

def init_db(db):
    sql_empty_db = 'SELECT * FROM sqlite_master WHERE type = "table"'
    if len([_ for _ in db.cursor().execute(sql_empty_db)]) == 0:
       db.cursor().execute(sql_bugs_table)

def main():
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    for handler in logging.root.handlers:
        handler.addFilter(logging.Filter('get_bugs_debian'))
    
    logger = logging.getLogger('get_bugs_debian')
    
    db = sqlite3.connect('bugs.db')
    init_db(db)
    
    
    cmd_list_packages = 'dpkg --get-selections | grep -e "install$" | sed "s:install$::"'
    with os.popen(cmd_list_packages, 'r') as l:
        packages = l.read()
    
    num_packages = len(packages.splitlines())
    procesed_packages = 0
    logger.info(f"packages {num_packages}")
    for package in packages.splitlines():
        package_clean = package.strip()
        # ~ bug_nums = debianbts.get_bugs(package=package_clean, severity='minor', tag='newcomer')
        bug_nums = debianbts.get_bugs(package=package_clean, tag='newcomer')
        procesed_packages += 1
        logger.info(f"procesed {((procesed_packages / num_packages) * 100):.2f} %")
        if len(bug_nums) == 0:
            continue
        logger.info(f"{package_clean} {len(bug_nums)}")
        for bug_num in bug_nums:
            bug = debianbts.get_status(bug_num)[0]
            db.cursor().execute("""INSERT INTO bugs (package, bug_num, severity, subject, date, tags, done)
                VALUES (?, ?, ?, ?, ?, ?, ?);""",
                (package_clean, bug.bug_num, bug.severity, bug.subject, f'{bug.date}', f'{bug.tags}', int(bug.done)))
            db.commit()

if __name__ == '__main__':
    main()