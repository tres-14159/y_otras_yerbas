#! /bin/bash

#~ get_opt.example.sh
#~ Copyright (C) 2022 Miguel de Dios Matias

#~ This program is free software: you can redistribute it and/or modify
#~ it under the terms of the GNU General Public License as published by
#~ the Free Software Foundation, either version 3 of the License, or
#~ (at your option) any later version.

#~ This program is distributed in the hope that it will be useful,
#~ but WITHOUT ANY WARRANTY; without even the implied warranty of
#~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#~ GNU General Public License for more details.

#~ You should have received a copy of the GNU General Public License
#~ along with this program. If not, see <http://www.gnu.org/licenses/>.

: '
Examples the calls:

$ ./getopt.example.bash --arg-b 111 -a 2222 3333
argA 1
argB 111
argC 
argD 0
unamedOptions 2222 3333

$ ./getopt.example.bash -a --arg-b=111
argA 1
argB 111
argC 
argD 0
unamedOptions 

$ ./getopt.example.bash -c abc def
argA 0
argB 
argC abc
argD 0
unamedOptions def
'

function help() {
    echo "$0 [(--arg-a | -a)] [(--arg-b | -b) <data_b>] [(--arg-c | -c <data_c>)] [-d] [(--help | -h)]"
}

LONG_OPTION_LIST=(
    "arg-a"
    "arg-b:"
    "arg-c:"
    "help"
)
SORT_OPTION_LIST=(
    "a"
    "b:"
    "c:"
    "d"
    "h"
)
# Read the parameters
opts=$(getopt -q \
  --longoptions "$(printf "%s," "${LONG_OPTION_LIST[@]}")" \
  --name "$(basename "$0")" \
  --options "$(printf "%s" "${SORT_OPTION_LIST[@]}")" \
  -- "$@"
)
eval set -- "$opts"

argA=0
argD=0
unamedOptions=()
# It it is same a queue (process the head) because $1 and $2
while true
do
    case "$1" in
        --arg-a | -a)
            argA=1
            ;;
        --arg-b | -b)
            argB=$2
            shift 1
            ;;
        --arg-c | -c)
            argC=$2
            shift 1
            ;;
        -d)
            argD=1
            ;;
        --help | -h)
            help
            exit 0
            ;;
        --)
            # End options now the unamed options
            ;;
        *)
            unamedOptions+=("$1")
            ;;
    esac
    shift 1
    if [ $# -eq 0 ]
    then
        break
    fi
done

echo "argA $argA"
echo "argB $argB"
echo "argC $argC"
echo "argD $argD"
echo "unamedOptions ${unamedOptions[@]}"
