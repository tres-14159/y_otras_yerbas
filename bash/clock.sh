#!/bin/bash

#~ clock.sh
#~ Copyright (C) 2018 Miguel de Dios Matias

#~ This program is free software: you can redistribute it and/or modify
#~ it under the terms of the GNU General Public License as published by
#~ the Free Software Foundation, either version 3 of the License, or
#~ (at your option) any later version.

#~ This program is distributed in the hope that it will be useful,
#~ but WITHOUT ANY WARRANTY; without even the implied warranty of
#~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#~ GNU General Public License for more details.

#~ You should have received a copy of the GNU General Public License
#~ along with this program. If not, see <http://www.gnu.org/licenses/>.

if ! command -v banner &> /dev/null; then
	(>&2 echo 'ERROR: "banner" is not installed in your system. Maybe you can install with "apt install sysvbanner"')
    exit 1
fi

command="
str_time=\$( date +%H:%M:%S );

foreground=\$( od -A n -t d -N 1 /dev/urandom);
foreground=\$(( \$foreground % 8 ));
background=\$( od -A n -t d -N 1 /dev/urandom );
background=\$(( \$background % 8 ));

if [ \$foreground -eq \$background ] ;
then 
	foreground=\$(( ( \$foreground + 1 ) % 8 ));
fi ;

tput setaf \$foreground; tput setab \$background;

banner \$str_time;
";

watch -n 1 -t --color eval $command ;
