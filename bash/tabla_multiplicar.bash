#! /bin/bash

if [[ "$1" = "--help" || "$1" = "-h" ]]
then
	echo "Script educativo para usarlo:"
	echo "$0 juego"
	exit 0
fi

echo $1

num=$(( $RANDOM % 11 ))

echo "TABLA DE MULTIPLICAR DEL $num"
echo "------------------------------"

for i in $(seq 0 10)
do
	correcto=$(( $i * $num ))
	read -p "$num x $i = " resultado
	if [[ "$correcto" != "$resultado" ]]
	then
		echo "INCORRECTO PRUEBA OTRA VEZ"
		read -p "$num x $i = " resultado
		if [[ "$correcto" != "$resultado" ]]
		then
			echo "HAS FALLADO COMIENZA OTRA VEZ"
			sleep 5
			exit 1
		fi
	fi
done

$1
