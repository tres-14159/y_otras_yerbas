#!/bin/bash

#~ cortador_iguales.sh
#~ Copyright (C) 2017 Miguel de Dios Matias

#~ This program is free software: you can redistribute it and/or modify
#~ it under the terms of the GNU General Public License as published by
#~ the Free Software Foundation, either version 3 of the License, or
#~ (at your option) any later version.

#~ This program is distributed in the hope that it will be useful,
#~ but WITHOUT ANY WARRANTY; without even the implied warranty of
#~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#~ GNU General Public License for more details.

#~ You should have received a copy of the GNU General Public License
#~ along with this program. If not, see <http://www.gnu.org/licenses/>.


sonido=$1;
partes=$2;

rm -rf "partes";
mkdir "partes";



#--- DURACION DEL AUDIO EN MICROSEGUNDOS -------------------------------
linea_duracion=`ffmpeg -i $sonido 2>&1 | grep Duration`;
duracion=`echo $linea_duracion | cut -d',' -f1 | sed 's/Duration: //g'`

horas=`echo $duracion | cut -d':' -f1`
minutos=`echo $duracion | cut -d':' -f2`
segundos=`echo $duracion | cut -d':' -f3 | cut -d'.' -f1`
microsegundos=`echo $duracion | cut -d':' -f3 | cut -d'.' -f2`

total_microsegundos=$(( $microsegundos + (segundos * 100) + (minutos * 60 * 100) + (horas * 60  * 60 * 100) ));



# --- PARTES -----------------------------------------------------------
parte_microsegundos=$(( $total_microsegundos / $partes ));

echo "";
echo "";
echo "parte_microsegundos:$parte_microsegundos";
echo "";
echo "";

parte_segundos=0;
if [ $parte_microsegundos -ge 100 ];
	then
		parte_segundos=$(( $parte_microsegundos / 100 ));
		parte_microsegundos=$(( $parte_microsegundos - ($parte_segundos * 100) ));
fi;

parte_minutos=0;
if [ $parte_segundos -ge 60 ];
	then
		parte_minutos=$(( $parte_segundos / 60 ));
		parte_segundos=$(( $parte_segundos - ($parte_minutos * 60) ));
fi;


#--- TROCEADO DE LOS FICHEROS DE AUDIO ---------------------------------
comienzo_minutos=0;
comienzo_segundos=0;
comienzo_microsegundos=0;

for i in $(seq $partes)
do
	sonido_final=$(echo "partes/$i""___$sonido");
	
	echo "";
	echo "";
	echo "";
	echo "ffmpeg -i $sonido -acodec copy -t 00:$parte_minutos:$parte_segundos.$parte_microsegundos -ss 00:$comienzo_minutos:$comienzo_segundos.$comienzo_microsegundos $sonido_final";
	echo "";
	echo "";
	echo "";
	ffmpeg -i $sonido -acodec copy -t 00:$parte_minutos:$parte_segundos.$parte_microsegundos -ss 00:$comienzo_minutos:$comienzo_segundos.$comienzo_microsegundos $sonido_final;
	
	comienzo_microsegundos=$(( $comienzo_microsegundos + $parte_microsegundos ));
	sobrante=0;
	if [ $comienzo_microsegundos -ge 100 ];
	then
		sobrante=$(( $comienzo_microsegundos / 100 ));
		comienzo_microsegundos=$(( $comienzo_microsegundos - ($sobrante * 100) ));
	fi;
	
	comienzo_segundos=$(( $comienzo_segundos + $parte_segundos + $sobrante ));
	sobrante=0;
	if [ $comienzo_segundos -ge 60 ];
	then
		sobrante=$(( $comienzo_segundos / 60 ));
		comienzo_minutos=$(( $parte_minutos + $comienzo_segundos - ($sobrante * 100) ));
	fi;
done
