#!/usr/bin/env php
<?php
/*
 * indent_phtml.php
 * Copyright (C) 2019 Miguel de Dios Matias
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */

function debug($var, $file='', $oneline=false)
{
    $more_info = '';
    if (is_string($var)) {
        $more_info = 'size: '.strlen($var);
    } else if (is_bool($var)) {
        $more_info = 'val: '.($var ? 'true' : 'false');
    } else if (is_null($var)) {
        $more_info = 'is null';
    } else if (is_array($var)) {
        $more_info = count($var);
    }
    if ($oneline && is_string($var)) {
        $var = preg_replace("/[\t|\n| ]+/", ' ', $var);
    }
    
    $first_line = date('Y/m/d H:i:s') . ' ('.gettype($var).') ' . $more_info . "\n";
    $second_line = print_r($var, true);
    
    if ($file === true) {
        $file = '/tmp/logDebug';
    }
    if (strlen($file) > 0) {
        $f = fopen($file, 'a');
        ob_start();
        echo $first_line;
        echo $second_line;
        echo "\n\n";
        $output = ob_get_clean();
        fprintf($f, '%s', $output);
        fclose($f);
    } else {
        if (PHP_SAPI == 'cli') {
            echo $first_line . "\n";
            echo $second_line . "\n";
        }
        else {
            echo '<pre style="z-index: 10000; background: #fff; padding: 1em;">' . $first_line . "\n";
            echo $second_line;
            echo '</pre>';
        }
    }
}

function dependencies() {
    if (!function_exists('token_get_all')) {
        error_log('Not found tokeninzer in your php.');
        return false;
    }
    
    return true;
}

function show_help($script_name) {
    echo "Usage: $script_name [OPTION]... FILE_IN [FILE_OUT]\n";
    echo "Ident a php file (with html inline) and show in stdout or save to file.\n";
    echo "\n";
    echo "Arguments:\n";
    echo "\t -h, --help \t\t\t Show this help.\n";
    echo "\t -iFILE_IN, --input=FILE_IN \t PHP (with html inline) to indent.\n";
    echo "\t -oFILE_OUT, --output=FILE_OUT \t PHP file to store the indented input file.\n";
    echo "\n";
}

function indent($input_file, $output_file) {
    $source = file_get_contents($input_file);
    $tokens = token_get_all($source);
    foreach ($tokens as $token) {
        if (is_array($token)) {
            echo "Line {$token[2]}: ", token_name($token[0]), " ('{$token[1]}')", PHP_EOL;
        }
    }
    
    // CASES
    /**
     * 
     * PHP    | HTML   | PHP  | HTML
     *   HTML |   PHP  | HTML | PHP
     * PHP    | HTML   |      |
     *
     * Indent
     * 
     * HTML   | HTML   | HTML | HTML
     *        | HTML   |
     **/
     // PHP BLOCKS TYPES
     /**
      * Pure block PHP: <?php (all { code } close ?>
      * Open block PHP: example
      *      <?php
      *      foreach ($array as $item) {
      *      $value = $item + 100;
      *      ?>
      *          <b><?php echo $value; ?>
      * 
      * Close block PHP: example
      *          <b>something</b>
      *      <?php
      *      } // end if
      *      ?>
      **/
}

// __main__ or int main(int argc, char** argv)
$script_name = $argv[0];

// Check dependencies
if (!dependencies()) {
    exit(1);
}

$options = getopt('hi::o::', ['help', 'input::', 'output::'], $stop_index);
$arguments = array_keys($options);

$input_file = null;
$output_file = null;

if (count(array_intersect(['h', 'help'], $arguments)) > 0) {
    show_help($script_name);
}
else {
    if (in_array('i', $arguments)) {
        $input_file = $options['i'];
    }
    else if (in_array('input', $arguments)) {
        $input_file = $options['input'];
    }
    
    if (in_array('o', $arguments)) {
        $output_file = $options['o'];
    }
    else if (in_array('output', $arguments)) {
        $output_file = $options['output'];
    }
}

if (count($argv) >= count($options)) {
    for ($i = $stop_index; $i < count($argv); $i++) {
        if ($i - $stop_index == 0) {
            $input_file = $argv[$i];
        }
        if ($i - $stop_index == 1) {
            $output_file = $argv[$i];
        }
    }
}

if (is_null($input_file)) {
    error_log('Not found input file.');
    exit(1);
}

// Check correct input file.
exec('php -l ' . $input_file, $output, $status);
if ($status != 0) {
    error_log($output);
    exit(1);
}

$status = indent($input_file, $output_file);
exit($status);
?>
