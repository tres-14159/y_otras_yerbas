            <?php
// A example with a horrific indentation

$title = 'This is the title';
$rows = array(array('aaa', 'bbb', 'ccc'),
array('ddd', 'eee', 'fff'),
    array('<b>
aaaa</b>', 'hhh', 'iii'));

function print_rows($rows) {
    foreach ($rows as $row) {
?>
    <tr>
<?php
       foreach ($row as $cell) {
?>
<td><?=$cell?></td>
<?php
    }
?>
</tr>
<?php
}
}
?>
<html>
    <head>
<title>A example with a horrific indentation
</title>
</head>
<body>
    <h1>
<?php
echo $title;?></h1>


<table border="1">
<?php print_rows($rows);?>
</table>

    </body>
</html>
