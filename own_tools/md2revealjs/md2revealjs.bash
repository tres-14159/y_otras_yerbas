#! /bin/bash

current_dir=$(pwd)
markdown_file=$1

if [ "$1" == "" ]
then
    echo "[ERROR] No markdown file to generate pdf."
    exit 1
fi

temp_dir=$(mktemp -d)
echo "[INFO] Create temp dir '$temp_dir'"

cd /tmp
git clone https://github.com/hakimel/reveal.js.git --depth=1
if [ $? -ne 0 ]
then
    echo "[INFO] Revealjs git was cloned."
else
    echo "[INFO] Revealjs git is cloned."
fi

cd /tmp/reveal.js

count_packages=$(( $(npm ls --parseable | wc -l) - 1 ))

if [ $count_packages -eq 0 ]
then
    echo "[INFO] Revealjs has not installed npm packages."
    npm install
else
    echo "[INFO] Revealjs has installed npm packages."
fi

cp -r /tmp/reveal.js/* $temp_dir

cp "$current_dir/$markdown_file" $temp_dir
cd $temp_dir

replace="<div class='reveal'><section data-markdown='$markdown_file' data-separator='^\\\n\\\n\\\n' data-separator-vertical='^\\\n\\\n' data-separator-notes='^Note:' data-charset='utf-8'></section></div>"

html_markdown=$(cat "$temp_dir/index.html" | tr '\n' '\0' | sed -E "s|<div class=\"reveal\">.*</div>|$replace|g; t; q42" | tr '\0' '\n');

echo $?

echo "$html_markdown" > "$temp_dir/index.html"
npm start -- --port=8001
