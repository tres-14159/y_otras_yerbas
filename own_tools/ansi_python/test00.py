#!/usr/bin/env python3
#
# test00.py
# Copyright (C) 2019  Miguel de Dios Matias (as developer)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import enum
import re

class Token(enum.Enum):
    string = enum.auto()
    font = enum.auto()
    saveCursor = enum.auto()
    clearLineFromCursorToRight = enum.auto()
    saveScreen = enum.auto()
    restoreScreen = enum.auto()
    invisibleCursor = enum.auto()
    visibleCursor = enum.auto()
    moveCursor = enum.auto()

ESC = '\x1b'

ansi = {
    Token.saveCursor.name: ESC + '7',
    Token.clearLineFromCursorToRight.name: ESC + '[K',
    Token.saveScreen.name: ESC + '[?47h',
    Token.restoreScreen.name: ESC + '[?47l',
    Token.invisibleCursor.name: ESC + '[?25h',
    Token.visibleCursor.name: ESC + '[?25l',
    Token.font.name: ESC + '[{}m',
    Token.moveCursor.name: ESC + '[{};{}H'
}

fontValues = {
    'reset': 0
}

color4bits = {
    'black': {'fg': 30, 'bg': 40},
    'red': {'fg': 31, 'bg': 41},
    'green': {'fg': 32, 'bg': 42}
}

test = ansi['font'].format(color4bits['red']['fg']) + "HELLO" + ansi['font'].format(fontValues['reset']) + ' WORLD'
test2 = '\x1b7\x1b[?47h\x1b[?1002l\x1b[?1000l\x1b[?1000h\x1b[?1002h\x1b)0\x1b[?25l\x1b[0;39;49m\x1b[H\x1b[1;1H\x1b[0;39;49m\x0f\x1b[K\x1b[2;1H\x1b[0;39;49m\x0f\x1b[K\x1b[3;1H\x1b[0;39;49m\x0f\x1b[K\x1b[4;1H\x1b[0;39;49m\x0f\x1b[K\x1b[5;1H\x1b[0;39;49m\x0f\x1b[K\x1b[6;1H\x1b[0;39;49m\x0f\x1b[K\x1b[7;1H\x1b[0;39;49m\x0f\x1b[K\x1b[8;1H\x1b[0;39;49m\x0f\x1b[K\x1b[9;1H\x1b[0;39;49m\x0f\x1b[K\x1b[10;1H\x1b[0;39;49m\x0f\x1b[K\x1b[11;1H\x1b[0;39;49m\x0f\x1b[K\x1b[12;1H\x1b[0;39;49m\x0f\x1b[K\x1b[13;1H\x1b[0;39;49m\x0f\x1b[K\x1b[14;1H\x1b[0;39;49m\x0f\x1b[K\x1b[15;1H\x1b[0;39;49m\x0f\x1b[K\x1b[16;1H\x1b[0;39;49m\x0f\x1b[K\x1b[17;1H\x1b[0;39;49m\x0f\x1b[K\x1b[18;1H\x1b[0;39;49m\x0f\x1b[K\x1b[19;1H\x1b[0;39;49m\x0f\x1b[K\x1b[20;1H\x1b[0;39;49m\x0f\x1b[K\x1b[21;1H\x1b[0;39;49m\x0f\x1b[K\x1b[22;1H\x1b[0;39;49m\x0f\x1b[K\x1b[23;1H\x1b[0;39;49m\x0f\x1b[K\x1b[24;1H\x1b[0;39;49m\x0fHello World\x1b[K'

def regexAnsi(code):
    return '.*?({})'.format(
        re.escape(ansi[code].replace('{}', '__')).replace('__', '(.*?)'))

def subAnalize(token, string):
    subToken = string
    value = string
    if re.match('^[0-9]+$', string):
        if string == str(0):
            subToken = 'reset'
        else:
            for color, types in color4bits.items():
                for typeColor, valueColor in types.items():
                    if string == str(valueColor):
                        subToken = 'color4bits'
                        value = [typeColor, color]
    return subToken, value

def analize(string, typeToken=Token.string):
    stack = []
    # ~ print(repr(string))
    first = None
    for token in ansi.keys():
        regexCode = regexAnsi(token)
        r = re.match(regexCode, string)
        if r:
            if first is None:
                first = [token, r]
            else:
                if r.span(1)[0] < first[1].span(1)[0]:
                    first = [token, r]
    # ~ print(first)
    if first is not None:
        token, r = first
        preString = string[:r.regs[1][0]]
        posString = string[r.regs[1][1]:]
        # ~ print(len(r.regs))
        subTokenString = None
        if len(r.regs) == 3:
            subTokenString = string[r.regs[2][0]:r.regs[2][1]]
        if len(preString):
            stack.append([Token.string, None, preString])
        subToken, value = [None, None]
        if subTokenString is not None:
            subToken, value = subAnalize(token, subTokenString)
        stack.append([token, subToken, value])
        stack += analize(posString, Token.string)
    else:
        if len(string):
            stack.append([Token.string, None, string])
    return stack

def main():
    print(test)
    print(analize(test))

if __name__ == '__main__':
    main()
