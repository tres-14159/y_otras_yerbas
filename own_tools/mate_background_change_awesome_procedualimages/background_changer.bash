#!/bin/bash

#~ meld_diff.sh
#~ Copyright (C) 2018 Miguel de Dios Matias

#~ This program is free software: you can redistribute it and/or modify
#~ it under the terms of the GNU General Public License as published by
#~ the Free Software Foundation, either version 3 of the License, or
#~ (at your option) any later version.

#~ This program is distributed in the hope that it will be useful,
#~ but WITHOUT ANY WARRANTY; without even the implied warranty of
#~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#~ GNU General Public License for more details.

#~ You should have received a copy of the GNU General Public License
#~ along with this program. If not, see <http://www.gnu.org/licenses/>.

function generate_random()
{
    init_param=$1;
    end_param=$2;
    
    rand=$( od -A n -t d -N 3 /dev/urandom );
    
    return_number=$(( ($rand % ($end_param + 1 - $init_param)) + $init_param ));
    
    echo $return_number;
}

function generate_float()
{
    number=$1
    
    echo "scale=3; $number/1000" | bc
}

command_dir=$(pwd);
working_dir="/tmp/awesome_backgrounds/";
mkdir -p $working_dir;

cd $working_dir;

source bin/activate 2>/dev/null

if [ $? -eq 1 ]
then
    virtualenv -p python3 .
fi

source bin/activate 2>/dev/null

python -m fractal --help 2>/dev/null 1>/dev/null

if [ $? -eq 1 ]
then
    pip3 install git+https://github.com/danilobellini/fractal.git
fi


real=$(generate_float $( generate_random 0 1600 ) );
signal=$( generate_random 0 1);
if [ $signal -eq 1 ]
then
    real=$( echo "0 - $real" | bc);
fi

float=$(generate_float $( generate_random 0 1600 ) );
signal=$( generate_random 0 1);
if [ $signal -eq 1 ]
then
    float=$( echo "0 - $float" | bc);
else
    float=" + $float";
fi

zoom=$(generate_float $( generate_random 0 1000 ) );
signal=$( generate_random 0 1);
if [ $signal -eq 1 ]
then
    zoom=$( echo "0 - $zoom" | bc);
fi

python -m fractal julia $real $float j --size=1000x600 --depth=100 --zoom=$zoom -o $working_dir"background.png"

gsettings set org.mate.background picture-filename $working_dir"background.png"

sleep 1m
$0 # This is not working with a relative path

# nohup <full_path>/background_changer.bash &
