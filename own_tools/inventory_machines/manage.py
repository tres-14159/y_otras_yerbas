#! /usr/bin/env python3
#
# daily_meeting.py
# Copyright (C) 2019  Miguel de Dios Matias (as developer)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# For to pretty json:
# cat machines.json | python3 -m json.tool

import json
import subprocess
import shlex
import os

machines = {}

def list_machines(fields = None):
    global machines
    
    i = 1
    for machine in machines['machines']:
        if not fields:
            print('{i}) {machine} {ip}'.format(
                i=i, machine=machine['machine'], ip=machine['ip']))
            aws = '✔️' if machine['aws'] else '❌'
            ssh = '✔️' if machine['ssh'] else '❌'
            print('{} aws: {aws}\tssh: {ssh}'.format(' ' * len(str(i) + ')'), aws=aws, ssh=ssh))
        else:
            field_values = []
            for field in fields:
                field_values.append('{}'.format(machine[field]))
            print('{i} {fields}'.format(i=i, fields=' '.join(field_values)))
        i +=1

def main():
    global machines
    
    with open('machines.json') as f:
        machines = json.load(f)
    
    print('')
    print('--- MANAGE -----')
    print('----------------')
    print('a) List machines')
    print('b) ssh machine')
    print('c) sshfs machine')
    print('----------------')
    print('0) Exit')
    print('----------------')
    choice = input('Choice (a,b,c,0)> ')
    print('')
    
    if choice == 'a':
        list_machines()
        main()
    elif choice == 'b':
        print('--- SELECT A MACHINE ---')
        print('------------------------')
        list_machines(('machine',))
        print('')
        
        choice = input('Choice (1 - {}, 0 = Exit)> '.format(len(machines['machines'])))
        index = int(choice) - 1
        if index > 0:
            command_line_template = 'ssh -i {key_file} {user}@{ip}'
            command_line = command_line_template.format(
                key_file = machines['machines'][index]['key_file'],
                ip = machines['machines'][index]['ip'],
                user = machines['machines'][index]['user'])
            print('Connect SSH....')
            print('')
            subprocess.run(shlex.split(command_line))
    elif choice == 'c':
        print('--- SELECT A MACHINE ---')
        print('------------------------')
        list_machines(('machine',))
        print('')
        
        choice = input('Choice (1 - {}, 0 = Exit)> '.format(len(machines['machines'])))
        index = int(choice) - 1
        if index > 0:
            command_line_template = 'sshfs -o IdentityFile={directory}/{key_file} {user}@{ip}:/ /mnt/sshfs1/'
            command_line = command_line_template.format(
                directory=os.getcwd(),
                key_file = machines['machines'][index]['key_file'],
                ip = machines['machines'][index]['ip'],
                user = machines['machines'][index]['user'])
            print('Connect SSHFS....')
            print('')
            subprocess.run(shlex.split(command_line))

if __name__ == '__main__':
    main()
