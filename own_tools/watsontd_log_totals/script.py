import pandas
import datetime
import math

a = pandas.read_csv("/tmp/tareas.csv", header=None, sep=';')

a['total_s'] = None
for i, row in a.iterrows():
    timeChunks = row[0].split()
    horas, minutos, segundos = 0, 0, 0
    for t in timeChunks:
        horas = int(t[:-1]) if 'h' in t else horas
        minutos = int(t[:-1]) if 'm' in t else minutos
        segundos = int(t[:-1]) if 's' in t else segundos
    a['total_s'][i] = datetime.timedelta(hours=horas, minutes=minutos, seconds=segundos).total_seconds()
del(a[0])

b = a.groupby(1).sum().reset_index()
b['h'], b['m'] = None, None
for i, row in b.iterrows():
    b['h'][i] = int(b['total_s'][i] / 3600)
    b['m'][i] = math.ceil(int(b['total_s'][i] % 3600) / 60)
