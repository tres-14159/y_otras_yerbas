#! /bin/bash

# set -x

function usage() {
    echo "$0 is for avoid shit form of fichaje in factorialhr web."
    echo "Usage: $0 [ -e <email> -p <password> -f <fichaje as day-hourstart-hourend>] [-t <file_fichaje_days> -d <day_start_of_week> ] [-m <month>]"
    echo "       day - number in month"
    echo "       hourstart - number hh:mm"
    echo "       hourend - number hh:mm"
    echo ""
    echo "Example run:"
    echo ""
    echo "$0 -p abracadrabra -f 3-8:00-16:00"
    echo ""
    echo "Other example run (with file):"
    echo ""
    echo "$0 -t aburrida_jornada_semanal.txt -d 3"
    echo ""
    echo "Note: File fichaje days format (plain file, line with value, not conf file or other formats):"
    echo "<email>"
    echo "<password>"
    echo "0-9:30-14:30"
    echo "0-15:30-18:30"
    echo "1-9:30-14:30"
    echo "1-15:30-18:30"
    echo "2-9:30-14:30"
    echo "2-15:30-18:30"
    echo "3-9:30-14:30"
    echo "3-15:30-18:30"
    echo "4-9:30-15:30"
}

function fichar() {
    url=$1
    month=$2
    year=$3
    fichaje=$4 
    day_start=${5:-"0"}   
    
    day=$(( $day_start + $(echo $fichaje | cut -d"-" -f1) ))
    hourstart=$(echo $fichaje | cut -d"-" -f2)
    hourend=$(echo $fichaje | cut -d"-" -f3)
    
    echo "INFO: Add $day/$month/$year $hourstart - $hourend"

    # heredoc (multiline) needs without indent and new line
json_payload=$(cat <<-JSON_EOF
{
    "period_id": $period_id,
    "clock_in": "$hourstart",
    "clock_out": "$hourend",
    "minutes": 0,
    "day": $day,
    "observations": null,
    "history":[]
}
JSON_EOF
)
    echo $json_payload | http post "$url_factorial/attendance/shifts" --session ./factorial.session.json --header
}

while getopts ":f:e:p:t:d:m:h" opt
do
    case "${opt}" in
        e)
            email=${OPTARG}
            ;;
        f)
            fichaje=${OPTARG}
            ;;
        p)
            password=${OPTARG}
            ;;
        t)
            template_file=${OPTARG}
            ;;
        d)
            day_start=${OPTARG}
            ;;
        m)
            month=${OPTARG}
            ;;
        h)
            usage
            exit 0
            ;;
    esac
done

url_factorial="https://api.factorialhr.com"
email=${email:-"mdedios@dotgiscorp.com"}

if [ ! -z "$template_file" ]
then
    email=$(sed "1q;d" "$template_file")
    password=$(sed "2q;d" "$template_file")
fi
if [ -z "$password" ]
then
    echo "ERROR: No password."
    usage
    exit 1
fi
year=$(date +%Y)
if [ -z "$month" ]
then
    month=$(date +%m)
fi

echo "INFO: Check previous session"

line_status_header=$(http "$url_factorial/notifications/tasks" --session ./factorial.session.json --headers | grep "HTTP/1.1" | tr -d '\r' | tr -d '\n')
status=${line_status_header#"HTTP/1.1 "}

if [[ ("$status" != "200 OK") || !(-f "./factorial.session.json") ]]
then
    rm factorial.session.json
    
    echo "INFO: Get authenticity_token token from factorialhr."
    
    authenticity_token=$(http "$url_factorial/users/sign_in" --session ./factorial.session.json | grep csrf-token | awk -F[\",\"] '{print $4}')


    echo "INFO: Login in factorialhr."

    http -f post "$url_factorial/users/sign_in" user[email]=$email user[password]=$password authenticity_token="$authenticity_token" return_host="factorialhr.com" commit="Sign in" user[remember_me]="0" --session ./factorial.session.json --header
fi


echo "INFO: Get period_id for $month and $year"
period_id=$(http get "$url_factorial/attendance/periods?year=$year&month=$month" --session ./factorial.session.json | jq ".[0].id")

if [ -z "$template_file" ]
then
    fichar "$url_factorial" $month $year $fichaje
else
    tail +3 "$template_file" | while IFS= read -r fichaje
    do
        fichar "$url_factorial" $month $year $fichaje $day_start
    done
fi

