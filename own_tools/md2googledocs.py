#!/usr/bin/env python3
# 
# md2googledocs.py
# Copyright (C) 2020  Miguel de Dios Matias (as developer)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


# Need to install:
#
# Pip package for google docs >= 1.12.5
# $ pip3 install --upgrade google-api-python-client google-auth-httplib2 google-auth-oauthlib
#
# Pandoc >= 2.5
# $ sudo apt install pandoc
#
#
# Need to create credentials.google_docs.json (name project: myproject or something shit and deskop app):
# https://developers.google.com/docs/api/quickstart/python#step_1_turn_on_the
#
# And for the google drive credentials.google_drive.json
# https://developers.google.com/drive/api/v3/quickstart/python#step_1_turn_on_the

import os
import json
import pickle
import argparse
import subprocess
import shlex

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow

SCOPES_DOCS = ['https://www.googleapis.com/auth/documents.readonly']
# ~ SCOPES_DRIVE = ['https://www.googleapis.com/auth/drive.metadata.readonly']

DOCUMENT_ID = '1QTFQP8fxSUlykZlqhA9skuh9Kf4hD9bUldLxRjCMiuk'

def main():
    global service
    
    parser = argparse.ArgumentParser(
        epilog='Need to create credentials.json (name project: myproject or something shit and deskop app): https://developers.google.com/docs/api/quickstart/python#step_1_turn_on_the')
    parser.add_argument("input_file", type=str, action="store", help="Input file")
    parser.add_argument("output_file", type=str, action="store", help="Output file")
    args = parser.parse_args()
    output_file = args.output_file
    
    try:
        command_line = shlex.split(f"pandoc -f markdown -t json {args.input_file}")
        json_output = subprocess.run(
            command_line, stdout=subprocess.PIPE).stdout.decode("utf-8")
        json_file = json.loads(json_output)
        
        creds = None
        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                # google docs
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.google_docs.json', SCOPES_DOCS)
                
                # google drive
                # ~ flow = InstalledAppFlow.from_client_secrets_file(
                    # ~ 'credentials.google_drive.json', SCOPES_DRIVE)
                
                creds = flow.run_local_server(port=0)
                # Save the credentials for the next run
                with open('token.pickle', 'wb') as token:
                    pickle.dump(creds, token)
        
        service = build('docs', 'v1', credentials=creds)
        
        
        document = service.documents().get(documentId=DOCUMENT_ID).execute(http=None, num_retries=0)
        
        
        
        # ~ service = build('drive', 'v3', credentials=creds)
        # ~ results = service.files().list(
            # ~ pageSize=10, fields="nextPageToken, files(id, name)").execute()
        # ~ items = results.get('files', [])

        # ~ if not items:
            # ~ print('No files found.')
        # ~ else:
            # ~ print('Files:')
            # ~ for item in items:
                # ~ print(u'{0} ({1})'.format(item['name'], item['id']))

        
    except FileNotFoundError:
        print("Error not found pandoc in your system.")

if __name__ == '__main__':
     main()
