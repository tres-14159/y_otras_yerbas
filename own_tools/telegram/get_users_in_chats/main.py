#! /usr/bin/env python3

# params.txt
# -i 123
# -s abc
#
# xargs -a params.txt python3 main.py

"""
Useful queries:

CREATE VIEW view_users AS
SELECT
    users.id AS user_id,
    users.username,
    (users.first_name || ' ' || users.last_name) AS full_name,
    GROUP_CONCAT(chats.name, ', ') AS chat_names
FROM
    userInChats
INNER JOIN
    users ON users.id = userInChats.id_user
INNER JOIN
    chats ON chats.id = userInChats.id_chat
GROUP BY
    users.id, users.username, full_name;
"""

from telethon import TelegramClient, events, sync
import argparse
import sqlite3

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--api_id", type=int, action="store",
        help="Api id", required=True)
    parser.add_argument("-s", "--api_hash", type=str, action="store",
        help="Api hash", required=True)

    print("[INFO] Getting terminal args.")
    
    args = parser.parse_args()
    print(f"[DEBUG] The terminal args: \"{args}\" ")
    
    print("[INFO] Connecting to Telegram (maybe need double login).")
    client = TelegramClient('python', args.api_id, args.api_hash)
    client.start()
    
    print("[INFO] Connecting to database.")
    con = sqlite3.connect('telegram.sqlite3')
    cur = con.cursor()
    cur.execute('CREATE TABLE IF NOT EXISTS users (id INTEGER NOT NULL, username TEXT, first_name TEXT, last_name TEXT);')
    cur.execute('CREATE TABLE IF NOT EXISTS chats (id INTEGER NOT NULL, name TEXT);')
    cur.execute('CREATE TABLE IF NOT EXISTS userInChats (id_user INTEGER NOT NULL, id_chat INTEGER NOT NULL, datetime INTEGER NOT NULL);')
    con.commit()
    
    for chat in client.iter_dialogs():
        if chat.is_group:
            name = chat.name.replace('"', '\\"').replace("'", "\\'")
            print(f"[INFO] Get users from chat ({chat.id}){name}.")
            if cur.execute(f"SELECT COUNT(*) FROM chats WHERE id = {chat.id}").fetchone()[0] == 0:
                cur.execute(f"INSERT INTO chats (id, name) VALUES (?, ?);", (chat.id, name))
                con.commit()
            print(f"[INFO] There are {len(list(client.iter_participants(chat)))} users in ({chat.id}){chat.name}.")
            for user in client.iter_participants(chat):
                if cur.execute(f"SELECT COUNT(*) FROM users WHERE id = {user.id}").fetchone()[0] == 0:
                    username = ('' if user.username is None else user.username).replace('"', '\\"').replace("'", "\\'")
                    first_name = ('' if user.first_name is None else user.first_name).replace('"', '\\"').replace("'", "\\'")
                    last_name = ('' if user.last_name is None else user.last_name).replace('"', '\\"').replace("'", "\\'")
                    cur.execute(f"INSERT INTO users (id, username, first_name, last_name) VALUES (?, ?, ?, ?);", (user.id, username, first_name, last_name))
                    con.commit()
                cur.execute(f"INSERT INTO userInChats (id_user, id_chat, datetime) VALUES (?, ?, strftime('%s', 'now'));", (user.id, chat.id))
    con.close()

if __name__ == '__main__':
    main()
