#! /usr/bin/env python3

# Mirar albunes como grouped_id
# ~ In [30]: for x in t.iter_messages(a, 20, reverse=True):
    # ~ ...:     print(x.id, x.grouped_id)
    # ~ ...: 
# ~ 1 None
# ~ 4 12888287497937226
# ~ 5 12888287497937226
# ~ 6 12888346025981578
# ~ 7 12888346025981578
# ~ 8 12888346025981578
# ~ 9 12888346025981578
# ~ 10 12888432352691458
# ~ 11 12888432352691458
# ~ 12 12888432352691458
# ~ 13 12888432352691458
# ~ 14 12888518598323738
# ~ 15 12888518598323738
# ~ 16 12888518598323738
# ~ 17 12888518598323738
# ~ 18 12888518598323738
# ~ 19 12888518598323738
# ~ 20 12888518598323738
# ~ 21 12888518598323738
# ~ 22 12888604903823802



# pip3 install telethon

# api_hash
# api_id 
# from https://my.telegram.org/auth?to=apps

import json
import os

from telethon import TelegramClient, events, sync
import argparse

def save_telegram_data(telegram_data, root_dir, file_name = "telegram_data.json"):
    os.chdir(root_dir)
    with open("telegram_data.json", "w") as f:
        telegram_data = json.dump(telegram_data, f)

def create_dir_and_go(dir_photos, root_dir):
    os.chdir(root_dir)
    try:
        os.makedirs(dir_photos)
    except FileExistsError as e:
        pass
    os.chdir(dir_photos)

def download_album(album, root_dir):
    dir_photos = 'photos'
    create_dir_and_go(dir_photos, root_dir)
    
    last_id = 0
    for album_message in album:
        text = album_message.get_entities_text()
        if len(text) > 1:
            model = text[1][1]
            character = text[0][1]
            dir_photos = f'{model}/{character}'
            create_dir_and_go(dir_photos, root_dir)
        elif len(text) > 0:
            model = text[0][1]
            dir_photos = f'{model}'
            create_dir_and_go(dir_photos, root_dir)
    for album_message in album:
        print(f"[INFO] Saving image in \"{dir_photos}\"")
        album_message.download_media()
        last_id = album_message.id
    return last_id

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--api_id", type=int, action="store",
        help="Api id", required=True)
    parser.add_argument("-s", "--api_hash", type=str, action="store",
        help="Api hash", required=True)
    parser.add_argument("-c", "--channel_id", type=int, action="store",
        help="Channel id")
    parser.add_argument("-m", "--limit", type=int, action="store",
        help="Amount albums to get", default=20)
    parser.add_argument("-l", "--list", action="store_true")

    print("[INFO] Getting terminal args.")

    args = parser.parse_args()
    print(f"[DEBUG] The terminal args: \"{args}\" ")
    
    print("[INFO] Connecting to Telegram (maybe need double login).")
    client = TelegramClient('python', args.api_id, args.api_hash)
    client.start()
    
    if args.list:
        print("[INFO] List your channels in Telegram")
        for chat in client.iter_dialogs():
            print('{:<20}{}'.format(chat.id, chat.name))
    else:
        print("[INFO] Get last id message/media downloaded")
        try:
            with open("telegram_data.json", "r") as f:
                telegram_data = json.load(f)
        except FileNotFoundError as e:
            telegram_data = dict()
            telegram_data[f'{args.channel_id}'] = dict()
            telegram_data[f'{args.channel_id}']['last_message'] = 0
        
        chat = client.get_entity(args.channel_id)

        root_dir = os.getcwd()
        
        amount = 0
        id_group = 0
        album = []
        for message in client.iter_messages(chat, offset_id=telegram_data[f'{args.channel_id}']['last_message'], reverse=True):
            print(f"[INFO] Getting data message {message.id} in group {message.grouped_id} with media {message.media is not None} and text \"{message.message}\"")
            if id_group == message.grouped_id:
                album.append(message)
            else:
                if len(album) > 0:
                    print(f"[INFO] Download previous album ({len(album)} photos)")
                    telegram_data[f'{args.channel_id}']['last_message'] = download_album(album, root_dir)
                    save_telegram_data(telegram_data, root_dir)
                
                if args.limit == amount:
                    print(f"[INFO] Reached the limit {args.limit}")
                    break
                
                album = []
                album.append(message)
                id_group = message.grouped_id
                amount += 1
                

if __name__ == '__main__':
    main()
