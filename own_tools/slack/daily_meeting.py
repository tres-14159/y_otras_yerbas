#!/usr/bin/env python3
#
# daily_meeting.py
# Copyright (C) 2019  Miguel de Dios Matias (as developer)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# Requirements:
# pip install slackclient

# Token
# https://api.slack.com/custom-integrations/legacy-tokens
# legacy-token == user_token (webapi and easy...but legacy...f*ck off) I 💖 XMPP.

import argparse
import datetime
import sys

from slackclient import SlackClient

template_message_daily_meeting = """
*Daily meeting*
*¿Qué hice ayer? ({date})*
{yesterday_task}
*¿Qué voy a hacer hoy?*
{today_task}
*Impedimentos*
{impediments}
"""

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--slack_token", type=str, action="store",
        help="The slack token for API", default=None)
    parser.add_argument("-c", "--channel", type=str, action="store",
        help="The slack Channel to send ", default=None)
    parser.add_argument("-d", "--debug", action="store_true",
        help="Debug (send to own channel to test)")
    args = parser.parse_args()
    slack_token = args.slack_token
    channel = args.channel
    debug = args.debug
    
    if (len(sys.argv) == 1) or (slack_token is None):
        parser.print_help()
        exit(1)
    
    sc = SlackClient(slack_token)
    
    if not sc.api_call('api.test')['ok']:
        print('ERROR: can not connect to slack please check token or connection.')
        exit(1)
    
    if debug:
        user_id = sc.api_call('auth.test')['user_id']
        api_channel = user_id
    else:
        channels = sc.api_call('conversations.list', types='private_channel')['channels']
        channel_id = None
        for channel_slack in channels:
            if channel_slack['name'] == channel:
                channel_id = channel_slack['id']
        if channel_id is None:
            print('ERROR: can not found the channel "{}".'.format(channel))
            exit(1)
        api_channel = channel_id
    
    yesterday = []
    while True:
        yesterday.append(input("What did you yesterday?"))
        if input("More task? [Y/n]>").upper() == 'N':
            break
    today = []
    while True:
        today.append(input("What did you today?>"))
        if input("More task? [Y/n]>").upper() == 'N':
            break
    
    impediments = []
    while input("Impediments? [y/N]>").upper() == 'Y':
        impediments.append(input("What is the impediment?>"))
    
    message_daily_meeting = template_message_daily_meeting.format(
        date="{0:%d/%m/%Y - %A}".format(datetime.date.today()),
        yesterday_task="".join(["• {}\n".format(task) for task in yesterday]),
        today_task="".join(["• {}\n".format(task) for task in today]),
        impediments="".join(["• {}\n".format(impediment) for impediment in impediments]) if len(impediments) > 0 else "Ninguno.")
    
    
    sc.api_call(
      "chat.postMessage",
      channel=api_channel,
      text=message_daily_meeting,
      as_user=True,
      mrkdwn=True
    )

if __name__ == '__main__':
    main()
