#! /bin/bash

#~ generate_random_csv.bash
#~ Copyright (C) 2019 Miguel de Dios Matias

#~ This program is free software: you can redistribute it and/or modify
#~ it under the terms of the GNU General Public License as published by
#~ the Free Software Foundation, either version 3 of the License, or
#~ (at your option) any later version.

#~ This program is distributed in the hope that it will be useful,
#~ but WITHOUT ANY WARRANTY; without even the implied warranty of
#~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#~ GNU General Public License for more details.

#~ You should have received a copy of the GNU General Public License
#~ along with this program. If not, see <http://www.gnu.org/licenses/>

function usage() {
    echo "$0 is for generate random csv file."
    echo "Usage: $0 <file.csv> [-l <lines>] [-s <char>] [-e] [-h]"
    echo "       file.csv - name of file in csv format"
    echo "       lines - number of lines"
    echo "       -e - add header"
    echo "       -h - show this help"
    echo ""
}

lines=300
separator=";"
header=0
filename="/dev/stdout"
for i in $(seq $#)
do
    next=$(( i + 1 ))
    case "${!i}" in
        "-l")
            lines=${!next}
            ;;
        "-s")
            separator=${!next}
            ;;
        "-e")
            header=1
            ;;
        "-h")
            usage
            ;;
        *)
            echo "${!i}" | grep -e ".*\.csv" > /dev/null
            if [ $? -eq 0 ]
            then
                filename=${!i}
            fi
            ;;
    esac
done

echo -n "" > $filename
if [ $header -eq 1 ]
then
    echo "FIELD_FLOAT"$separator"FIELD_INTEGER"$separator"FIELD_STRING"$separator"FIELD_DATE" >> $filename
fi
for i in $(seq 1 $lines)
do
    echo -n $( echo "$RANDOM * $RANDOM / 10000" | bc -l)"$separator"  >> $filename
    echo -n $(($RANDOM % 100 + 1))"$separator" >> $filename
    echo -n $(cat /dev/urandom | tr -dc 'A-Z' | fold -w 12 | head -n 1)"$separator" >> $filename
    echo $(date) >> $filename
done
