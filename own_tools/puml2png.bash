#! /bin/bash

#~ puml2png.bash
#~ Copyright (C) 2019 Miguel de Dios Matias

#~ This program is free software: you can redistribute it and/or modify
#~ it under the terms of the GNU General Public License as published by
#~ the Free Software Foundation, either version 3 of the License, or
#~ (at your option) any later version.

#~ This program is distributed in the hope that it will be useful,
#~ but WITHOUT ANY WARRANTY; without even the implied warranty of
#~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#~ GNU General Public License for more details.

#~ You should have received a copy of the GNU General Public License
#~ along with this program. If not, see <http://www.gnu.org/licenses/>


if [ $# -lt 1 ]
then
    echo "$0 : generate a png from plantuml file, the png file has the same name of plantuml. And added this diagram into metadata image."
    echo ""
    echo "To get the metadata is with exiftool (in commmand line in GNU/Linux):"
    echo "    exiftool -diagram -b <file.png>"
    echo "Example:"
    echo "    $0 <file.puml>"
    exit 1
fi

which plantuml > /dev/null
if [ $? -eq 1 ]
then
    ls plantuml.jar > /dev/null
    if [ $? -ne 0 ]
    then
        wget https://datapacket.dl.sourceforge.net/project/plantuml/plantuml.jar
    fi
fi

file=$1
basefile=$(echo $1 | sed 's/.puml//')
filepng=$(echo $basefile.png)

java -jar plantuml.jar $file -tpng filepng

convert $filepng -set diagram: "$(cat $file)" $filepng
