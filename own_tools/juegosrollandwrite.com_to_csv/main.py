#!/usr/bin/env python3

import re
import requests
from bs4 import BeautifulSoup

docker_output_dir='/pedo'

pages = 60
not_titles_game = ['5 Juegos Roll', 'HISTORIA DE']

games = []


for page in range(60, 0, -1):
    if page == 1:
        page_url = ''
    else:
        page_url = f'page/{page}/'
    
    url = f'https://juegosrollandwrite.com/tag/print-and-play/{page_url}'
    print(f'[INFO] Scrapping {url}')
    req = requests.get(url)

    soup = BeautifulSoup(req.text, 'lxml')
    mainContent = soup.find_all('div', id='main-content')

    temp = []
    for article in mainContent[0].find_all('article'):
        header = article.find('header')
        link = header.find('a')
        name = link.text.strip()
        name = re.sub('\s+', ' ', name)
        url = link.get('href')
        if not any([name.startswith(no_game_title) for no_game_title in not_titles_game]):
            temp.append({'name': name, 'url': url})
    print(f'[INFO] Added {len(temp)} games')
    games = temp + games

with open(f'/{docker_output_dir}/games.csv', 'w') as f:
    f.write('name;url\n')
    for game in games:
        f.write(f"{game['name']};{game['url']}\n")
