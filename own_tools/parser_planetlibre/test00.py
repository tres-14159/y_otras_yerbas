# License GPL3

# Parse and get rss links from planetlibre

from bs4 import BeautifulSoup
import urllib.request
import requests # Atareao.es fuck security needs user agent
from urllib.parse import urlparse

list_rss = set()

def get_rss(url):
    try:
        headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'}
        r = requests.get(url, headers=headers)
        soup = BeautifulSoup(r.content.decode(), 'html.parser')
        if 'rss' in str(soup):
            for link in soup.select('link'):
                type_link = link.get('type')
                if type_link:
                    if 'rss' in type_link:
                        rss_url = link.get('href')
                        if not ('comments' in rss_url):
                            if not urlparse(rss_url).netloc:
                                if url[-1] == '/' and rss_url[0] == '/':
                                    rss_url = url + rss_url[1:]
                                else:
                                    rss_url = url + rss_url
                            print(rss_url)
                            list_rss.add(rss_url)
    except Exception as e:
        print(e)

found_list = False
with urllib.request.urlopen('https://planetlibre.es/blogs/') as response:
    soup = BeautifulSoup(response, 'html.parser')
    for post in soup.select('.post-row'):
        for div in post.select('.col-md-12'):
            if found_list:
                for elem in div.select('a'):
                    blog = elem.contents.pop().strip()
                    url = elem.get('href')
                    print('-------------------------------------------')
                    print(blog, url)
                    get_rss(url)
            else:
                for elem in div.select('h2'):
                    if 'Blogs' in elem.contents:
                        found_list = True
                        break

temp = '<ul>'
for rss in list_rss:
	temp += '<li><a href="{}">{}</a></li>'.format(rss, rss)
temp += '</ul>'

html_temp = BeautifulSoup(temp)
with open('/tmp/list_rss.html', 'w') as f:
	f.write(html_temp.prettify())