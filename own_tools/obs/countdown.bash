#! /bin/bash

#~ clock.sh
#~ Copyright (C) 2022 Miguel de Dios Matias

#~ This program is free software: you can redistribute it and/or modify
#~ it under the terms of the GNU General Public License as published by
#~ the Free Software Foundation, either version 3 of the License, or
#~ (at your option) any later version.

#~ This program is distributed in the hope that it will be useful,
#~ but WITHOUT ANY WARRANTY; without even the implied warranty of
#~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#~ GNU General Public License for more details.

#~ You should have received a copy of the GNU General Public License
#~ along with this program. If not, see <http://www.gnu.org/licenses/>.

if ! command -v toilet &> /dev/null
then
    (>&2 echo 'ERROR: "toilet" is not installed in your system. Maybe you can install with "apt install toilet"')
    exit 1
fi

if ! command -v figlet &> /dev/null
then
    (>&2 echo 'ERROR: "figlet" is not installed in your system. Maybe you can install with "apt install figlet"')
    exit 1
fi

function hr {
    for i in $(seq $COLUMNS)
    do
        echo -n "-"
    done
}

function list_filters() {
    toilet --filter list
    echo '"random" : select a random filter'
    exit 0
}

function random_filter() {
    filters=()
    for f in $(toilet --filter list | tail -n +2 | cut -d: -f1 | tr -d "\"")
    do
        filters+=($f)
    done
    rand_index=$(( $RANDOM % ${#filters[@]} ))
    echo ${filters[$rand_index]}
}

function list_fonts() {
    OLDIFS=$IFS
    IFS=$'\n'
    for f in /usr/share/figlet/*.[tf]lf
    do
        filename=$(basename "$f")
        font=${filename%.*}
        echo "$font"
        hr
        toilet -f $font "$font"
        echo ""
    done
    IFS=$OLDIFS
    echo "random"
    hr
    echo "Choose a random font."
    exit 0
}

function random_font() {
    fonts=()
    OLDIFS=$IFS
    IFS=$'\n'
    for f in /usr/share/figlet/*.[tf]lf
    do
        filename=$(basename "$f")
        font=${filename%.*}
        fonts+=($font)
    done
    IFS=$OLDIFS
    rand_index=$(( $RANDOM % ${#fonts[@]} ))
    echo ${fonts[$rand_index]}
}

function help() {
    echo "$0 [Options] <time>"
    echo ""
    echo "<time>                             : Time format is 1s to 999s or upper. Or 1m to 999m. Or 1h to 999h. And mixed 1h1m1s to 999h999m999s."
    echo "--filter       | -f <toiletfilter> : Use the toilet filters (can you repeat this option) for the countdown."
    echo "                                     Note: the order of filter is important \"--filter metal --filter box\" != \"--filter box --filter metal\" ."
    echo "                                     Note: You can repeat filter sutch as several boxes."
    echo "--list-filters                     : List the toilet filters."
    echo "--font         | -t <font>         : Use the toilet/figlet font."
    echo "--list-fonts                       : List the toilet filters."
    echo "--only-seconds | -s                : Show a seconds countdown instead a clock."
    echo "--microseconds | -m                : Show the microseconds as 11:23:44.666 or if --only-seconds as 42.666 ."
    echo "--end-text     | -e <end_text>     : The end text to show when the countdown finished. By default: Boom!!!"
    echo "--debug                            : Show the toilet command base to paint the countdown."
    echo "--help         | -h                : Show this help."
    echo ""
    echo "Example:"
    echo "$0 --filter metal --filter border -t random 31h415s"
    echo ""
    echo "Copyright (C) 2022 Miguel de Dios Matias Licensed GPLv3+ ."
    exit 0
}

LONG_OPTION_LIST=(
    "filter:"
    "list-filters"
    "font:"
    "list-fonts"
    "only-seconds"
    "microseconds"
    "end-text:"
    "debug"
    "help"
)
SORT_OPTION_LIST=(
    "f:"
    "t:"
    "s"
    "m"
    "e:"
    "h"
)
# Read the parameters
opts=$(getopt -q \
  --longoptions "$(printf "%s," "${LONG_OPTION_LIST[@]}")" \
  --name "$(basename "$0")" \
  --options "$(printf "%s" "${SORT_OPTION_LIST[@]}")" \
  -- "$@"
)
eval set -- "$opts"


debug=0
only_seconds=0
microseconds=0
font="random"
end_text="Boom!!!"
filters=()
unamed_options=()
# It it is same a queue (process the head) because $1 and $2
while true
do
    case "$1" in
        --filter | -f)
            filters+=($2)
            shift 1
            ;;
        --list-filters)
            list_filters
            shift 1
            ;;
        --font | -t)
            font=$2
            shift 1
            ;;
        --list-fonts)
            list_fonts
            shift 1
            ;;
        --only-seconds | -s)
            only_seconds=1
            ;;
        --microseconds | -m)
            microseconds=1
            ;;
        --end-text | -e)
            end_text=$2
            shift 1
            ;;
        --debug)
            debug=1
            ;;
        --help | -h)
            help
            shift 1
            ;;
        --)
            # End options now the unamed options
            ;;
        *)
            unamed_options+=("$1")
            ;;
    esac
    shift 1
    if [ $# -eq 0 ]
    then
        break
    fi
done

if [ ${#unamed_options[@]} -eq 0 ]
then
    help
fi

command="toilet -t " # -t is to use term width.

if [ "$font" = "random" ]
then
    font=$(random_font)
fi
command="$command --font=\"$font\""

if [ ${#filters[@]} -ne 0 ]
then
    for i in $(seq 0 $(( ${#filters[@]} - 1 )) )
    do
        filter=${filters[$i]} 
        if [ "$filter" = "random" ]
        then
            filter=$(random_filter)
        fi
        command="$command --filter $filter"
    done
fi


if [ ${#unamed_options[@]} -ne 1 ]
then
    (>&2 echo "ERROR: Please only $0 needs one string as 12s or 12m34s, don't separate the time count as 12m 34s . ")
    exit 1
fi
count_down=${unamed_options[0]}

total_minutes=0
if [[ "$count_down" =~ ^[0-9]+h[^h]*$ ]]
then
    total_minutes=$(( $(echo $count_down | cut -dh -f1) * 60 ))
    count_down=$(echo $count_down | cut -dh -f2)
fi

total_seconds=0
if [[ "$count_down" =~ ^[0-9]+m[^m]*$ ]]
then
    total_seconds=$(( ($total_minutes + $(echo $count_down | cut -dm -f1)) * 60 ))
    count_down=$(echo $count_down | cut -dm -f2)
fi

if [[ "$count_down" =~ ^[0-9]+s$ ]]
then
    total_seconds=$(( $total_seconds + $(echo $count_down | cut -ds -f1) ))
fi

if [[ $total_seconds -eq 0 ]]
then
    (>&2 echo "Error: 0 seconds...are you sure?")
    exit 1
fi

if [ $only_seconds -eq 1 ]
then
    count_digits=$(echo -n $total_seconds | wc -c)
else
    echo "TO-DO: Show a typical countdown clock (as casio clock)."
    echo "       Please use --only-seconds | -s ."
    exit 0
fi
log_error=$(eval $command 666 2>&1)
if [ $? -ne 0 ]
then
    echo "$command"
    (>&2 echo $log_error)
    exit 1
fi

reset
if [ $microseconds -eq 0 ]
then
    wait="0.8"
    init_time=$(date +%s)
else
    wait="0.1"
    init_time=$(date +"%s.%3N")
fi

while true
do
    if [ $microseconds -eq 0 ]
    then
        delta=$(( $(date +%s) - $init_time ))
        last_seconds=$(( $total_seconds - $delta))
        string_seconds=$(printf "%0${count_digits}d" $last_seconds)
    else
        delta=$( echo $(date +"%s.%3N")" - $init_time" | bc -l )
        last_seconds=$( echo "$total_seconds - $delta" | bc -l )
        last_seconds_integer=$( echo $last_seconds | cut -d. -f1 )
        string_seconds=$(printf "%0${count_digits}d" $last_seconds_integer)
        last_seconds_decimal=$( echo $last_seconds | cut -d. -f2 )
        string_seconds="$string_seconds.$last_seconds_decimal"
    fi
    if (( $(echo "$last_seconds <= 0" |bc -l) ))
    then
        # TO-DO ADD A OPTION TO SHOW A MESSAGE
        clear
        toilet="$command $end_text"
        eval $toilet
        exit 0
    fi
    if [ $only_seconds -eq 1 ]
    then
        toilet="$command $string_seconds"
    fi
    clear
    if [ $debug -eq 1 ]
    then
        echo $toilet
        exit 0
    else
        eval $toilet
    fi
    sleep $wait
done
