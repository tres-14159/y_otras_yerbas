#!/usr/bin/env python3
# 
# megascript_transifex.py
# Copyright (C) 2019  Miguel de Dios Matias (as developer)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import sys
import argparse
import datetime
import pickle
import pprint
import json
import random
import requests

from concurrent.futures import ProcessPoolExecutor

# ~ resource_source = DEFAULT_RESOURCE_SOURCE = 'stable-cataclysm-dda'
# ~ language_source = DEFAULT_LANGUAGE_SOURCE = 'es_AR'
# ~ resource_destination = DEFAULT_RESOURCE_DESTINATION = 'stable-cataclysm-dda'
# ~ language_destination = DEFAULT_LANGUAGE_DESTINATION = 'es_ES'
resource_source = DEFAULT_RESOURCE_SOURCE = 'master-cataclysm-dda'
language_source = DEFAULT_LANGUAGE_SOURCE = 'es_AR'
resource_destination = DEFAULT_RESOURCE_DESTINATION = 'master-cataclysm-dda'
language_destination = DEFAULT_LANGUAGE_DESTINATION = 'es_ES'

auth = ('api', '1/68307e7cd3587af8d34ffb90fef7d1b92be36acd')

def get_url(language, resource):
    url_base = 'https://www.transifex.com/api/2/project/cataclysm-dda/resource/{resource}/translation/{language}/'
    return url_base.format(resource=resource, language=language)

def empty_translation(entry):
    if type(entry['translation']) == str and entry['translation'] == '':
        return True
    if type(entry['translation']) == dict and not any(entry['translation'].values()):
        return True
    return False

def get_strings():
    global resource_source, language_source, resource_destination, language_destination
    
    url = get_url(language_destination, resource_destination) + 'strings/'
    print(datetime.datetime.now(), "GET STRINGS", url)
    r = requests.get(url, auth=auth)
    print(url, r.status_code)
    try:
        destination_data = r.json()
    except Exception as e:
        print(e)
        print(r)
        exit(1)
    url = get_url(language_source, resource_source) + 'strings/'
    print(datetime.datetime.now(), "GET STRINGS", url)
    r = requests.get(url, auth=auth)
    print(url, r.status_code)
    source_data = r.json()
    
    print(datetime.datetime.now(), "GET EMPTY STRINGS")
    destination_data_empty = [entry for entry in destination_data if empty_translation(entry)]
    source_data_empty = [entry for entry in source_data if empty_translation(entry)]
    source_data_not_empty = [entry for entry in source_data if not empty_translation(entry)]
    
    print(language_destination, resource_destination, 'count', len(destination_data))
    print(language_source, resource_source, 'count', len(source_data))
    print(language_destination, resource_destination, 'empty count', len(destination_data_empty))
    print(language_source, resource_source, 'count empty', len(source_data_empty))
    print(language_source, resource_source, 'count not empty', len(source_data_not_empty))
    
    print(datetime.datetime.now(), "RETURN GET_STRINGS")
    return (destination_data, source_data, destination_data_empty, source_data_empty, source_data_not_empty)

def upload_entry(entry):
    global resource_source, language_source, resource_destination, language_destination
    
    data = {}
    data['translation'] = entry['translation']
    
    print(datetime.datetime.now(), "UPLOADING")
    url = get_url(language_destination, resource_destination) + 'string/' + entry['string_hash'] + '/'
    r = requests.put(url, auth=auth, data=json.dumps(data), headers = {'Content-Type': 'application/json'})
    print(datetime.datetime.now(), "UPLOADED", url, r.status_code)
    if r.status_code != 200:
        print("##ERROR")
        print(r.content)
        print(json.dumps(data))
        print("######")

def fill_empty_translations(destination_data, source_data, destination_data_empty, source_data_empty, source_data_not_empty):
    global resource_source, language_source, resource_destination, language_destination
    
    pool = ProcessPoolExecutor(30)
    
    print(datetime.datetime.now(), "FILL EMPTY TRANSLATIONS", language_destination, resource_destination)
    for destination_entry in destination_data_empty:
        print('.', end='')
        for entry in source_data_not_empty:
            if not empty_translation(entry):
                equal = destination_entry['string_hash'] == entry['string_hash']
                if not equal:
                    equal = destination_entry['source_string'] == entry['source_string']
                if equal:
                    print('')
                    print(language_destination, resource_destination)
                    pprint.pprint(destination_entry)
                    print(language_source, resource_source)
                    pprint.pprint(entry)
                    added = input("Added? [Y/n]>")
                    if added == 'y' or added == "":
                        if type(destination_entry['translation']) ==  str and type(entry['translation']) == dict:
                            destination_entry['translation'] = entry['translation']['1']
                        elif type(entry['translation']) ==  str and type(destination_entry['translation']) == dict:
                            destination_entry['translation']['1'] = entry['translation']
                            destination_entry['translation']['5'] = entry['translation']
                        else:
                            destination_entry['translation'] = entry['translation']
                        pool.submit(upload_entry, destination_entry)

def main():
    global resource_source, language_source, resource_destination, language_destination
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-rs", "--resource_source", type=str, action="store",
                    help="Resource source", default=DEFAULT_RESOURCE_SOURCE)
    parser.add_argument("-ls", "--language_source", type=str, action="store",
                    help="Language source", default=DEFAULT_LANGUAGE_SOURCE)
    parser.add_argument("-rd", "--resource_destination", type=str, action="store",
                    help="Resource destination", default=DEFAULT_RESOURCE_DESTINATION)
    parser.add_argument("-ld", "--language_destination", type=str, action="store",
                    help="Language destination", default=DEFAULT_LANGUAGE_DESTINATION)
    args = parser.parse_args()
    resource_source = args.resource_source
    language_source = args.language_source
    resource_destination = args.resource_destination
    language_destination = args.language_destination
    
    print(datetime.datetime.now(), "COMAND LINE ARGS")
    print(datetime.datetime.now(), "resource_source", resource_source)
    print(datetime.datetime.now(), "language_source", language_source)
    print(datetime.datetime.now(), "resource_destination", resource_destination)
    print(datetime.datetime.now(), "language_destination", language_destination)
    
    print(datetime.datetime.now(), "STARTING")
    try:
        with open('data.pkl', 'rb') as input:
            destination_data = pickle.load(input)
            source_data = pickle.load(input)
            destination_data_empty = pickle.load(input)
            source_data_empty = pickle.load(input)
            source_data_not_empty = pickle.load(input)
    except FileNotFoundError:
        destination_data, source_data, destination_data_empty, source_data_empty, source_data_not_empty = get_strings()
        with open('data.pkl', 'wb') as output:
            pickle.dump(destination_data, output, pickle.HIGHEST_PROTOCOL)
            pickle.dump(source_data, output, pickle.HIGHEST_PROTOCOL)
            pickle.dump(destination_data_empty, output, pickle.HIGHEST_PROTOCOL)
            pickle.dump(source_data_empty, output, pickle.HIGHEST_PROTOCOL)
            pickle.dump(source_data_not_empty, output, pickle.HIGHEST_PROTOCOL)
    else:
        print(datetime.datetime.now(), "LOADED STRINGS FROM FILE")
    
    random.shuffle(destination_data_empty) # Avoid boring same strings start all time
    fill_empty_translations(
            destination_data, source_data, destination_data_empty, source_data_empty, source_data_not_empty)

if __name__ == '__main__':
    main()
