import { useState } from 'react';

export default function Login() {
  const [usuario, setUsuario] = useState('');
  const [password, setPassword] = useState('');

  return (
    <>
      <h2>Login</h2>
      <form>
        <input type="text" value={usuario} placeholder="usuario" onChange={(e) => setUsuario(e.target.value)} />
        <input type="password" value={password} placeholder="password" onChange={(e) => setPassword(e.target.value)} />
        <input type="button" value="Login" />
      </form>
    </>
  );
}
