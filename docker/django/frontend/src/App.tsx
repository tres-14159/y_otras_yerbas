import './App.css'

import { Routes, Route, Link, Navigate } from "react-router-dom";

import Inicio from './Inicio.tsx';
import Productos from './Productos.tsx';
import Clientes from './Clientes.tsx';
import Login from './Login.tsx';

function App() {
  let a: boolean = false;
  
  return (
    <>
     {a == false && (
          <Navigate to="/login" replace={true} />
     )}
      <h1>TIENDA</h1>
      <nav>
        <ul>
          <li>
            <Link to="/">Inicio</Link>
          </li>
          <li>
            <Link to="/productos">Productos</Link>
          </li>
          <li>
            <Link to="/clientes">Clientes</Link>
          </li>
        </ul>
      </nav>
      <Routes>
        <Route path="/" element={<Inicio />} />
        <Route path="/productos" element={<Productos />} />
        <Route path="/clientes" element={<Clientes />} />
        <Route path="/login" element={<Login />} />
      </Routes>
    </>
  )
}

export default App
