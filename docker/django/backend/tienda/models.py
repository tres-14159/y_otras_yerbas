from django.db import models

class Producto(models.Model):
    nombre = models.CharField(max_length=200)
    descripcion = models.TextField()
    precio_actual = models.FloatField(default=0)

class Cliente(models.Model):
    nombre = models.CharField(max_length=200)
    direccion = models.TextField()
    porcentaje_descuento_actual = models.FloatField(default=0)

class Factura(models.Model):
    cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    fecha = models.DateField(auto_now = True)
    porcentaje_descuento = models.FloatField(default=0)

class LineaFactura(models.Model):
    factura = models.ForeignKey(Factura, on_delete=models.CASCADE)
    productos = models.ForeignKey(Producto, on_delete=models.CASCADE)
    cantidad = models.FloatField(default=0)
    precio = models.FloatField(default=0)
