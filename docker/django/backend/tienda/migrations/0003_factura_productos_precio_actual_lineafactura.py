# Generated by Django 4.2.2 on 2023-08-24 11:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tienda', '0002_cliente'),
    ]

    operations = [
        migrations.CreateModel(
            name='Factura',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fecha', models.DateField(auto_now=True)),
                ('porcentaje_descuento', models.FloatField(default=0)),
                ('cliente', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tienda.cliente')),
            ],
        ),
        migrations.AddField(
            model_name='productos',
            name='precio_actual',
            field=models.FloatField(default=0),
        ),
        migrations.CreateModel(
            name='LineaFactura',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('cantidad', models.FloatField(default=0)),
                ('precio', models.FloatField(default=0)),
                ('factura', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tienda.factura')),
                ('productos', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tienda.productos')),
            ],
        ),
    ]
