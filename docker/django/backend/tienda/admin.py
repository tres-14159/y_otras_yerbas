from django.contrib import admin

from tienda.models import Producto, Cliente, Factura, LineaFactura

admin.site.register(Producto)
admin.site.register(Cliente)
admin.site.register(Factura)
admin.site.register(LineaFactura)
