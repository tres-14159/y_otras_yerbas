from django.http import HttpResponse
from rest_framework import viewsets, permissions
from tienda.models import Producto, Cliente, Factura, LineaFactura
from tienda.serializer import ProductoSerializer, ClienteSerializer, FacturaSerializer, LineaFacturaSerializer


def index(request):
    return HttpResponse("Hello, world. You're at the polls index.")

class ProductoViewSet(viewsets.ModelViewSet):
    queryset = Producto.objects.all()
    serializer_class = ProductoSerializer
    permission_classes = [permissions.IsAuthenticated]

class ClienteViewSet(viewsets.ModelViewSet):
    queryset = Cliente.objects.all()
    serializer_class = ClienteSerializer
    permission_classes = [permissions.IsAuthenticated]

class FacturaViewSet(viewsets.ModelViewSet):
    queryset = Factura.objects.all()
    serializer_class = FacturaSerializer
    permission_classes = [permissions.IsAuthenticated]

class LineaFacturaViewSet(viewsets.ModelViewSet):
    queryset = LineaFactura.objects.all()
    serializer_class = LineaFacturaSerializer
    permission_classes = [permissions.IsAuthenticated]
