# Estructura

tienda
 - productos (id, nombre, descripcion)
 - stock (id_producto, cantidad, precio_actual)
 - cliente(id, nombre, direccion, porcentaje_descuento_actual)
 - factura(id, id_cliente, fecha, porcentaje_descuento)
 - linea(id_factura, id_producto, cantidad, precio)

# Notas

```
$ docker exec -it django-backend-1 bash
```

```
export DJANGO_SUPERUSER_USERNAME=usuario DJANGO_SUPERUSER_PASSWORD=password DJANGO_SUPERUSER_EMAIL=email@email.com; python3 manage.py createsuperuser --noinput
``

Frontend:

```
$ docker run -it --rm node:20-slim bash
$ npm create vite@latest
docker cp dreamy_poitras:/front_tienda/. ./caca
```
