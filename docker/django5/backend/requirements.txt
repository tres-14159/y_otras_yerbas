Django==5.0.4
django-cors-headers==4.3.1
djangorestframework==3.15.1
djangorestframework-simplejwt==5.3.1

# Swagger
drf-yasg==1.21.7

# Postgres
psycopg2==2.9.9

# I want the things to be easy
ipython==8.23.0