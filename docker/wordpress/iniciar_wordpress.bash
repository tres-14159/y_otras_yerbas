#! /bin/bash

function instalar_wordpress() {
    working_dir=$1
    
    echo "[INFO] Iniciando nuevo wordpress"
    docker-compose up --build -d
    
    echo "[INFO] Esperando por la BD"
    bd_not_up=true
    while ($bd_not_up)
    do
        bd_connection=$(nc -v -w20 127.0.0.1 3306)
        chars=$(echo $bd_connection | wc -c)
        if [ $chars -gt 1 ]
        then
            bd_not_up=false
        fi
    done
    
    echo "[INFO] Instalando con usuario prueba:prueba"
    curl 'http://localhost:8080/wp-admin/install.php?step=2' -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:78.0) Gecko/20100101 Firefox/78.0' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Content-Type: application/x-www-form-urlencoded' -H 'Origin: http://localhost:8080' -H 'Connection: keep-alive' -H 'Referer: http://localhost:8080/wp-admin/install.php' -H 'Cookie: wordpress_37d007a56d816107ce5b52c10342db37=prueba%7C1629236287%7Co2nH6Lip5cnSCxfTp5FzcTaXBijicodnfIzNc6i7uWE%7C9436c1d96af0a545f20dd2fc01d485b329ca2156c13ae1f0070bd5e250ae0843; wordpress_test_cookie=WP%20Cookie%20check; wordpress_logged_in_37d007a56d816107ce5b52c10342db37=prueba%7C1629236287%7Co2nH6Lip5cnSCxfTp5FzcTaXBijicodnfIzNc6i7uWE%7Ceb924ec7c354d01aa22f0b2fe4c5aea8b4c976831e8aac01432f193e9251c967; wp-settings-time-1=1629063822' -H 'Upgrade-Insecure-Requests: 1' --data-raw 'weblog_title=prueba&user_name=prueba&admin_password=prueba&admin_password2=prueba&pw_weak=on&admin_email=prueba%40prueba.com&Submit=Install+WordPress&language=' > /dev/null
    
    docker exec copia_local_laberinto_wordpress_1 sh -c "echo \"define('FS_METHOD', 'direct');\" >> wp-config.php"
    docker exec copia_local_laberinto_wordpress_1 sh -c "echo \"php_value upload_max_filesize 256M\" >> .htaccess"
    docker exec copia_local_laberinto_wordpress_1 sh -c "echo \"php_value post_max_size 256M\" >> .htaccess"
    docker exec copia_local_laberinto_wordpress_1 sh -c "echo \"php_value memory_limit 512M\" >> .htaccess"
    docker exec copia_local_laberinto_wordpress_1 sh -c "echo \"php_value max_execution_time 300\" >> .htaccess"
    docker exec copia_local_laberinto_wordpress_1 sh -c "echo \"php_value max_input_time 300\" >> .htaccess"
    
    echo "[INFO] Instalando el WP-CLI"
    docker exec copia_local_laberinto_wordpress_1 curl -O https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar
    docker exec copia_local_laberinto_wordpress_1 chmod +x wp-cli.phar
    docker exec copia_local_laberinto_wordpress_1 mv wp-cli.phar /usr/local/bin/wp
    docker exec copia_local_laberinto_wordpress_1 sh -c "apt update; apt install less -y"
    
    echo "[INFO] Instalando el plugin de backups."
    docker exec copia_local_laberinto_wordpress_1 wp --allow-root plugin install https://downloads.wordpress.org/plugin/all-in-one-wp-migration.7.46.zip
    docker exec copia_local_laberinto_wordpress_1 wp --allow-root plugin activate all-in-one-wp-migration
    docker exec copia_local_laberinto_wordpress_1 chmod 777 -R /var/www/html/wp-content/plugins/all-in-one-wp-migration/storage 
    docker exec copia_local_laberinto_wordpress_1 chmod 777 -R /var/www/html/wp-content/ai1wm-backups/
}

dir_base=$(basename $(pwd))
working_dir=$(pwd)

installed=false
for c in $(docker ps --format "{{.Names}}")
do
    if [[ $c =~ ^$dir_base.*$ ]]
    then
        installed=true
    fi
done

if ($installed)
then
    zenity --question --text="¿Borrar Wordpress previo?"
    if [ $? -eq 0 ]
    then
        echo "[INFO] Borrando anterior wordpress"
        docker-compose down
        docker rm $(docker ps -a -q) -v -f
        docker volume prune -f
        
        instalar_wordpress $working_dir
    else
        exit 0
    fi
else
    instalar_wordpress $working_dir
fi

xdg-open http://localhost:8080/wp-admin/
