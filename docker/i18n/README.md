# Diagram

pot -> po (country language) -> mo (binary)

# Generate the pot (initial)

```
xgettext --keyword=_ --language=C --add-comments --sort-output -o po/test.pot main.c
```
