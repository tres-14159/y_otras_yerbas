#include <stdio.h>
#include <libintl.h>
#include <locale.h>

#define _(STRING) gettext(STRING)

int main(int argc, char** argv) {
	// i18n
	setlocale (LC_ALL, "");
	// First step in local path and after in the system path
    	bindtextdomain ("test", "./locale");
    	textdomain ("test");

	printf(_("HELLO WORLD\n"));
	return  0;
}
