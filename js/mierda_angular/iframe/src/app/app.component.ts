import {Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {BehaviorSubject, fromEvent, Subject, takeUntil} from "rxjs";
import {AsyncPipe, JsonPipe} from "@angular/common";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, AsyncPipe, JsonPipe],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent implements OnInit, OnDestroy {
  private destroy$ = new Subject<void>();

  @ViewChild('iframe') iframe: ElementRef | undefined;

  private respuestaEvent: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  public respuesta$ = this.respuestaEvent.asObservable();

  ngOnInit(): void {
    fromEvent<MessageEvent>(window, 'message')
      .pipe(takeUntil(this.destroy$))
      .subscribe(event => {
        if (event.data.origin === 'iframe') {
          this.respuestaEvent.next(event.data);
        }
      });
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }

  enviar() {
    const message = {'origin': 'parent', 'data': {'aaa': 122, 'bbb': 'ccc'}};
    this.iframe?.nativeElement.contentWindow.postMessage(message, "*");
  }
}
