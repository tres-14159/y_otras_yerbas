import {OrbitControls} from 'three/examples/jsm/controls/OrbitControls.js';
import {
    AxesHelper,
    BoxGeometry,
    BufferGeometry,
    DoubleSide,
    GridHelper, MathUtils, Matrix4,
    Mesh,
    MeshBasicMaterial,
    PerspectiveCamera, Plane, PlaneGeometry, PlaneHelper,
    Scene,
    SphereGeometry, TextureLoader,
    Vector3,
    WebGLRenderer
} from "three";
import { GUI } from 'dat.gui';
import {Color} from "three";

const scene = new Scene();
scene.add( new AxesHelper( 20 ) );

// INI - Camera code
const camera = new PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
scene.add(camera);
// Camara no torcida
// camera.position.set(0, 0, 5);

// Camara torcida
camera.position.set(7, 3, 15);
camera.lookAt(scene.position);
// End camara torcida
camera.updateMatrixWorld(true);
// END - Camera code

// INI - Renderer code
const renderer = new WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.localClippingEnabled = true;
document.body.appendChild(renderer.domElement);
// END - Renderer code

// INI - Grid code
const size = 50;
const divisions = 10;
const gridHelper = new GridHelper(size, divisions);
scene.add(gridHelper);
// END - Grid code

let material = new MeshBasicMaterial({
    map: new TextureLoader().load(new URL("./assets/uv_grid_opengl.jpg", import.meta.url).toString())
});


const cubeGeometry = new BoxGeometry(5, 5, 5);
const cube = new Mesh(cubeGeometry, material);
cube.position.set(3, 4, 0);
cube.rotation.set(0.5, 0.25, 0);
scene.add(cube);

const planeMesh = new Mesh(new PlaneGeometry(10, 10), material);
planeMesh.scale.set(2, 1.5, 1.5);
planeMesh.rotation.x = -Math.PI / 4;
planeMesh.position.x = 4;
scene.add(planeMesh);


const normal = new Vector3(0, 0, 1);
planeMesh.updateMatrixWorld();
normal.applyQuaternion(planeMesh.quaternion);
const cameraDistance = 12;
const cameraPosition = normal.clone().multiplyScalar(cameraDistance);
camera.position.copy(cameraPosition);
camera.lookAt(new Vector3(0, 0, 0)); // Hack: need to set but it not change rotation or position camera
camera.updateMatrixWorld(true);

const topLeft = new Vector3(-1, 1, 0);
const topRight = new Vector3(1, 1, 0);
const bottomRight = new Vector3(1, -1, 0);
const bottomLeft = new Vector3(-1, -1, 0);
topLeft.unproject(camera);
topRight.unproject(camera);
bottomRight.unproject(camera);
bottomLeft.unproject(camera);
const points = [topLeft, topRight, bottomRight, bottomLeft];

const center = new Vector3();
points.forEach(point => center.add(point));
center.divideScalar(points.length);


const width = topLeft.distanceTo(topRight);

const vectorH = topRight.clone().sub(topLeft).normalize();

let posTop = new Vector3();
let posBottom = new Vector3();
const fakePlaneM = new Proxy({
    percent: 100,
    rotation: 0
}, {
    get: (target, prop): number => {
        if (prop === 'percent') {
            return target[prop];
        }
        if (prop === 'rotation') {
            return target[prop];
        }
        throw new Error('Property not found');
    },
    set: (target, prop, value) => {
        if (prop === 'percent') {
            target[prop] = value;
            updatePlane();
        }
        else if (prop === 'rotation') {
            target[prop] = value;
            updatePlane();
        }
        return true;
    }
});

function translatePlane() {
    const inputMin = 0;
    const inputMax = 100;
    const outputMin = 0;
    const outputMax = width;

    const mappedValue = MathUtils.mapLinear(
        fakePlaneM.percent, inputMin, inputMax, outputMin, outputMax);

    posTop = topLeft.clone().add(vectorH.clone().multiplyScalar(mappedValue));
    posBottom = bottomLeft.clone().add(vectorH.clone().multiplyScalar(mappedValue));
}

function rotatePlane() {
    const midpoint = new Vector3().addVectors(posBottom, posTop).multiplyScalar(0.5);

    let  referencePoint= center.clone();
    if (center === midpoint) {
        referencePoint = topLeft;
        if (posTop === topLeft) {
            referencePoint = topRight;
        }
    }

// // Calcula los vectores AB y AC
    const AB = new Vector3().subVectors(posBottom, referencePoint);
    const AC = new Vector3().subVectors(posTop, referencePoint);
//
// // Calcula la normal al plano (producto cruzado)
    const normal = new Vector3().crossVectors(AB, AC).normalize();
//
// // Calcula el ángulo de rotación
    const angle = MathUtils.degToRad(-fakePlaneM.rotation);

    const rotationMatrix = new Matrix4().makeRotationAxis(normal, angle);

// 4. Trasladar los puntos al origen, aplicar la rotación y trasladarlos de vuelta
    posTop.sub(midpoint).applyMatrix4(rotationMatrix).add(midpoint);
    posBottom.sub(midpoint).applyMatrix4(rotationMatrix).add(midpoint);
}

function updatePlane() {
    translatePlane();
    rotatePlane();
    planeM.setFromCoplanarPoints(
        posTop,
        posBottom,
        camera.position.clone());
}



const planeM = new Plane(new Vector3(0, 0, 1));
updatePlane();

const planeDebugHelper = new PlaneHelper( planeM, 15, 0xffff00 );
scene.add( planeDebugHelper );


material = new MeshBasicMaterial({
    map: new TextureLoader().load( new URL("./assets/uv_grid_opengl.jpg", import.meta.url).toString()),
    clippingPlanes: [planeM]
});
cube.material = material;



const controls = new OrbitControls(camera, renderer.domElement);
controls.listenToKeyEvents(window);

const animate = () => {
    requestAnimationFrame(animate);
    controls.update();
    renderer.render(scene, camera);
};
animate();


const gui = new GUI();
const planeFolderGui = gui.addFolder('Plane clipping')
planeFolderGui.add(fakePlaneM, 'percent', 0, 100);
planeFolderGui.add(fakePlaneM, 'rotation', 0, 360);
planeFolderGui.open()