import {OrbitControls} from 'three/examples/jsm/controls/OrbitControls.js';
import {
    BoxGeometry,
    BufferGeometry,
    DoubleSide,
    GridHelper,
    Mesh,
    MeshBasicMaterial,
    PerspectiveCamera,
    Scene,
    SphereGeometry,
    Vector3,
    WebGLRenderer
} from "three";

const scene = new Scene();

// INI - Camera code
const camera = new PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
scene.add(camera);
// Camara no torcida
// camera.position.set(0, 0, 5);

// Camara torcida
camera.position.set(7, 3, 15);
camera.lookAt(scene.position);
// End camara torcida
camera.updateMatrixWorld(true);
// END - Camera code

// INI - Renderer code
const renderer = new WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);
// END - Renderer code

// INI - Grid code
const size = 50;
const divisions = 10;
const gridHelper = new GridHelper(size, divisions);
scene.add(gridHelper);
// END - Grid code

// INI - Cube green code
const cube = new Mesh(
    new BoxGeometry(1, 1, 1),
    new MeshBasicMaterial({color: 0x00ff00})
);
scene.add(cube);
// END - Cube green code

// INI - Points of view plane
// the plane is parallel to the camera and rendered the scene), corners of the plane
const topLeft = new Vector3(-1, 1, 0);
const topRight = new Vector3(1, 1, 0);
const bottomRight = new Vector3(1, -1, 0);
const bottomLeft = new Vector3(-1, -1, 0);


topLeft.unproject(camera);
topRight.unproject(camera);
bottomRight.unproject(camera);
bottomLeft.unproject(camera);
console.log("vertex", topLeft, topRight, bottomRight, bottomLeft);
// END - Points of view plane

// This plane has not the correct center, it is 0,0,0
/*
const geometry = new BufferGeometry()
const points = [topLeft, topRight, bottomRight, bottomLeft];
geometry.setFromPoints(points)
const indices = [
    0, 1, 2, // Primer triángulo
    0, 2, 3  // Segundo triángulo
];
geometry.setIndex(indices);
geometry.computeVertexNormals()

const material = new MeshBasicMaterial({ color: 0xffffff, side: DoubleSide });
const mesh = new Mesh(geometry, material)
scene.add(mesh)
 */


// INI - Plane fill the plane view (it is with the correct center)
const geometry = new BufferGeometry()
const points = [topLeft, topRight, bottomRight, bottomLeft];

// 1. Calcular el centro geométrico del plano
const center = new Vector3();
points.forEach(point => center.add(point));
center.divideScalar(points.length);

// 2. Restar el centro de cada vértice para centrar la geometría en el origen
const adjustedPoints = points.map(point => point.clone().sub(center));
geometry.setFromPoints(adjustedPoints);
const indices = [
    0, 1, 2, // Primer triángulo
    0, 2, 3  // Segundo triángulo
];
geometry.setIndex(indices);
geometry.computeVertexNormals()

const material = new MeshBasicMaterial({color: 0xffffff, side: DoubleSide});
const mesh = new Mesh(geometry, material);
mesh.position.copy(center);
scene.add(mesh)

// Suponiendo que ya tienes el objeto `mesh` en tu escena
mesh.updateMatrixWorld(true);
const worldPosition = new Vector3();
mesh.getWorldPosition(worldPosition);
console.log("World Position of mesh:", worldPosition);
// END - Plane fill the plane view

// INI - Sphere put on the plane using local coordinates
const sphereGeometry = new SphereGeometry(0.1, 32, 32);
const sphereMaterial = new MeshBasicMaterial({color: 0xff0000});
const sphere = new Mesh(sphereGeometry, sphereMaterial);
mesh.add(sphere);
sphere.position.set(0, 0, 0);
// END - Sphere put on the plane using local coordinates


const controls = new OrbitControls(camera, renderer.domElement);
controls.listenToKeyEvents(window);

const animate = () => {
    requestAnimationFrame(animate);
    controls.update();
    renderer.render(scene, camera);
};
animate();