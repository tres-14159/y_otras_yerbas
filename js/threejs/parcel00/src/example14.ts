
    import { GUI } from 'dat.gui';
    import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
    import {
    ArrowHelper,
    BoxGeometry,
    Color, DoubleSide,
    GridHelper, MathUtils,
    Mesh,
    MeshBasicMaterial,
    PerspectiveCamera, Plane, PlaneGeometry, PlaneHelper, Raycaster,
    Scene, SphereGeometry,
    TextureLoader, Vector2, Vector3,
    WebGLRenderer
} from "three";

    // Configuración básica de js
    const scene = new Scene();
    const camera = new PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
    camera.position.z = 15;
    camera.position.y = 0;
    scene.add(camera);

    const renderer = new WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.localClippingEnabled = true;

    document.body.appendChild(renderer.domElement);


    const size = 10;
    const divisions = 10;
    const gridHelper = new GridHelper( size, divisions );
    scene.add( gridHelper );
    gridHelper.position.y = -1;

    const cubeGeometry = new BoxGeometry(5, 5, 5);
    // Generar materiales con colores aleatorios para cada cara
    const cubeMaterials = [];
    for (let i = 0; i < 6; i++) { // El cubo tiene 6 caras
        const color = new Color(Math.random(), Math.random(), Math.random()); // Color aleatorio
        cubeMaterials.push(new MeshBasicMaterial({ color }));
    }
    const cubeMaterial = new MeshBasicMaterial({ color: 0xff0000 });
    const cube = new Mesh(cubeGeometry, cubeMaterial);

    cube.position.x = 3;
    cube.position.y = 4;
    cube.position.z = 0;
    cube.rotation.x = 0.5;
    cube.rotation.y = 0.25;

    scene.add(cube);

    let material = new MeshBasicMaterial({
        map: new TextureLoader().load(new URL("./assets/uv_grid_opengl.jpg", import.meta.url).toString())
    });
    const planeMesh = new Mesh(new PlaneGeometry(10, 10), material);
    planeMesh.scale.set(2, 1.5, 1.5);
    planeMesh.rotation.x = -Math.PI / 4;
    planeMesh.position.x = 4;
    scene.add(planeMesh);

    var arrow = new ArrowHelper(new Vector3(1,1,0), new Vector3(0,0,0), 10, 0x00ff00);
    scene.add(arrow);

    const normal = new Vector3(0, 0, 1);
    planeMesh.updateMatrixWorld();
    normal.applyQuaternion(planeMesh.quaternion);
    const cameraDistance = 12;
    const cameraPosition = normal.clone().multiplyScalar(cameraDistance);
    camera.position.copy(cameraPosition);

    // // Calcular el tamaño del view plane
    // const viewDistance = camera.near; // o cualquier distancia que desees
    // const aspect = camera.aspect;
    // const fov = MathUtils.degToRad(camera.fov);
    // const height = 2 * Math.tan(fov / 2) * viewDistance; // Altura del view plane
    // const width = height * aspect; // Ancho del view plane
    //
    // // Crear un plano que represente el view plane
    // const geometry = new PlaneGeometry(width, height);
    // const material3 = new MeshBasicMaterial({ color: 0xff0000, side: DoubleSide, transparent: true, opacity: 0.5 });
    // const viewPlane = new Mesh(geometry, material3);
    //
    // // Posicionar el plano en frente de la cámara
    // viewPlane.position.z = -viewDistance; // Coloca el plano hacia adelante desde la cámara
    // viewPlane.lookAt(0, 0, 0); // Asegúrate de que el plano esté orientado hacia la escena
    //
    // // Añadir el view plane a la escena
    // scene.add(viewPlane);


    // Calculate the plane's size based on camera's field of view and distance
    const distance = 50; // Adjust this distance as needed
    const fov = camera.fov * Math.PI / 180; // Convert FOV to radians
    const height = 2 * distance * Math.tan(fov / 2);
    const width = height * camera.aspect;

    // Create the plane geometry
    const planeGeometry = new PlaneGeometry(width, height);
    const planeMaterial = new MeshBasicMaterial({ color: 0x00ff00, side: DoubleSide });
    const plane = new Mesh(planeGeometry, planeMaterial);

    // Position the plane in front of the camera
    plane.position.z = -distance;

    // Orient the plane to face the camera
    plane.lookAt(camera.position);

    // Add the plane to the scene
    scene.add(plane);

    const planeM = new Plane(new Vector3(0, 0, 1));
    planeM.setFromCoplanarPoints(
        planeMesh.localToWorld(new Vector3(0, 1, 0)),
        planeMesh.localToWorld(new Vector3(0, -1, 0)),
        camera.position.clone());

    material = new MeshBasicMaterial({
        map: new TextureLoader().load( new URL("./assets/uv_grid_opengl.jpg", import.meta.url).toString()),
        clippingPlanes: [planeM]
    });
    cube.material = material;

    const planeDebugHelper = new PlaneHelper( planeM, 15, 0xffff00 );
    scene.add( planeDebugHelper );


    const controls = new OrbitControls( camera, renderer.domElement );
    controls.listenToKeyEvents(window);

    const animate = () => {
        requestAnimationFrame(animate);
        controls.update();
        renderer.render(scene, camera);
    };
    animate();


    const fakePlaneM = new Proxy({
            percent: 0
        }, {
        get: (target, prop) => {
            if (prop === 'percent') {
                return target[prop];
            }
        },
        set: (target, prop, value) => {
            if (prop === 'percent') {
                target[prop] = value;

                const inputMin = 0;
                const inputMax = 100;
                const outputMin = -3;
                const outputMax = 3;

                const mappedValue = MathUtils.mapLinear(
                    value, inputMin, inputMax, outputMin, outputMax);


                planeM.setFromCoplanarPoints(
                    planeMesh.localToWorld(new Vector3(mappedValue, 1, 0)),
                    planeMesh.localToWorld(new Vector3(mappedValue, -1, 0)),
                    camera.position.clone());
            }
            return true;
        }
    }
    );

    fakePlaneM.percent = 50;

    const gui = new GUI();
    const planeFolderGui = gui.addFolder('Plane clipping');
    planeFolderGui.add(fakePlaneM, 'percent', 0, 100);
    planeFolderGui.open();
