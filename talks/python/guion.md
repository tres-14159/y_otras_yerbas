Python

- (imagen enfermera) shhh Si te has dado cuenta, por fi no digas el truco de magia.

- ¿Qué es python?

- Lenguaje de programación
- Inventado por Guido Van Rossum
- Lenguaje libre de programación
- Lenguaje interpretado
- Lenguaje Multiparadigma
- Lenguaje tipado dinámico
- Nació en 1991
- "Forma rara de escribir"
- Sí. Se llama así en honor a los Monty Python

- Historia
  - Python1
    - Empezó a pensarlo el Señor Guido Van Rossum en 1989 trabajando en la Universidad CWI en los Países Bajos
    - Curiosidad el famoso videojuego español Blade Edge of Darkness (2001) lo usa para su motor y mods (en concreto la versión Python 1.5.2 del 1998
  - Python2
    - 20 años de lenguaje, de 2000-2020
    - Casi todo (80% paquetes famosos, y características del lenguaje) lo del 3 estaba en el 2.
  - Python3
    - En paralelo con Python2 desde 2008, lanzamiento oficial 2020
    - Ojo de momento es un estándar vivo
      - aparición de switch/match por ejemplo
      - Anotaciones poner tipos como PHP, opcional y para "leerlo mejor" y editores, no falla ni nada
        - herramienta mypy chequea que el código tenga bien los tipos.
    - Problema Python2 → Python3
      - ~ 0% (si no, modulo six para compatibilizar)
      - Paquetes famosos migrados 99% (PyGame perdió soporte a video)
      - Internet lleno _"bienintencionados"_ tutoriales, ejemplos...caducados de python2
  - Futuro
    - No hay Python4 a la vista
- Otras cosas
  - Con python se puede hacer casi cualquier cosa, desde scripts, app de escritorio, backend...
  - PEP
    - Forma "democrática" y científica de decidir las nuevas características, librerías, herramientas y otras cosas core.
  - Dictador benevolente
  - 2018 Fin de la dictadura comienzo del consejo de sabios
  - Problema GIL
    - https://realpython.com/python-gil/
    - Vida normal no es un problema, paquetes importantes ya traen mecanismos para optimizar.
- Números de Python
  - % Paquetes en Linux Mint que dependan de python3
  ```
  $ echo "scale=2; $(apt rdepends python3 | wc -l) * 100 / $(apt-cache pkgnames | wc -l)" | tee >(bc -l)
  scale=2; 2463 * 100 / 72080
  3.41
  ```
  
  ```
  $ echo "scale=2; $(apt rdepends python2 | wc -l) * 100 / $(apt-cache pkgnames | wc -l)" | tee >(bc -l)
  scale=2; 262 * 100 / 72080
  .36
  ```
  
  - Satelites CubeSat, con Pycubed placa libre que funciona con CircuitPython
  - StackOverflow 2021 Survey:
    - 3º más popular (1º javascript)
    - 6º Más amado (1º Rust)
    - 19º Mejor pagado (1º Clojure, 8º Rust, 9º Go)
  - Github (https://github.com/madnight/githut)
    - 3 primeros en el 2021
    - Proyectos con mas estrillitas en python entre los 30 primeros
      https://towardsdatascience.com/top-30-github-python-projects-at-the-beginning-of-2022-86b1e3dad1a
  - PyPi (repo de paquetes)
    - 350.000 paquetes
  - Django (framework de backend webs y mas)
    - 6º en ofertas de trabajo tecnologías web (2020)
      https://towardsdatascience.com/top-10-in-demand-web-development-frameworks-in-2021-8a5b668be0d6
  -Three Reasons Python Is The AI Lingua Franca
   https://www.datanami.com/2021/05/26/three-reasons-python-is-the-ai-lingua-franca/
  
- Herramientas
  - Cli
    - python3
      - Salir shell interactivo ctrl+d o escribir quit
    - python3 <fichero.py>
      - Ejecuta código
    - python3 -i <fichero.py>
      - Ejecuta código y entra en modo interactivo.
    - python3 -m http.server 8000
      - Levanta un servidor http
      - Muchos paquetes "se pueden ejecutar" con -m
  - Idle
    - Casi no se usa
  - Pip
    - Gestor de paquetes oficial
    - pip3
  - Ipython
    - ipython3
    - Cli + Vitaminas
    - Colores
    - lineas son celdas (recordar para después)
    - Trucos
      - tecla tabulador: es tu amiga.
      - flechas arriba y abajo: moverse en el historial (y editar para relanzarlo con enter)
      - ctrl + r: buscar en el historial.
      - <lo_que_sea>? : te devuelve el docstring.
      - ? : ayuda ipython.
      - !<linea_comando> : ejecuta <linea_comando> (OJO no python estándar)
        logs = !ls | grep log
      - %history o %hist : muestra el historial de comandos
      - Hay mas
        - %quickref : muestra ayuda rápida de ipython.
        - %lsmagic : lista los "magic" que hay.
        - %timeit <función o código python>: test de carga.
        - %pwd : muestra el directorio de trabajo.
        - %debug : entrar en el pdb depurador.
        - %clear: limpia pantalla.
        - %pinfo: muestra info (y docstring si hay) de un objeto.
        - %paste: pega y ejecuta lo que hay en el portapapeles.
        - %prun <funcion python>: ejecuta y devuelve estadísticas tiempo y otras.
        - %save <camino_fichero> <rango_lineas>: guarda como python el rango de lineas del history.
        - %load <camino_fichero>: carga un fichero python.
  - Flake8
    - Chequear calidad del código
    - pip3 install flake8
    - flake8-html : informes visuales
    - https://github.com/DmytroLitvinov/awesome-flake8-extensions
  - pdb
    - Depurador oficial
    - Un poco rudo como gdb o lldb
    - Se puede poner cuando sale un error:
      - https://stackoverflow.com/questions/242485/starting-python-debugger-automatically-on-error
  - ¿PyCharm de Jetbrains? Pues evitalo, (escalador soviético)
  - Otras herramientas: pytest, Locust....
- Jupyter Notebook
    - Revival Terminales Tontos en la Web
    - pip3 install jupyterlab
    - jupyter-lab
    - notebooks son celdas de cosas (markdown, python....)
    - celdas se mandan a ejecutar
    - <ficheros.ipynb> : son un json con código, datos (la última ejecución) y metadatos.
      - Github los muestra
    - Usos:
      - Informes
      - Ciencia
      - Aprender
      - Plot twist (gif del obama tirando el micro) RINSE y la conferencia es un jupyter notebook
- Gestores de paquetes
  - Pip (BASICO)
    - pip3 install
    - pip3 help
    requirements.txt
  - Pipenv (pip + virtualenv (versiones python) + lock) (DEPRECADO)
  - pyproject.toml
   - Como cargo o npm
     - Poetry
     - PDM
  
- Lenguaje
  - Como se escribe en Python
  
  - Legible y funcional HOLA MUNDO
  
    ```
    print("Hola")
    ```
  
  - Traumático y correctisimo HOLA MUNDO
  
    ```
#! /bin/env python3

def main():
    print("Hola Mundo.")


if __name__ == "__main__":
    main()
    exit(0)
    ```
  
  - Básico
    - Los "bloques de código" se tabulan (con espacios o tabuladores, no mezclar).
        (Instrucción `pass` es tu amiga.)
        ```
            def factorial(n):
                if n == 1:
                    return 1
                else:
                    m = n * factorial(n – 1)
                return m

            factorial(100)
        ```
    - Las lineas no terminan con ; ...bueno se pueden usar para poner lineas en una linea....
    - Para cortar una linea larga se usa \
    ```
    factura = panes * 1.00 + \
        Patatas * 3.50 + lechugas * 5
    ```
    - Todos los loops, funciones, métodos, clases y sentencias condicionales terminan en :
    - Los comentarios de una linea comienzan con # y los múltilinea se meten entre """ """
  - Básico: Variables 1
    - Todo son objetos, no hay tipos básicos.
    - Se usan donde se necesitan.
    - No hay constantes ("Convenciones de codificación" llamar variables CONSTANTE)
    - Son locales (salvo que metas dentro de la función `global <nom_var>`).
    - No hay punteros, ni referencias (lo resuelve con mutable/inmutable, ver mas adelante)
    - Existe el null/nill es el None y None != undefined .
    - `_` es variable basura por "Convenciones de codificación" pero existir exite si se crea
    - Existen los objetos
  - Básico: Variables 2
    - Mutable que puedes modificarlo
      - cuidado, cuando es un parámetro que lo puedes cambiar sin querer
      - cuidado, el = es alias pero no instanciación, hay que copiarlo explicitamente.
    - Inmutable que no puede modificarlo, pero si quemar un nuevo valor.

    - Tipos de datos (los principales)

| Tipo    | Mutabililidad |  Ejemplo                                                                          |
|---------|---------------|-----------------------------------------------------------------------------------|
| `bool`  | immutable     | `True` <br> `False`                                                               |
| `int`   | inmutable     | `42` <br> `0xFEE1DEAD` <br> `1_000_000`                                           |
| `str`   | immutable     | `"HOLA"` <br> `'HOLA'` <br> """ <br> HOLA <br> ADIOS <br> """                     |
| `float` | inmutable     | `1.23` <br> `1.23e45`                                                             |
| `list`  | mutable       | `[0, 1.1, "dos", {"tres": "three"}]`                                              |
| `tuple` | inmutable     | `(0, 1.1, "dos", {"tres": "three"})`                                              |
| `dict`  | mutable       | `{None: "", 0: "cero", "uno": 1, 2.0 : [2, "two"], "tres": {3: 3}, False : True}` |
    
    - Otros tipos de datos:
      bytearray, bytes, complex, range, set
    - las listas pueden crecer `.append` y no hay huecos.
    - los dict pueden tener de clave cualquier cosas que sea "hasheable" por ejemplo un objeto
    - Acceso a los valores en las listas/tuplas
    a = [1, 2, 3, 4, 5]
    a[0]
    a[0: 3]

  - Básico: Operaciones
  - Básico: Condicionales
  - Básico: Loops
  - Básico: Funciones
  - Programación orientada objetos
  - Frikadas utiles
    - destructuración
    - with
    - Docstrings
    - Decoradores
    - Monkey paching
- Paquetes famosos
  - Core
  - Desarrollo Web
      - Flask
      - Django
  - Ciencia
    - Pandas
    - Keras
    - Bokeh
  - Scrapeo
    - Requests
    - Beautiful Soup
    - scrapy
  - Microservicios
    - Celery
  - Base de Datos
    - SQLAlchemy
  - Juegos
    - Pygame
    - Pygame Zero
  

