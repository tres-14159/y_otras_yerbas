extends Node2D

var rng = RandomNumberGenerator.new()

func _process(delta):
	var planet = rng.randi_range(1, 6)
	$moon_0.position.x -= 20 * delta
	$moon_1.position.x -= 20 * delta
	$moon_2.position.x -= 20 * delta
	$earth.position.x -= 20 * delta
	$saturn.position.x -= 20 * delta
	
	match planet:
		1:
			$moon_0.position.x -= 400 * delta
		2:
			$moon_1.position.x -= 400 * delta
		3:
			$moon_2.position.x -= 400 * delta
		4:
			$earth.position.x -= 400 * delta
		5:
			$saturn.position.x -= 400 * delta
		_:
			$moon_0.position.x -= 500 * delta
			$moon_1.position.x -= 500 * delta
			$moon_2.position.x -= 500 * delta
			$earth.position.x -= 500 * delta
			$saturn.position.x -= 500 * delta
