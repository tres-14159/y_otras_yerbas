extends Sprite

var init_speed := 10
var top_speed := 200
var acceleration := 100
var current_speed := init_speed
var time_pressed := 0

var game_over = preload("res://gameover.tscn").instance()

func _process(delta):
	var pressed = false
	if Input.is_action_pressed("move_left"):
		position.x -= current_speed * delta
		pressed = true
	if Input.is_action_pressed("move_right"):
		position.x += current_speed * delta
		pressed = true
	if Input.is_action_pressed("move_up"):
		position.y -= current_speed * delta
		pressed = true
	if Input.is_action_pressed("move_down"):
		position.y += current_speed * delta
		pressed = true
	if pressed:
		if current_speed < top_speed:
			current_speed += acceleration * delta
	else:
		current_speed = init_speed

func _on_Area2D_area_entered(area):
	if area:
		get_tree().root.add_child(game_over)
