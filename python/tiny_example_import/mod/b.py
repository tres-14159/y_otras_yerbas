from mod.a import testA
# It is ok too:
# from .a import testA

def testB():
    print("TEST B CALLS TEST A")
    testA()
