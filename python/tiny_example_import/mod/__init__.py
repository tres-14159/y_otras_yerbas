__all__ = ['a', 'b', 'testA', 'testB']

from mod.a import *
from mod.b import *

# It is ok too:
# from .a import *
# from .b import *

