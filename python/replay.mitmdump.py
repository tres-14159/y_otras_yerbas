# Uses for slow backends, you can record a sesion with the backend with this script.
# 
# Install Mitm
# pip install mitmproxy
# 
# Store a sesion:
# mitmdump -w sesion.mitm
#
# Use the mitmdump as proxy (example)
# curl --proxy http://127.0.0.1:8080 "http://127.0.0.1:8888"
#
# Replay the sesion.mitm
# mitmdump -s replay.mitmdump.py --set replay_file=sesion.mitm

from mitmproxy import http
from mitmproxy import ctx
from mitmproxy.io import FlowReader

class Replay:
    def __init__(self):
        self.flows = []

    def load(self, loader):
        loader.add_option("replay_file", str, "", "Archivo de sesión .mitm")

    def running(self):
        if not ctx.options.replay_file:
            ctx.log.error("Debes proporcionar un archivo de sesión con --set replay_file=archivo.mitm")
            return

        try:
            with open(ctx.options.replay_file, "rb") as f:
                reader = FlowReader(f)
                self.flows = list(reader.stream())
            ctx.log.info(f"Cargados {len(self.flows)} requests desde {ctx.options.replay_file}")
        except Exception as e:
            ctx.log.error(f"Error cargando sesión: {e}")

    def request(self, flow: http.HTTPFlow):
        # Busca una respuesta grabada que coincida con la solicitud actual
        for recorded_flow in self.flows:
            if flow.request.url == recorded_flow.request.url:
                flow.response = recorded_flow.response.copy()
                ctx.log.info(f"Replaying response for {flow.request.url}")
                return

        ctx.log.warn(f"No se encontró respuesta para {flow.request.url}")

addons = [Replay()]
