#!/usr/bin/env python3

#~ test00.py
#~ Copyright (C) 2019 Miguel de Dios Matias

#~ This program is free software: you can redistribute it and/or modify
#~ it under the terms of the GNU General Public License as published by
#~ the Free Software Foundation, either version 3 of the License, or
#~ (at your option) any later version.

#~ This program is distributed in the hope that it will be useful,
#~ but WITHOUT ANY WARRANTY; without even the implied warranty of
#~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#~ GNU General Public License for more details.

#~ You should have received a copy of the GNU General Public License
#~ along with this program. If not, see <http://www.gnu.org/licenses/>.

import urwid

class Caca:
    palette = [
        ('body',         'black',      'light gray', 'standout'),
        ('header',       'white',      'dark red',   'bold'),
        ('button normal','light gray', 'dark blue', 'standout'),
        ('button select','white',      'dark green'),
        ('button disabled','dark gray','dark blue'),
        ('edit',         'light gray', 'dark blue'),
        ('bigtext',      'white',      'black'),
        ('chars',        'light gray', 'black'),
        ('exit',         'white',      'dark cyan'),
        ]
    
    def __init__(self):
        pass
    
    def setup_view(self):
        txt = urwid.Text(u"Hello World")
        return urwid.Filler(txt, 'top')
    
    def unhandled_input(self, key):
        if key in ('f8'):
            raise urwid.ExitMainLoop()
    
    def main(self):
        self.view = self.setup_view()
        self.loop = urwid.MainLoop(self.view, self.palette,
            unhandled_input=self.unhandled_input)
        self.loop.run()

def main():
    Caca().main()

if '__main__'==__name__:
    main()
