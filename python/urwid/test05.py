#!/usr/bin/env python3

#~ test00.py
#~ Copyright (C) 2019 Miguel de Dios Matias

#~ This program is free software: you can redistribute it and/or modify
#~ it under the terms of the GNU General Public License as published by
#~ the Free Software Foundation, either version 3 of the License, or
#~ (at your option) any later version.

#~ This program is distributed in the hope that it will be useful,
#~ but WITHOUT ANY WARRANTY; without even the implied warranty of
#~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#~ GNU General Public License for more details.

#~ You should have received a copy of the GNU General Public License
#~ along with this program. If not, see <http://www.gnu.org/licenses/>.

import urwid

# from https://github.com/openstack/gertty/blob/master/gertty/mywid.py
class FixedButton(urwid.Button):
    def sizing(self):
        return frozenset([urwid.FIXED])

    def pack(self, size, focus=False):
        return (len(self.get_label()) + 4, 1)

def screen_size():
    return urwid.raw_display.Screen().get_cols_rows()

def calc_size(widget):
    width = (screen_size()[0],)
    return widget.pack(width)


class Dialog:
    size = [0, 0]
    dialog = None
    
    def __init__(self, parent, question, callback_yes):
        question_w = urwid.Text(question, "center")
        self.increaseSize(calc_size(question_w))
        yes_btn = FixedButton("Yes", None, "quit")
        yes_btn = urwid.AttrWrap(yes_btn, 'button normal', 'button select')
        self.increaseSize(calc_size(yes_btn))
        no_btn = FixedButton("No", None, "back")
        no_btn = urwid.AttrWrap(no_btn, 'button normal', 'button select')
        self.increaseSize(calc_size(no_btn))
        body = [question, no_btn, yes_btn]
        window = urwid.ListBox(urwid.SimpleFocusListWalker(body))
        window = urwid.LineBox(window)
        self.size[0] += 2
        self.size[1] += 2
        self.dialog = urwid.Overlay(window, parent, 'center', self.size[0], 'middle', self.size[1])
    
    def increaseSize(self, size):
        if self.size[0] < size[0]:
            self.size[0] = size[0]
        self.size[1] += size[1]


b = urwid.Text("BBB")
bb = urwid.LineBox(b)
bbb = urwid.Filler(bb)

urwid.MainLoop(Dialog(bbb, '¿Probando?', None).dialog).run()
