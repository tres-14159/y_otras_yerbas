#!/usr/bin/env python3

#~ test00.py
#~ Copyright (C) 2019 Miguel de Dios Matias

#~ This program is free software: you can redistribute it and/or modify
#~ it under the terms of the GNU General Public License as published by
#~ the Free Software Foundation, either version 3 of the License, or
#~ (at your option) any later version.

#~ This program is distributed in the hope that it will be useful,
#~ but WITHOUT ANY WARRANTY; without even the implied warranty of
#~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#~ GNU General Public License for more details.

#~ You should have received a copy of the GNU General Public License
#~ along with this program. If not, see <http://www.gnu.org/licenses/>.

import urwid

class Caca:
    palette = [
        ('body',         'black',      'light gray', 'standout'),
        ('header',       'white',      'dark red',   'bold'),
        ('button normal','light gray', 'dark blue', 'standout'),
        ('button select','white',      'dark green'),
        ('button disabled','dark gray','dark blue'),
        ('edit',         'light gray', 'dark blue'),
        ('bigtext',      'white',      'black'),
        ('chars',        'light gray', 'black'),
        ('exit',         'white',      'dark cyan'),
        ]
    
    def __init__(self):
        pass
    
    def setup_view(self):
        header = urwid.Text("TEST01.py Ñandú Piragüista con EMOJIS (F8 exits)")
        header = urwid.AttrWrap(header, 'header')
        body = urwid.Text("Hello 😊 World 🌍")
        body = urwid.Filler(body)
        body = urwid.AttrWrap(body, 'body')
        self.window = urwid.Frame(header=header, body=body)
        
        # ~ b = urwid.Text("BBB")
        # ~ b_size = b.pack()
        # ~ bb = urwid.LineBox(b)
        # ~ bbb = urwid.Filler(bb)
        # ~ self.prompt = urwid.Overlay(bbb, self.window, 'center', b_size[0] + 2, 'middle', b_size[1] + 2)
        
        self.quit_prompt()
    
    def quit_prompt(self):
        question = urwid.Text(("bold", "Exit?"), "center")
        yes_btn = urwid.Button("Yes", self.exit_callback, "quit")
        yes_btn = urwid.AttrWrap(yes_btn, 'button normal', 'button select')
        no_btn = urwid.Button("No", self.back_callback, "back")
        no_btn = urwid.AttrWrap(no_btn, 'button normal', 'button select')
        body = [question, urwid.Divider(), urwid.Divider(), no_btn, yes_btn]

        self.prompt = urwid.ListBox(urwid.SimpleFocusListWalker(body))

        # ~ self.prompt = urwid.Overlay(self.prompt, self.window, 'center', self.prompt.pack()[0], 'middle', self.prompt.pack()[1])
        # ~ self.prompt = urwid.Overlay(self.prompt, self.window, 'center', ('relative', 50), 'middle', ('relative', 50))
    
    # ~ def quit_prompt(self):
        # ~ self.prompt = urwid.Text("AAA")
        # ~ self.prompt = urwid.Overlay(urwid.LineBox(self.prompt), self.window, 'center', None, 'middle', None)
    
    def back_callback(self):
        self.loop.widget = self.window
    
    def exit_callback(self):
        raise urwid.ExitMainLoop()
    
    def unhandled_input(self, key):
        if key in ('f8'):
            self.loop.widget = self.prompt
    
    def main(self):
        self.setup_view()
        self.loop = urwid.MainLoop(self.window, self.palette,
            unhandled_input=self.unhandled_input)
        self.loop.run()

def main():
    Caca().main()

if '__main__'==__name__:
    main()
