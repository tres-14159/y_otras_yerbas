#!/usr/bin/env python3

#~ test00.py
#~ Copyright (C) 2019 Miguel de Dios Matias

#~ This program is free software: you can redistribute it and/or modify
#~ it under the terms of the GNU General Public License as published by
#~ the Free Software Foundation, either version 3 of the License, or
#~ (at your option) any later version.

#~ This program is distributed in the hope that it will be useful,
#~ but WITHOUT ANY WARRANTY; without even the implied warranty of
#~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#~ GNU General Public License for more details.

#~ You should have received a copy of the GNU General Public License
#~ along with this program. If not, see <http://www.gnu.org/licenses/>.

import urwid

question = urwid.Text(("bold", "Exit?"), "center")
yes_btn = urwid.Button("Yes", None, "quit")
yes_btn = urwid.AttrWrap(yes_btn, 'button normal', 'button select')
no_btn = urwid.Button("No", None, "back")
no_btn = urwid.AttrWrap(no_btn, 'button normal', 'button select')
body = [question, urwid.Divider(), urwid.Divider(), no_btn, yes_btn]
prompt = urwid.ListBox(urwid.SimpleFocusListWalker(body))

# ~ a = urwid.Text("AAA\nCCC\nDDD")
# ~ a_size = a.pack()
# ~ aa = urwid.LineBox(a)
# ~ aaa = urwid.Filler(aa)

b = urwid.Text("BBB")
bb = urwid.LineBox(b)
bbb = urwid.Filler(bb)

# ~ c = urwid.Overlay(aaa, bbb, 'center', a_size[0] + 2, 'middle', a_size[1] + 2)
c = urwid.Overlay(prompt, bbb, 'center', ('relative', 10), 'middle', ('relative', 10))

urwid.MainLoop(c).run()
