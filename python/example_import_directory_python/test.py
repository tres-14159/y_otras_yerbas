# ----------- EXAMPLE 1 --------------------

#~ from aaa import test
#~ test(1111)

# ----------- EXAMPLE 2 --------------------
#~ from aaa.bbb import *
#~ test(1111)

# ----------- EXAMPLE 3 --------------------
#~ import aaa
#~ aaa.bbb.test(1111)

# ----------- EXAMPLE 4 --------------------
#~ from aaa import bbb
#~ bbb.test(1111)

from aaa import bbb
bbb.test(1111)