#!/usr/bin/env python3
#
# 6174.py
# Copyright (C) 2019  Miguel de Dios Matias (as developer)
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# https://en.wikipedia.org/wiki/Kaprekar%27s_routine
# https://www.bbc.com/mundo/noticias-49426284

def sort_number(number):
    sort_list = list(str(number))
    sort_list.sort()
    return int(str.join('', sort_list))

def reverse_number(number):
    sort_list = list(str(number))
    sort_list.sort()
    sort_list.reverse()
    return int(str.join('', sort_list))

def main():
    while True:
        user_number = input('Number (four digits)> ')
        try:
            number = int(user_number)
        except ValueError:
            print('"{}" is not a number'.format(user_number))
        else:
            if number <= 9999 and number >= 0:
                # Check the number has 2 digits differents
                if str(number).count(str(number)[0]) < 3  and str(number).count(str(number)[1]) < 3:
                    break
                else:
                    print('The number must has only two equal digits.')
    sort = sort_number(number)
    reverse = reverse_number(number)
    result = 0
    
    print('Number:\t\t\t{}'.format(number))
    print('Sort:\t\t\t{}'.format(sort))
    print('Reverse:\t\t{}'.format(reverse))
    
    over_steps = 0
    step_number = 0
    while True:
        step_number = reverse - sort
        print('{} - {} = {}'.format(reverse, sort, step_number))
        sort = sort_number(step_number)
        reverse = reverse_number(step_number)
        if step_number == 6174:
            if over_steps == 0:
                print("Found Kaprekar's routine")
                print('It is running 3 steps more to show it')
            over_steps +=1
        if over_steps == 4:
            break

if __name__ == '__main__':
    main()
