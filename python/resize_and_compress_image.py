# Instalar con pip wand

from wand.image import Image
from wand.api import library
import glob, os

print("[INFO] Starting conversion")

os.chdir(".")
os.mkdir("redimensionada")
count = 0
for foto in glob.glob("*.jpg"):
    print(f"[INFO] Converting {foto}")
    f = Image(filename=foto)
    f.strip()
    f.sample(int(f.size[0] * 0.25), int(f.size[1] * 0.25))
    f.rotate(90)
    f.compression_quality = 70
    f.save(filename=f"redimensionada/{foto}")
    count += 1

print(f"[INFO] Converted {count} fotos")
input("[INFO] Press any key to close")
