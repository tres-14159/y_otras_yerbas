#!/usr/bin/env python3

from fastapi import FastAPI, Security, HTTPException
from fastapi_jwt import JwtAuthorizationCredentials, JwtAccessBearer
from fastapi.responses import HTMLResponse
from pydantic import BaseModel

import json
import fcntl
import os

app = FastAPI()
access_security = JwtAccessBearer(secret_key="secret_key", auto_error=True)

class Login(BaseModel):
    user: str
    passw: str

class Item(BaseModel):
    item: str
    category: str
    amount: int
    price: float

@app.post('/users/create')
async def create(newUser: Login):
    if not os.path.isfile('users.json'):
        open('users.json', 'w').close()
    
    with open("users.json", "r+") as f:
        try:
            fcntl.flock(f, fcntl.LOCK_EX | fcntl.LOCK_NB)
            size = f.seek(0, os.SEEK_END)
            users = []
            if size > 0:
                f.seek(0)
                f_json = f.read()
                users = json.loads(f_json)
            if len(list(filter(lambda x: x['user'] == newUser.user, users))) > 0:
                raise HTTPException(status_code=400, detail="Error exists")
            # YES IT CLEAR PASSWORD IN THE FILE
            user = newUser.dict()
            user['enabled'] = False
            user['role'] = 'user'
            users.append(user)
            f.truncate(0)
            f.seek(0)
            f.write(json.dumps(users))
            return {'created': True}
        except IOError as e:
            raise HTTPException(status_code=500, detail=f"Error login {e}")
        finally:
            fcntl.flock(f, fcntl.LOCK_UN)
            f.close()
            

@app.post("/users/auth")
async def auth(login: Login = None):
    with open("users.json", "r") as f:
        try:
            fcntl.flock(f, fcntl.LOCK_EX | fcntl.LOCK_NB)
            size = f.seek(0, os.SEEK_END)
            if size == 0:
                raise HTTPException(status_code=500, detail=f"Error empty file")
            f.seek(0)
            f_json = f.read()
            users = json.loads(f_json)
            filtered = list(filter(lambda x: x['user'] == login.user and x['passw'] == login.passw, users))
            if len(filtered) == 0:
                raise HTTPException(status_code=400, detail="Error not exists")
            if not filtered[0]['enabled']:
                raise HTTPException(status_code=400, detail="Error not enabled")
            subject = {"username": filtered[0]['user'], "role": filtered[0]['user']}
            return {"access_token": access_security.create_access_token(subject=subject)}
        except IOError:
            raise HTTPException(status_code=500, detail="Error login")
        finally:
            fcntl.flock(f, fcntl.LOCK_UN)
            f.close()

@app.get("/users/me")
async def read_current_user(
    credentials: JwtAuthorizationCredentials = Security(access_security),
):
    if not credentials:
        raise HTTPException(status_code=401, detail='my-custom-details')
    return {"username": credentials["username"], "role": credentials["role"]}

@app.get("/items")
async def read_items(
    q: str | None = None,
    amount: int | None = None,
    category: str | None = None
    credentials: JwtAuthorizationCredentials = Security(access_security)
):
    if not credentials:
        raise HTTPException(status_code=401, detail='Not login')
    try:
        with open("items.json", "r") as f:
            try:
                fcntl.flock(f, fcntl.LOCK_EX | fcntl.LOCK_NB)
                f_json = f.read()
                items = json.loads(f_json)
                return items
            except IOError:
                raise HTTPException(status_code=500, detail="Error get items")
            finally:
                fcntl.flock(f, fcntl.LOCK_UN)
                f.close()
    except FileNotFoundError:
        return []

@app.post("/items")
async def create_items(
    newItem: Item,
    credentials: JwtAuthorizationCredentials = Security(access_security)
):
    if not credentials:
        raise HTTPException(status_code=401, detail='Not login')

    if not os.path.isfile('items.json'):
        open('items.json', 'w').close()

    with open("items.json", "r+") as f:
        try:
            fcntl.flock(f, fcntl.LOCK_EX | fcntl.LOCK_NB)
            size = f.seek(0, os.SEEK_END)
            items = []
            if size > 0:
                f.seek(0)
                f_json = f.read()
                items = json.loads(f_json)
            filtered = list(filter(lambda x: x[1]['item'] == newItem.item, enumerate(items)))
            if len(filtered) == 0:
                items.append(newItem.dict())
            else:
                items[filtered[0][0]] = newItem.dict()
            f.truncate(0)
            f.seek(0)
            f.write(json.dumps(items))
            return {'created': True}
        except IOError as e:
            raise HTTPException(status_code=500, detail=f"Error login {e}")
        finally:
            fcntl.flock(f, fcntl.LOCK_UN)
            f.close()

@app.get('/items/{index}')
async def get_item(
    index: int,
    credentials: JwtAuthorizationCredentials = Security(access_security),
):
    with open("items.json", "r") as f:
        try:
            fcntl.flock(f, fcntl.LOCK_EX | fcntl.LOCK_NB)
            f_json = f.read()
            items = json.loads(f_json)
            return items[index]
        except FileNotFoundError:
            return []
        except IOError as e:
            raise HTTPException(status_code=500, detail=f"Error login {e}")
        finally:
            fcntl.flock(f, fcntl.LOCK_UN)
            f.close()
    

@app.get('/')
def home():
    return {"message": "Hello World"}

# uvicorn main:app --reload

# jwt=$(http post http://127.0.0.1:8000/auth | jq .access_token -r)
# http -A bearer -a $jwt http://127.0.0.1:8000/users/me

# HTMLResponse(content=html_content, status_code=200)
