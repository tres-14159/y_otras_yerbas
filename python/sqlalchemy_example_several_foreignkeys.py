from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, relationship


Base = declarative_base()

class A(Base):
    __tablename__ = 'a'
    id = Column(Integer, primary_key=True)
    z = Column(String, nullable = False)
    bs = relationship("B", cascade="all, delete", back_populates="a")
    c1s = relationship("C", cascade="all, delete", foreign_keys="C.a1_id", back_populates="a1")
    c2s = relationship("C", cascade="all, delete", foreign_keys="C.a2_id", back_populates="a2")
    
    def __init__(self, z):
        self.z = z

class B(Base):
    __tablename__ = 'b'
    id = Column(Integer, primary_key=True)
    z = Column(String, nullable = False)
    a_id = Column(Integer, ForeignKey('a.id'))
    a = relationship("A", back_populates="bs")
    
    def __init__(self, z, a_id):
        self.z = z
        self.a_id = a_id

class C(Base):
    __tablename__ = 'c'
    id = Column(Integer, primary_key=True)
    z = Column(String, nullable = False)
    a1_id = Column(Integer, ForeignKey('a.id'))
    a2_id = Column(Integer, ForeignKey('a.id'))
    a1 = relationship("A", back_populates="c1s", foreign_keys=[a1_id])
    a2 = relationship("A", back_populates="c2s", foreign_keys=[a2_id])
    
    def __init__(self, z, a1_id, a2_id):
        self.z = z
        self.a1_id = a1_id
        self.a2_id = a2_id

engine = create_engine('sqlite:///caca.db', echo=True)
Base.metadata.create_all(engine) # Create or update sqlitefile
Session = sessionmaker(bind=engine)
session = Session()

session.add(A('a'))
session.add(A('b'))
session.add(A('c'))

session.add(B('x', 1))
session.add(B('y', 1))
session.add(B('z', 2))

session.add(C('r', 1, 1))

session.commit()

a = session.query(A).first()