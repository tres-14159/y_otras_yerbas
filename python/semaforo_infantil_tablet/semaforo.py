#! /usr/bin/env python3

from flask import Flask, jsonify, request
import tempfile
import pickle
import json

default_debug = True
default_port = 5000 
default_value = {'coche': 'rojo', 'peaton': 'verde'}

index_page = """
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>🚦SEMÁFORO</title>
    </head>
    <body>
        <h1 style="text-align:center;">SEMÁFORO</h1>
        <hr />
        <h2>CONTROL</h2>
        <span style="font-size: xx-large;">⚙️</span> <a href="{url_controlador}/control">{url_controlador}/control</a>
        <h2>SEMÁFORO COCHES</h2>
        <span style="font-size: xx-large;">🚶</span> <a href="{url_controlador}/peatones">{url_controlador}/peatones</a>
        <h2>SEMÁFORO PEATONES</h2>
        <span style="font-size: xx-large;">🚗</span> <a href="{url_controlador}/coches">{url_controlador}/coches</a>
    </body>
</html>
"""

control_page = """
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>🚦SEMÁFORO: CONTROL</title>
    </head>
    <body>
        <h1 style="text-align:center;">SEMÁFORO: CONTROL ⚙</h1>
        <hr />
        <div>
            <span style="font-size: xx-large;">🚦🚗</span>
            <span style="font-size: xx-large;">🔴</span><input type="range" id="coche" id="price" min="0" max="1" step="1" value="0" onChange="change('coche');"><span style="font-size: xx-large;">🟢</span>
        </div>
        <div>
            <span style="font-size: xx-large;">🚦🚶</span>
            <span style="font-size: xx-large;">🔴</span><input type="range" id="peaton" id="price" min="0" max="1" step="1" value="1" onChange="change('peaton');"><span style="font-size: xx-large;">🟢</span>
        </div>
        
        <script type="text/javascript">
            let semaforo = {semaforo};
            
            function change(tipo) {{
                let valor = document.getElementById(tipo).value;
                switch (tipo) {{
                    case 'coche':
                        semaforo['coche'] = valor == 0 ? 'rojo' : 'verde';
                        semaforo['peaton'] = valor == 1 ? 'rojo' : 'verde';
                        break;
                    case 'peaton':
                        semaforo['peaton'] = valor == 0 ? 'rojo' : 'verde';
                        semaforo['coche'] = valor == 1 ? 'rojo' : 'verde';
                        break;
                }}
                setSliders();
                fetch(`/estado?set=1&coche=${{semaforo['coche']}}&peaton=${{semaforo['peaton']}}`);
            }}
            
            function setSliders() {{
                switch (semaforo['coche']) {{
                    case 'rojo':
                        document.getElementById('coche').value = 0;
                        break;
                    case 'verde':
                        document.getElementById('coche').value = 1;
                        break;
                }}
                switch (semaforo['peaton']) {{
                    case 'rojo':
                        document.getElementById('peaton').value = 0;
                        break;
                    case 'verde':
                        document.getElementById('peaton').value = 1;
                        break;
                }}
            }}
            setSliders();
        </script>
    </body>
</html>
"""

semaforo_coches_page = """
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>🚦SEMÁFORO: COCHES</title>
        <style type="text/css">
            body {
                height: 100%;
                width: 100%;
                background: black;
                display: flex;
                align-items: center;
                justify-content: center;
            }
            .semaforo {
                border-radius: 40%/10%;
                background: #004400;
                display: flex;
                align-items: center;
                justify-content: center;
                flex-direction: column;
                width: fit-content;
            }
            
            .luz {
                height: 25vh;
                width: 25vh;
                border-radius: 50%/50%;
                margin: 1vh;
                border-color: black;
                border-width: 3px;
                border-style: solid;
            }
            
            .negro {
                background: #222222;
            }
            .rojo {
                background: red;
            }
            .amarillo {
                background: yellow;
            }
            .verde {
                background: lime;
            }
        </style>
    </head>
    <body>
        <div class="semaforo">
            <div id="rojo" class="luz rojo"></div>
            <div id="amarillo" class="luz amarillo"></div>
            <div id="verde" class="luz verde"><div>
        </div>
        <script type="text/javascript">
            var previo;
            setInterval(function() {
                fetch('/estado')
                    .then(data => {
                        data.json().then(semaforo => {
                            document.getElementById('rojo').classList.replace('rojo', 'negro');
                            document.getElementById('amarillo').classList.replace('amarillo', 'negro');
                            document.getElementById('verde').classList.replace('verde', 'negro');
                            if (typeof(previo) === 'undefined') {
                                switch (semaforo['coche']) {
                                    case 'rojo':
                                        document.getElementById('rojo').classList.replace('negro', 'rojo');
                                        break;
                                    case 'verde':
                                        document.getElementById('verde').classList.replace('negro', 'verde');
                                        break;
                                }
                            }
                            else {
                                switch (semaforo['coche']) {
                                    case 'rojo':
                                        if (previo['coche'] == 'verde') {
                                            document.getElementById('amarillo').classList.replace('negro', 'amarillo');
                                            setTimeout(function() {
                                                document.getElementById('amarillo').classList.replace('amarillo', 'negro');
                                                document.getElementById('rojo').classList.replace('negro', 'rojo');
                                            }, 100);
                                        }
                                        else {
                                            document.getElementById('rojo').classList.replace('negro', 'rojo');
                                        }
                                        break;
                                    case 'verde':
                                        document.getElementById('verde').classList.replace('negro', 'verde');
                                        break;
                                }
                            }
                            previo = semaforo;
                        })
                    });
            }, 200);
        </script>
    </body>
</html>
"""

semaforo_peatones_page = """
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>SEMÁFORO</title>
        <style type="text/css">
            body {
                background: black;
                display: flex;
                align-items: center;
                justify-content: center;
            }
            .semaforo {
                border-radius: 40%/10%;
                background: #004400;
                display: flex;
                align-items: center;
                justify-content: center;
                flex-direction: column;
                width: fit-content;
            }
            
            .luz {
                height: 45vh;
                width: 45vh;
                border-radius: 50%/50%;
                margin: 1vh;
                border-color: black;
                border-width: 3px;
                border-style: solid;
                
                display: flex;
                align-items: center;
                justify-content: center;
            }
            
            .negro {
                background: #222222;
            }
            .rojo {
                fill:#ff0000;
            }
            .verde {
                fill:#44aa00;
            }
        </style>
    </head>
    <body>
        <div class="semaforo">
            <div id="rojo" class="luz negro">
            <svg
               xmlns:dc="http://purl.org/dc/elements/1.1/"
               xmlns:cc="http://creativecommons.org/ns#"
               xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
               xmlns:svg="http://www.w3.org/2000/svg"
               xmlns="http://www.w3.org/2000/svg"
               id="svg4023"
               version="1.1"
               viewBox="0 0 14.896023 51.659645"
               height="80%"
               width="80%">
              <defs
                 id="defs4017" />
              <metadata
                 id="metadata4020">
                <rdf:RDF>
                  <cc:Work
                     rdf:about="">
                    <dc:format>image/svg+xml</dc:format>
                    <dc:type
                       rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
                    <dc:title></dc:title>
                  </cc:Work>
                </rdf:RDF>
              </metadata>
              <g
                 transform="translate(-159.80645,-121.30262)"
                 id="layer1">
                <g
                   style="fill:#ff0000;fill-opacity:1"
                   transform="matrix(0.07130078,0,0,0.07130078,151.22627,117.52368)"
                   id="g2458">
                  <circle
                     r="56"
                     cx="188"
                     cy="105"
                     class="rojo"
                     style="fill-opacity:1;stroke:none"
                     id="circle2460"
                     transform="translate(36.796873,3.9999996)" />
                  <path
                     class="rojo"
                     style="fill-opacity:1;stroke:none"
                     d="m 223.375,186 c -8.18045,0.13141 -62.91111,0.32153 -66.53125,0.625 C 113.81441,190.23216 120.75,400.89977 120.75,431 c 35.47023,14 42.34564,-36.57929 48.40625,-177.54808 l 1,183.54808 h 51.5 6.28125 51.5 l 1,-183.54808 C 286.49811,394.42071 293.37352,445 328.84375,431 c -10e-6,-30.10023 6.93559,-240.76784 -36.09375,-244.375 -3.62015,-0.30347 -58.35081,-0.49359 -66.53125,-0.625 -0.18703,-0.003 -0.92643,0.003 -1.0625,0 -0.0678,0.002 -0.6267,-0.002 -0.71875,0 0,0 -1.0625,0 -1.0625,0 z"
                     id="path2462" />
                  <path
                     class="rojo"
                     style="fill-opacity:1;stroke:none"
                     d="m 169.95312,454.5 -19.875,323.03125 c 70.56449,-4.89076 70.65922,28.07791 70.65625,-248.0625 1.26065,-0.13507 2.71821,-0.37999 4.0625,-0.5625 1.35617,0.18354 2.82245,0.4271 4.09375,0.5625 -0.003,276.14041 0.0605,243.17174 70.625,248.0625 L 279.67187,454.5 l -47.6875,0.0312 h -14.375 z"
                     id="path2464" />
                </g>
              </g>
            </svg>
            </div>
            <div id="verde" class="luz negro">
                <svg
                   xmlns:dc="http://purl.org/dc/elements/1.1/"
                   xmlns:cc="http://creativecommons.org/ns#"
                   xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                   xmlns:svg="http://www.w3.org/2000/svg"
                   xmlns="http://www.w3.org/2000/svg"
                   id="svg8014"
                   version="1.1"
                   viewBox="0 0 28.385574 52.677715"
                   height="80%"
                   width="80%">
                  <defs
                     id="defs8008" />
                  <metadata
                     id="metadata8011">
                    <rdf:RDF>
                      <cc:Work
                         rdf:about="">
                        <dc:format>image/svg+xml</dc:format>
                        <dc:type
                           rdf:resource="http://purl.org/dc/dcmitype/StillImage" />
                        <dc:title></dc:title>
                      </cc:Work>
                    </rdf:RDF>
                  </metadata>
                  <g
                     transform="translate(-60.646496,-91.500428)"
                     id="layer1">
                    <g
                       style="fill:#44aa00"
                       transform="matrix(0.07503948,0,0,0.07503948,59.896101,87.823493)"
                       id="g2466">
                      <circle
                         r="56"
                         cx="188"
                         cy="105"
                         class="verde"
                         style="stroke:none"
                         id="circle2468" />
                      <path
                         class="verde"
                         style="stroke:none"
                         d="M 22,427 C 173.66667,190.33333 159.33333,183.66667 216,182 c 33,1 34,10 90,54 69,50.66667 64,51.33333 63,107 -0.33333,67 4.33333,62 -49,87 V 317 L 273,275 V 434 L 161,433 160,307 C 68,449 74,441 22,427 Z"
                         id="path2470" />
                      <path
                         class="verde"
                         style="stroke:none"
                         d="m 163,450 h 109 c 0,25 8,23 66,132 61,112.33333 62,104.66667 27,169 L 214,480 C 71,756.66667 89,748.33333 10,743 L 89,595 c 67.66667,-122.33333 71.33333,-122.66667 74,-145 z"
                         id="path2472" />
                    </g>
                  </g>
                </svg>
            <div>
        </div>
        <script type="text/javascript">
            setInterval(function() {
                fetch('/estado')
                    .then(data => {
                        data.json().then(semaforo => {
                            document.styleSheets[0].rules[4].style.fill = "rgb(0, 0, 0)"
                            document.styleSheets[0].rules[5].style.fill = "rgb(0, 0, 0)"
                            switch (semaforo['peaton']) {
                                case 'rojo':
                                    document.styleSheets[0].rules[4].style.fill = "rgb(255, 0, 0)"
                                    break;
                                case 'verde':
                                    document.styleSheets[0].rules[5].style.fill = "rgb(68, 170, 0)"
                                    break;
                            }
                        })
                    });
            }, 200);
        </script>
    </body>
</html>
"""

def get_ip():
    import socket
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        s.connect(('10.255.255.255', 1))
        IP = s.getsockname()[0]
    except Exception:
        IP = '127.0.0.1'
    finally:
        s.close()
    return IP

fstatus = tempfile.mkstemp()[1]
print(f'[INFO] Temp file "{fstatus}"')
app = Flask(__name__)

@app.route("/")
def index():
    return index_page.format(url_controlador=f"http://{get_ip()}:{default_port}")

@app.route('/estado')
def estado():
    set_estado = request.args.get('set', default = 0, type = int)
    coche = request.args.get('coche', default = default_value['coche'], type = str)
    peaton = request.args.get('peaton', default = default_value['peaton'], type = str)
    print(f'[INFO] {set_estado} {coche} {peaton}')
    
    if set_estado:
        semaforo = {'coche': coche, 'peaton': peaton}
        with open(fstatus, "wb") as f:
            pickle.dump(semaforo, f)
    
    with open(fstatus, "rb") as f:
        try:
            semaforo = pickle.load(f)
        except (EOFError, pickle.UnpicklingError) as e:
            semaforo = default_value
    
    return jsonify(semaforo)

@app.route("/control")
def control():
    with open(fstatus, "rb") as f:
        try:
            semaforo = pickle.load(f)
        except (EOFError, pickle.UnpicklingError) as e:
            semaforo = default_value
    return control_page.format(semaforo = json.dumps(semaforo))

@app.route("/coches")
def semaforo_coches():
    return semaforo_coches_page

@app.route("/peatones")
def semaforo_peatones():
    return semaforo_peatones_page

app.run(host="0.0.0.0", port=default_port, debug=default_debug)
