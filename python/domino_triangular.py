#! /bin/env python3

# Domino triangular

a = [i for i in range(556)]
b = ['{:03d}'.format(i) for i in a]

for n in range(6,10):
    b = [x for x in b if str(n) not in x]

c = b.copy()
d = []
for n in b:
    d.append(n)
    n1 = n[1:] + n[0:1]
    n2 = n1[1:] + n1[0:1]
    if n1 not in d:
        i = None
        try:
            i = c.index(n1)
        except:
            pass
        if i is not None:
            del(c[i])
    if n2 not in d:
         i = None
         try:
             i = c.index(n2)
         except:
             pass
         if i is not None:
             del(c[i])
