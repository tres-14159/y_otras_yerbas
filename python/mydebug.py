def debug(var, file = None):
    """
    class Foo:
        aaa = 1
        bbb = True
        def bar():
            pass
    aaa = {1: "aaa", 2: "b", 3: "c"}
    aaa = [1, "aaa", 2, "b", 3, "c"]
    aaa = None
    aaa = 1
    aaa = 1.666
    aaa = "abcdf"
    aaa = True
    
    debug(aaa)
    debug(aaa, True)
    debug(aaa, "/tmp/mylog")
    """
    import time
    import inspect
    import pprint

    if file is True:
        file = "/tmp/debug"
    elif (not isinstance(file, str)):
        file = None

    more_info = ""

    custom_obj = False

    if (isinstance(var, str)):
        more_info = "size: {}".format(len(var))
    elif (isinstance(var, bool)):
        more_info = "val: " + ('true' if var else 'false')
    elif var is None:
        more_info = "is None"
    elif (isinstance(var, list)):
        more_info = "size: {}".format(len(var))
    elif (isinstance(var, tuple)):
        more_info = "size: {}".format(len(var))
    elif (isinstance(var, set)):
        more_info = "size: {}".format(len(var))
    elif (isinstance(var, dict)):
        more_info = "size: {}".format(len(var))
    else:
        try:
            int(var)
        except ValueError:
            custom_obj = True
        
        more_info = "__str__: {}".format(var)
    
    first_line = time.strftime("%Y/%m/%d %H:%M:%S", time.gmtime()) + " (" + "{}".format(type(var)) + ") " +more_info
    if custom_obj:
        second_line = pprint.pformat(inspect.getmembers(var))
    else:
        second_line = "{}".format(var)
    
    if file is None:
        print(first_line)
        print(second_line)
    else:
        f = open(file, "a")
        f.write(first_line + "\n")
        f.write(second_line + "\n")
        f.close()
