import random

secret = random.randrange(1, 100)
min_num = 1
max_num = 100
num = random.randrange(1, 100)


while True:
    if num == secret:
        print(f"Correct: num {num} == secret {secret}")
        break
    elif num > secret:
        print(f"Fail: num {num} > secret {secret}")
        temp = int(min_num + (num - min_num) / 2)
        max_num = num
        num = temp
    elif num < secret:
        print(f"Fail: num {num} < secret {secret}")
        temp = int(num + (max_num - num) / 2)
        min_num = num
        num = temp
    if num > 100 or num <= 1:
        print("Fail")
        break
    _ = input(f"(min {min_num} - max {max_num}) Next {num}>")
